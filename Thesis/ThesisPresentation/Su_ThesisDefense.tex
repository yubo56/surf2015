    \documentclass[xcolor=dvipsnames]{beamer}
    \usepackage{amsmath, amsthm, amssymb, lmodern, graphicx, wasysym, upgreek}
    \usetheme{Madrid}
    \usefonttheme{professionalfonts}
    \newcommand{\scinot}[2]{#1\times10^{#2}}
    \newcommand{\bra}[1]{\left<#1\right|}
    \newcommand{\ket}[1]{\left|#1\right>}
    \newcommand{\dotp}[2]{\left<\left.#1\right|#2\right>}
    \newcommand{\rd}[2]{\frac{\mathrm{d}#1}{\mathrm{d}#2}}
    \newcommand{\pd}[2]{\frac{\partial#1}{\partial#2}}
    \newcommand{\rtd}[2]{\frac{\mathrm{d}^2#1}{\mathrm{d}#2^2}}
    \newcommand{\ptd}[2]{\frac{\partial^2 #1}{\partial#2^2}}
    \newcommand{\norm}[1]{\left|\left|#1\right|\right|}
    \newcommand{\abs}[1]{\left|#1\right|}
    \newcommand{\mum}[0]{\upmu \mathrm{m}}
    \newcommand{\pvec}[1]{\vec{#1}^{\,\prime}}
    \newcommand{\tensor}[1]{\overleftrightarrow{#1}}
    \let\Re\undefined
    \let\Im\undefined
    \newcommand{\ang}[0]{\text{\AA}}
    \newcommand{\mum}[0]{\upmu \mathrm{m}}
    \DeclareMathOperator{\Re}{Re}
    \DeclareMathOperator{\Tr}{Tr}
    \DeclareMathOperator{\Im}{Im}
    \newcommand{\expvalue}[1]{\left<#1\right>}
    \everymath{\displaystyle}
    \usepackage[font=scriptsize]{subcaption}

\title[Senior Thesis Final Presentation]{Simulation Pipeline for Velocity Field Measurements using the Kinetic Sunyaev-Zel'dovich Effect}
% \subtitle[]{Subtitle}
\author[Yubo Su]{Yubo Su\inst{1}\\
Mentor: Sunil R. Golwala\inst{1}}
\institute[Caltech]{\inst{1}California Institute of Technology}
\date{May 18 2016}
\logo{\includegraphics[width=0.15\textwidth]{images/logo.png}}
\setbeamertemplate{caption}{\insertcaption\par}
\begin{document}

% https://ned.ipac.caltech.edu/level5/Birkinshaw/Birk4_1.html
\frame{\titlepage}


\begin{frame}
    \frametitle{Introduction}
    \framesubtitle{The kSZ effect}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Cosmic Microwave Background (CMB) is thermal radiation left over from the Big Bang.
                \item Variations are one part in $10^4$, highly homogeneous.
            \end{itemize}
            \begin{figure}
                \centering
                \includegraphics[width=0.8\textwidth]{images/CMB.png}
            \end{figure}
        \end{column}

        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item SZ effects blueshift CMB due to scattering off energetic electrons.
            \end{itemize}
            \begin{figure}
                \centering
                \includegraphics[width=0.5\textwidth]{images/SZpic.png}
                \caption{{\scriptsize http://astro.uchicago.edu/sza/primer.html}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Introduction}
    \framesubtitle{The SZ effects}
    \begin{itemize}
        \item SZ effects scatter off intra-cluster medium, $T \gtrsim 1\mathrm{MK}$, probe of cluster properties.
        \item Three kinds of SZ effects: 
            \begin{itemize}
                \item Thermal (tSZ)
                \item Kinetic (kSZ)
                \item Relativistic (rSZ) is perturbation on tSZ
            \end{itemize}
        \item SZ effects are 
            \begin{itemize}
                \item Redshift independent.
                \item Spectrally distinct (kSZ maximum at tSZ minimum).
                \item Subject to different systematics from many cosmological probes today.
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Introduction}
    \framesubtitle{The SZ effects}
    \begin{figure}[!h]
        \centering
        \begin{subfigure}{0.47\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/SZ.png}
        \end{subfigure}
        \begin{subfigure}{0.47\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/SZShift.png}
        \end{subfigure}
    \end{figure}

    {\scriptsize (Carlstrom et al. 2002)}
\end{frame}

\begin{frame}
    \frametitle{Introduction}
    \framesubtitle{tSZ Effect}
    \begin{itemize}
        \item When CMB photons scatter off electron temperature $T_e$, have temperature blue-shifted by
            \begin{align*}
                \frac{\Delta T_{CMB}}{T_{CMB}} &= f(\nu, T_e)\int n_e\sigma_T\frac{k_BT_e}{m_ec^2}\mathrm{d}l
            \end{align*}
            with relativistic (rSZ) corrections in $f(\nu, T_e)$ (typically few percent).

            \begin{figure}[!h]
                \centering
                \includegraphics[width=0.47\textwidth]{images/tSZ.png}
                \caption*{{\scriptsize (Abell 2319, ESA)}}
            \end{figure}

        \item Primary Use: distance determination.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Introduction}
    \framesubtitle{kSZ Effect}
    \begin{itemize}
        \item When CMB photons scatter off peculiar velocity, have temperature blue-shifted by
            \begin{align*}
                \frac{\Delta T_{CMB}}{T_{CMB}} &= -\frac{v_z}{c}\tau_e
            \end{align*}

        \item Within local group:
            \begin{figure}[!h]
                \centering
                \includegraphics[width=0.47\textwidth]{images/kszsample.png}
                \caption*{{\scriptsize (Rubin et al. 2013)}}
            \end{figure}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Introduction}
    \framesubtitle{Frontier of kSZ Cosmology}
    \begin{itemize}
        \item Velocity fields are important to measure precisely to identify cosmological models (e.g. dark energy vs. modifications to gravity).
        \item kSZ recently measured peculiar velocities in a single cluster\footnote{Sayers et. al. 2013}.
        \item Next generation of detectors will have improved sensitivity, larger surveys.
        \item Larger systematic contributions to error budget, need to understand better any/all sources.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Introduction}
    \framesubtitle{Contamination in kSZ Surveys}
    \begin{itemize}
        \item Primary source of contamination: dusty-star forming galaxies, thousands of times more luminous than Milky Way.

            \begin{figure}[!h]
                \centering
                \includegraphics[width=0.6\textwidth]{images/dsfg.jpg}
            \end{figure}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Introduction}
    \framesubtitle{Contamination in kSZ Surveys}
    \begin{itemize}
        \item Dust masks in optical, but $T_{dust} \sim 20$--$60\mathrm{K}$ emits in FIR!
        \item Small angular width $\lesssim 1''$, must be subtracted!
        \item Well described for $\gtrsim 50\mum$ by modified blackbody
        \begin{align*}
            % S(\nu|\beta,T) &= \times \left( 1 - e^{-\tau(\nu)} \right) \times B(\nu, T) & \tau(\nu) &= \left( \frac{\nu}{\nu_0} \right)^\beta \ll 1\\
            S(\nu|\beta,T) &\propto \left( \frac{\nu}{\nu_0} \right)^\beta\frac{2h\nu^{3}}{c^2}\frac{1}{e^{\frac{h\nu}{k_BT}} - 1}
        \end{align*}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Introduction}
    \framesubtitle{Research Objectives}
    \begin{itemize}
        \item Subtract contamination at $\chi^2$-set minimum 
        \item Validate against many noise realizations (no validation in existing studies)
        \item Implications of subtraction in $2\mathrm{mm}, 3.3\mathrm{mm}$ bands for kSZ detection.
    \end{itemize}
    
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Setup}
    \begin{itemize}
        \item Multiple detectors survey same area in different frequencies.
            \begin{itemize}
                \item Correlate data across frequencies to identify point sources to subtract.
            \end{itemize}
        \item Data is represented as an image in multiple frequencies, noise + signal
            \begin{align*}
                \text{signal}_\nu &= \text{noise}_\nu + \sum\limits\text{sources}_\nu\\
                &= \text{noise}_\nu + \sum\limits \text{flux}_\nu \times \text{PSF}_\nu
            \end{align*}

        \item Fit data to multiple sources, goodness-of-fit $\chi^2$ minimization.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Approach}
    \begin{itemize}
        \item \textbf{Difficulty:} noise not white, atmospheric noise\footnote{Thermal noise is approximately white but atmospheric noise is angular frequency-dependent (Sayers et. al. 2009).}.
            
        \item \textbf{Solution:} Describe $\chi^2$ in angular frequency space!

        \item Goodness of fit is described by $\chi^2$

            $$\chi^2 = \frac{\abs{\text{observed} - \text{fit}}^2}{(\text{expected deviation})^2} = \sum\limits_{\nu}^{}\frac{\abs{\text{observed} - \text{fit}}^2}{J_\nu(\vec{k})}$$

        \item Progressively build up to full model, guaranteeing optimality/understanding of systematic sources of error in subtraction.
    \end{itemize}
\end{frame}

% \begin{frame}
%     \frametitle{Methods}
%     \framesubtitle{Approach}
%     \begin{itemize}
%         \item From explicit $\chi^2$ expression, analytically compute best estimator, uncertainty.
%         \item Compare to distribution of estimator over many noise realisations.
%     \end{itemize}
% \end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Outline}
    \begin{itemize}
        \item Single source, single frequency
        \item Multiple source, single frequency
        \item Multiple frequency, fixed SED
        \item \only<1>{Multiple frequency, Rayleigh-Jeans}\only<2>{\textbf{Multiple frequency, Rayleigh-Jeans}}
        \item \only<1>{Multiple frequency, Greybody}\only<2>{\textbf{Multiple frequency, Greybody}}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency}
    \begin{itemize}
        \item Goal:
            \begin{figure}
                \centering
                \begin{subfigure}{0.3\textwidth}
                    \includegraphics[width=\textwidth]{images/single_beam.png}
                    \caption{Turn this}
                \end{subfigure}
                \begin{subfigure}{0.3\textwidth}
                    \includegraphics[width=\textwidth]{images/noise.png}
                    \caption{into this.}
                \end{subfigure}
            \end{figure}
        \item Flux, sub-pixel position estimation with multiple sources.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency}
    \begin{itemize}
        \item Position Estimation:
            \begin{itemize}
                \item Convolve with optimal filter $\to$ nearest pixel max
                    \begin{figure}[!h]
                        \centering
                        \begin{subfigure}{0.3\textwidth}
                            \centering
                            \includegraphics[width=\textwidth]{images/pre_conv.png}
                        \end{subfigure}
                        ~
                        \begin{subfigure}{0.3\textwidth}
                            \centering
                            \includegraphics[width=\textwidth]{images/post_conv.png}
                        \end{subfigure}
                    \end{figure}
                \item Fit log to quadratic (Gaussian) for sub-pixel accuracy.
            \end{itemize}
        \item Flux by minimizing $\chi^2$ (linear parameter = closed form).
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency}
    \begin{figure}
        \centering
        \begin{subfigure}{0.3\textwidth}
            \centering
            \includegraphics[width=\textwidth]{plots/10000-1.pdf}
            \caption{Single Source, non-white}
        \end{subfigure}
        \begin{subfigure}{0.3\textwidth}
            \centering
            \includegraphics[width=\textwidth]{plots/x0hist-white.pdf}
            \caption{Non-white noise}
        \end{subfigure}
        \begin{subfigure}{0.3\textwidth}
            \centering
            \includegraphics[width=\textwidth]{plots/1000-20.pdf}
            \caption{Multiple Source, white}
        \end{subfigure}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Multiple frequency}
    \begin{figure}[!h]
        \centering
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/sig0.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/sig1.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/sig2.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/sig3.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/sig4.png}
        \end{subfigure}
        ~
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/noise0.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/noise1.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/noise2.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/noise3.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/noise4.png}
        \end{subfigure}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Multiple frequency}
    \begin{itemize}
        \item On the whole, defining $\chi^2$ sum over bands as goodness-of-fit works.

        \item \textbf{Problem:} Position estimation subtlety: shape of convolved map isn't Gaussian!

        \item \textbf{Solution:} Weighted sum of best position estimators in each band.
            
        \item \textbf{Problem:} At low SNR, lower bands lock onto noise instead of signal.

        \item \textbf{Solution:} Use $\chi^2$ to reject if individual band deviations bigger than expected.

            \begin{figure}[!h]
                \begin{subfigure}{0.3\textwidth}
                    \includegraphics[width=\textwidth]{images/conv_tot.png}
                    \caption{Sum of convolutions}
                \end{subfigure}
                \begin{subfigure}{0.3\textwidth}
                    \includegraphics[width=\textwidth]{images/conv_band4.png}
                    \caption{Convolution in $2\mathrm{mm}$.}
               \end{subfigure}
            \end{figure}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Multiple frequency}
    \begin{itemize}
        \item Known spectral energy density (SED).
    \begin{figure}
        \centering
        \begin{subfigure}{0.4\textwidth}
            \centering
            \includegraphics[width=\textwidth]{plots/pos.pdf}
        \end{subfigure}
        \begin{subfigure}{0.4\textwidth}
            \centering
            \includegraphics[width=\textwidth]{plots/amp.pdf}
        \end{subfigure}
        \caption{Multiple, multi-band sources.}
    \end{figure}
    \end{itemize}
    % discuss issue about how fitting fails when not sure of shape!
        % Formalism of weighted averages tells us how to proceed
    % Show off histograms
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Rayleigh-Jeans SED}
    \begin{itemize}
        \item Now, assume SED unknown.
        \item In $h\nu \ll kT$, approximate
            \begin{align*}
                S(\nu|\beta,T) \propto \left( \frac{\nu}{\nu_0} \right)^\beta \nu^2
            \end{align*}
            and so $\chi^2 = \chi^2(A, \vec{x}_0, \beta)$.
        \item $\beta$--$A$ covary, degrades estimation!
        \item Can analytically compute Hessian of $\chi^2$ surface to obtain shape of histogram ($e^{-\chi^2}$ is likelihood).
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Rayleigh-Jeans SED}
    \begin{itemize}
        \item Histogram + Gaussian fit
            \begin{figure}
                \centering
                \begin{subfigure}{0.3\textwidth}
                    \centering
                    \includegraphics[width=\textwidth]{plots/hist_betaamp.pdf}
                \end{subfigure}
                \begin{subfigure}{0.3\textwidth}
                    \centering
                    \includegraphics[width=\textwidth]{plots/gauss_betaamp.pdf}
                \end{subfigure}
            \end{figure}

        \item Histogram fit matches $\chi^2$ analytical calculation to within $1\%$!

        \item We can characterize the subtraction uncertainty in R-J limit.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Greybody SED}
    \begin{itemize}
        \item Observing at $\frac{h\nu}{k_B} \sim 20\mathrm{K}$, sources $20$--$60\mathrm{K}$ \emph{plus redshift}, not R-J!

        \item General greybody SED $S(\nu|\beta,T)$.

        \item \textbf{Problem:} Bad, non-Gaussian degeneracies in $\beta$--$T$, e.g. at SNR20
            \begin{figure}[!h]
                \centering
                \includegraphics[width=0.6\textwidth]{plots/beta_t_degen.png}
            \end{figure}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Greybody SED}
    \begin{itemize}
        \item \textbf{Solution:} While SEDs degenerate, are fluxes per band? Look at fluxes in each band.

        \item Multi-band flux uncertainty at \emph{fixed} SED can be predicted by formalism.
            \begin{table}[H]
                \centering
                \begin{tabular}{l|l l}
                    Band & Multi-band Uncertainty & Single-band Uncertainty\\\hline
                    750$\mum$ & 0.0531 & 0.0905\\
                    850$\mum$ &0.0421 & 0.0685\\
                    1.1$\mum$ &0.0250 & 0.0560\\
                    1.3$\mum$ &0.0174 & 0.0473\\
                    2$\mathrm{mm}$ &0.0064 & 0.0250\\
                    3$\mathrm{mm}$ &0.0019 & 0.0120
                \end{tabular}
            \end{table}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Greybody SED}
    \begin{itemize}
        \item Freeing SED gives too much complexity to compute analytically.

        \item Work-in-progress suggests flux uncertainties dependent on SNR! e.g.

            \begin{figure}[!h]
                \centering
                \begin{subfigure}{0.4\textwidth}
                    \centering
                    \includegraphics[width=\textwidth]{plots/pivot_frac_uncerts.png}
                \end{subfigure}
                \begin{subfigure}{0.4\textwidth}
                    \centering
                    \includegraphics[width=\textwidth]{plots/pivot_abs_uncerts.png}
                \end{subfigure}
            \end{figure}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Where next?}
    \begin{itemize}
        \item Non-fixed inserted source properties, look at resulting systematics if fixing various things.
        \item B\'ethermin catalog, confusion noise.
    \end{itemize}
    % Characterizing confusion noise by putting in realistic source distribution
        % specifically, examine ability to identify sources as function of amplitude in presence of confusion noise, instrumental noise
    % inserting SZ effect
\end{frame}

\begin{frame}
    \frametitle{Acknowledgements}
    \begin{itemize}
        \item Prof. Sunil R. Golwala --- For his infinite patience and illuminating guidance.
        \item Dr. Jack Sayers, Ted Macioce --- For persistent assistance of all forms.
        \item Caltech SFP/SURF program --- For initially funding this of research.
    \end{itemize}
\end{frame}

% \begin{frame}
%     \frametitle{References}
%     {\small
%     \begin{itemize}
%         \item \url{http://astro.uchicago.edu/sza/primer.html}

%         \item Sayers, J., S. R. Golwala, P. A. R. Ade, J. E. Aguirre, J. J. Bock, S. F. Edgington, J. Glenn, A. Goldin, D. Haig, A. E. Lange, G. T. Laurent, P. D. Mauskopf, H. T. Nguyen, and P. Rossinot. "Studies of Atmospheric Noise on Mauna Kea at 143 GHz with Bolocam." \emph{Millimeter and Submillimeter Detectors and Instrumentation for Astronomy IV} (2008): n. pag. Web.

%         \item Sayers, J., T. Mroczkowski, M. Zemcov, P. M. Korngut, J. Bock, E. Bulbul, N. G. Czakon, E. Egami, S. R. Golwala, P. M. Koch, K.-Y. Lin, A. Mantz, S. M. Molnar, L. Moustakas, E. Pierpaoli, T. D. Rawle, E. D. Reese, M. Rex, J. A. Shitanishi, S. Siegel, and K. Umetsu. "A Measurement Of The Kinetic Sunyaev-Zel'dovich Signal Toward Macs J0717.5 3745." \emph{ApJ The Astrophysical Journal} 778.1 (2013): 52. Web.
% \end{itemize}}
% \end{frame}

\begin{frame}
    \frametitle{Questions?}
    \begin{center}
        {\Large Questions?}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{xkcd}
    \begin{figure}[!h]
        \centering
        \includegraphics[width=0.7\textwidth]{images/xkcd_thesis.png}
    \end{figure}
\end{frame}

\end{document}

