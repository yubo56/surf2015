    \documentclass[xcolor=dvipsnames]{beamer}
    \usepackage{amsmath, amsthm, amssymb, lmodern, graphicx, wasysym, upgreek}
    \usetheme{Madrid}
    \usefonttheme{professionalfonts}
    \newcommand{\scinot}[2]{#1\times10^{#2}}
    \newcommand{\bra}[1]{\left<#1\right|}
    \newcommand{\ket}[1]{\left|#1\right>}
    \newcommand{\dotp}[2]{\left<\left.#1\right|#2\right>}
    \newcommand{\rd}[2]{\frac{\mathrm{d}#1}{\mathrm{d}#2}}
    \newcommand{\pd}[2]{\frac{\partial#1}{\partial#2}}
    \newcommand{\rtd}[2]{\frac{\mathrm{d}^2#1}{\mathrm{d}#2^2}}
    \newcommand{\ptd}[2]{\frac{\partial^2 #1}{\partial#2^2}}
    \newcommand{\norm}[1]{\left|\left|#1\right|\right|}
    \newcommand{\abs}[1]{\left|#1\right|}
    \newcommand{\mum}[0]{\upmu \mathrm{m}}
    \newcommand{\pvec}[1]{\vec{#1}^{\,\prime}}
    \newcommand{\tensor}[1]{\overleftrightarrow{#1}}
    \let\Re\undefined
    \let\Im\undefined
    \newcommand{\ang}[0]{\text{\AA}}
    \newcommand{\mum}[0]{\upmu \mathrm{m}}
    \DeclareMathOperator{\Re}{Re}
    \DeclareMathOperator{\Tr}{Tr}
    \DeclareMathOperator{\Im}{Im}
    \newcommand{\expvalue}[1]{\left<#1\right>}
    \everymath{\displaystyle}
    \usepackage[font=scriptsize]{subcaption}

\title[E10 Presentation]{The Kinetic Sunyaev-Zel'dovich Effect}
\author[Yubo Su]{Yubo Su\inst{1}}\\
\institute[Caltech]{\inst{1}California Institute of Technology}
\date{May 26 2016}
\logo{\includegraphics[width=0.15\textwidth]{images/logo.png}}
\setbeamertemplate{caption}{\insertcaption\par}
\begin{document}

% https://ned.ipac.caltech.edu/level5/Birkinshaw/Birk4_1.html
\frame{\titlepage}


\begin{frame}
    \frametitle{The Cosmic Microwave Background}

    \begin{itemize}
        \item Cosmic Microwave Background (CMB) is thermal radiation left over from the Big Bang.
        \item Variations are one part in $10^4$, highly homogeneous ($0.01\mathrm{K}$ per room temperature).
    \end{itemize}
    \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{images/CMBTimeline.jpg}
    \end{figure}
\end{frame}
\begin{frame}
    \frametitle{The Cosmic Microwave Background}
    \begin{itemize}
        \item CMB as measured by WMAP mission.
    \end{itemize}

    \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{images/CMB.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Cosmic Microwave Background}
    \begin{itemize}
        \item Very well understood!
    \end{itemize}
    \begin{figure}[!h]
        \centering
        \begin{subfigure}{0.45\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/CMB_blackbody.png}
        \end{subfigure}
        \begin{subfigure}{0.45\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/CMBPowerSpectrum.png}
        \end{subfigure}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Galaxy Clusters}
    \begin{itemize}
        \item Hundreds/thousands of galaxies bound together by gravity.
        \item Center of superheated, $10^{6}\mathrm{K}$ gas.
    \end{itemize}
    \begin{figure}[!h]
        \centering
        \includegraphics[width=0.7\textwidth]{images/GalaxyCluster.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The SZ Effect}

    \begin{itemize}
        \item SZ effects blueshift CMB due to scattering off energetic electrons.
    \end{itemize}
    \begin{figure}
        \centering
        \includegraphics[width=0.5\textwidth]{images/SZpic.png}
        \caption{{\scriptsize http://astro.uchicago.edu/sza/primer.html}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The SZ effect}
    \begin{itemize}
        \item SZ effects scatter off intra-cluster medium, $T \gtrsim 1\mathrm{MK}$, probe of cluster properties.
        \item Three kinds of SZ effects: 
            \begin{itemize}
                \item Thermal (tSZ)
                \item Kinetic (kSZ)
                \item Relativistic (rSZ) is perturbation on tSZ
            \end{itemize}
        \item SZ effects are 
            \begin{itemize}
                \item Redshift independent.
                \item Spectrally distinct (kSZ maximum at tSZ minimum).
                \item Subject to different systematics from many cosmological probes today.
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{The SZ effects}
    \begin{figure}[!h]
        \centering
        \begin{subfigure}{0.47\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/SZ.png}
        \end{subfigure}
        \begin{subfigure}{0.47\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/SZShift.png}
        \end{subfigure}
    \end{figure}

    {\scriptsize (Carlstrom et al. 2002)}
\end{frame}

\begin{frame}
    \frametitle{tSZ Effect}
    \begin{itemize}
        \item When CMB photons scatter off electron temperature $T_e$, have temperature blue-shifted by
            \begin{align*}
                \frac{\Delta T_{CMB}}{T_{CMB}} &= f(\nu, T_e)\int n_e\sigma_T\frac{k_BT_e}{m_ec^2}\mathrm{d}l
            \end{align*}
            with relativistic (rSZ) corrections in $f(\nu, T_e)$ (typically few percent).

            \begin{figure}[!h]
                \centering
                \includegraphics[width=0.47\textwidth]{images/tSZ.png}
                \caption*{{\scriptsize (Abell 2319, ESA)}}
            \end{figure}

        \item Primary Use: temperature/pressure profiles.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{kSZ Effect}
    \begin{itemize}
        \item When CMB photons scatter off peculiar velocity, have temperature blue-shifted by
            \begin{align*}
                \frac{\Delta T_{CMB}}{T_{CMB}} &= -\frac{v_z}{c}\tau_e
            \end{align*}

        \item Within local group:
            \begin{figure}[!h]
                \centering
                \includegraphics[width=0.47\textwidth]{images/kszsample.png}
                \caption*{{\scriptsize (Rubin et al. 2013)}}
            \end{figure}
        \item Primary use: peculiar velocities.
    \end{itemize}
\end{frame}
\end{document}

