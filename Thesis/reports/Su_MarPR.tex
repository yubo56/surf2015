    \documentclass[10pt]{article}
    \usepackage{fancyhdr, amsmath, amsthm, amssymb, mathtools, lastpage, hyperref, enumerate, graphicx, setspace, wasysym, upgreek, pdflscape}
    \usepackage[margin=0.5in, top=0.8in,bottom=0.8in]{geometry}
    \newcommand{\scinot}[2]{#1\times10^{#2}}
    \newcommand{\bra}[1]{\left<#1\right|}
    \newcommand{\ket}[1]{\left|#1\right>}
    \newcommand{\dotp}[2]{\left<\left.#1\right|#2\right>}
    \newcommand{\rd}[2]{\frac{\mathrm{d}#1}{\mathrm{d}#2}}
    \newcommand{\pd}[2]{\frac{\partial#1}{\partial#2}}
    \newcommand{\rtd}[2]{\frac{\mathrm{d}^2#1}{\mathrm{d}#2^2}}
    \newcommand{\ptd}[2]{\frac{\partial^2 #1}{\partial#2^2}}
    \newcommand{\norm}[1]{\left|\left|#1\right|\right|}
    \newcommand{\abs}[1]{\left|#1\right|}
    \newcommand{\pvec}[1]{\vec{#1}^{\,\prime}}
    \newcommand{\tensor}[1]{\overleftrightarrow{#1}}
    \let\Re\undefined
    \let\Im\undefined
    \newcommand{\ang}[0]{\text{\AA}}
    \newcommand{\mum}[0]{\upmu \mathrm{m}}
    \DeclareMathOperator{\Re}{Re}
    \DeclareMathOperator{\Tr}{Tr}
    \DeclareMathOperator{\Im}{Im}
    \newcommand{\expvalue}[1]{\left<#1\right>}
    \usepackage[labelfont=bf, font=scriptsize]{caption}\usepackage{tikz}
    \usepackage[font=scriptsize]{subcaption}
    \everymath{\displaystyle}

\tikzstyle{circle} = [draw, circle, fill=white, node distance=3cm, minimum height=2em]

\begin{document}

\title{Simulation Pipeline for Velocity Field Measurements using the Kinetic Sunyaev–--Zel'dovich Effect}

\author{Yubo Su\\
Mentor: Prof. Sunil R. Golwala}

\date{March 11, 2016}

\maketitle

\section*{Review}

The goal of the project is to derive from first principles an algorithm for source subtraction in multi-frequency band astronomical data. A thorough examination of the subtraction accuracy is performed each time more complex SED profiles are considered to guarantee that the analytic description captures all observed behavior accurately. The ultimate goal is to understand how source subtraction systematics manifest in kSZ detection limits. This requires an accurate characterization of the systematics the procedure induces.

At the last presentation, the description of the source subtraction for a Rayleigh-Jeans power-law approximation to the full grey-body SED was in progress. I present below data in support of the complete analytical model and accompanying software implementation. Furthermore, I discuss our ongoing efforts to characterize the phenomena that emerge when considering a grey-body SED.

\section{Completion of four-variable subtraction of point source fluxes}

Recall that in the $T \to \infty$ limit, a black body spectral energy distribution (SED) with temperature $T$ may be approximated by $S_\nu \propto \nu^{2 + \beta}$ for some dimensionless emissivity $\beta$. The source profile to be subtracted is then described by four parameters, $A$ some flux density, $\beta$ some emissivity and $\vec{x}_0$ the source location in the map. A subtraction procedure seeks estimates of these parameters $\hat{A}, \hat{\beta}, \tilde{x}_0$ to minimize the goodness-of-fit \eqref{eq:4var}.

\begin{align}
    \chi^2(\hat{A}, \hat{\beta}, \tilde{x}_0) &= \sum\limits_{\nu}^{} \iint \mathrm{d}^2\vec{\nu} \frac{\abs{\tilde{v}_\nu(\vec{\nu}) - \hat{A}\left(\frac{\nu}{\nu_0}\right)^{2+\hat{\beta}}e^{-2\pi j \tilde{x}_0 \cdot \vec{\nu}}\tilde{s}_\nu(\vec{\nu})}^2}{J_\nu(\vec{\nu})}\label{eq:4var}
\end{align}
where $\tilde{v}_\nu$ is the Fourier transform of the map in frequency band indexed  by $\nu$, $J_\nu$ is the power spectral density (PSD) in frequency band $\nu$, $\nu_0$ is the optically thick cutoff frequency\footnote{I have made the optically thin approximation $1 - e^{-\tau} \approx \tau = \left( \frac{\nu}{\nu_0} \right)^\beta$} and $\tilde{s}_\nu$ is the peak-normalized known beam profile in frequency band $\nu$ (diffraction limited point spread function $\sigma \propto L^{-1}$ for $L$ aperture size is assumed). The goodness-of-fit is computed in frequency space due to spatial correlations that are described by a $J_\nu$ that is not constant in spatial wavevector $\vec{\nu}$.

Since $\chi^2$ has a nonlinear dependence on $\beta$, a numerical search for the solution is required. IDL's truncated Newton minimization routine \texttt{tnmin} was used to perform this parameter search.

In order to understand whether this search saturates uncertainty limits on parameter estimation, we recall that the $\chi^2$ surface can be expanded about its minimum as
\begin{align}
    \chi^2(\theta) - \chi^2_{min} = \theta^T C^{-1} \theta + \mathcal{O}(\theta^4)\label{chi2}
\end{align}
with $C$ the covariance matrix and $\theta$ the model parameters. Given the analytical form of the $\chi^2$ above, it is possible to compute the entries $[C^{-1}]_{ij}$ and thus theoretical variances and covariances of $\theta$. It is also possible to generate a parameter grid mesh, sample the $\chi^2$ and fit a quadratic to compute the curvature that is expected to agree with the theory \eqref{chi2}. Finally, by generating many noise realizations, inserting sources of known parameters $\theta$ and applying the subtraction algorithm to obtain estimators $\hat{\theta}$, it is possible to produce a histogram of deviations $\theta - \hat{\theta}$. The width and orientation of this histogram should be roughly Gaussian with widths predicted by the covariance matrix entries $C$ above. 

\autoref{tab:4vartab} summarizes the successful agreement of all three of these procedures. Note that the parameter scales are chosen in arbitrary units and the procedure is applied in an unrealistically high signal-to-noise ratio (SNR) regime. This is to facilitate interpretation of the data where the $\chi^2$ is well-approximated as quadratic.

\section{Grey Body SED Progress}

The power-law approximation is only valid in the limit $h\nu \ll k_BT$, which for redshifted dust temperatures $T \in [5, 60]\mathrm{K}$ and observational wavelengths $\sim 2\mathrm{mm}$ is barely invalid. The full grey body SED is described by the following modification to \eqref{chi2} in \eqref{eq:GBchi2} below.
\begin{align}
    \chi^2_{GB}(\hat{A}, \hat{\beta}, \hat{T}, \tilde{x}_0) &= \sum\limits_{\nu}^{} \iint \mathrm{d}^2\vec{\nu} \frac{\abs{\tilde{v}_\nu(\vec{\nu}) - A\frac{\left( \frac{\nu}{\nu_0} \right)^\beta\nu^3}{e^{\frac{h\nu}{kT}} - 1}e^{-2\pi j \vec{x}_0 \cdot \vec{\nu}}\tilde{s}_\nu(\vec{\nu})}^2}{J_\nu(\vec{\nu})} \label{eq:GBchi2}
\end{align}

While the $\chi^2$ surface is quadratic when only considered as a function of $\hat{\beta}$, horrible degeneracies emerge among the $\hat{T}, \hat{\beta}$ estimators. To better understand how significant these degeneracies are when considering real source subtractions, realistic source parameters are adopted and a representative SNR of $5$ in the highest frequency band is assumed. Then, when looking at the $\chi^2_{GB}$ surface about $\chi^2_{GB, min}$ and varying only $\hat{\beta}, \hat{T}$, we observe the following shape degeneracy shown partially in \autoref{fig:c}.
\begin{figure}[!h]
    \centering
    \includegraphics[width=0.7\textwidth]{PRimages/c.png}
    \caption{Contour plot of $\chi^2_{GB}(\hat{T} / \hat{T}_{min}, \hat{\beta} / \hat{\beta}_{min})$. Each contour line is $\Delta \chi^2 = 0.23$ corresponding to $\sigma/10$ deviations from the minimizing parameter values.}
    \label{fig:c}
\end{figure}

Note that the $\Delta \chi^2 = 2.3$, or the $1\sigma$ contour extends far beyond the scope of the contour plot. This implies that parameter values drastically different from the inserted source parameters will produce indistinguishable fits to the data. An example of these indistinguishable SEDs is given in \autoref{fig:ab} for a map containing an inserted source with parameters $\beta = 1.5, T = 17\mathrm{K}$. The fluxes in each frequency band, along with their random uncertainties, are overlaid with the inserted SED and two SEDs within $\sigma/10$ ($\Delta \chi^2 \lesssim 0.23)$ of the best-estimate $\hat{\beta}_{min}, \hat{T}_{min}$.
\begin{figure}[!h]
    \centering
    \begin{subfigure}{0.4\textwidth}
        \centering
        \includegraphics[width=\textwidth]{PRimages/a.png}
        \caption{Within observational frequency band.}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
        \centering
        \includegraphics[width=\textwidth]{PRimages/b.png}
        \caption{Over wider frequency band.}
    \end{subfigure}
    \caption{Three SEDs within $\Delta \chi^2 \lesssim 2.3$ or $\sigma/10$ of one another. The red points with error bars are the fluxes in each frequency along with their random uncertainties, and the three SEDs are $\beta = 1.25, T = 40\mathrm{K}$ in red, $\beta = 1.5, T = 17\mathrm{K}$ in green (middle) and $\beta = 1.7, T = 12\mathrm{K}$ in blue (bottom).}
    \label{fig:ab}
\end{figure}

\section{Future Work}

Instead of continuing to try to develop techniques to probe the minimum of this degenerate $\chi^2$ surface, I will examine possible reparameterizations of the SED such that in transformed coordinates the degeneracy appears more ellipsoidal. Such a parameterization, while it would not give any better precision beyond the noise limit, would be a more fundamental description of the problem. The elongated shape of the $\chi^2$ surface would likely be preserved in any such mapping, and so we expect to describe a precise estimate of our constraining power over one parameter and a complete lack of precision on the other. Techniques to realise this description are ongoing.

As this reparameterization is investigated, work will begin to improve the accuracy of the gradient descent (\texttt{tnmin} does not properly descend to the true minimum on such a degenerate surface) with an eye to extending this approach to multiple sources and probing the confusion limit of subtraction.

\newgeometry{margin=1in}
\begin{landscape}
    \begin{table}
        \centering {\small
        \begin{tabular}{c|l l l|l l l|l l l}
            Var1, Var2 & $\sigma_1$ (h) &
            $\sigma_1$ ($\chi^2$) &
            $\sigma_1$ (t) &
            $\sigma_2$ (h) &
            $\sigma_2$ ($\chi^2$) &
            $\sigma_2$ (t) &
            $\mathrm{Covar}$ (h) &
            $\mathrm{Covar}$ ($\chi^2$) &
            $\mathrm{Covar}$ (t)\\[2pt]\hline

            $\hat{\beta}$, $\hat{A}$ &
            $12.10\pm 0.10 $&
            $11.82\pm 0.11$&
            $11.879 $&
            $6.128 \pm  0.049 $&
            $6.087\pm 0.046$&
            $6.0875 $&
            $5.46\pm 0.1 $&
            $5.28\pm 1.6\mathrm{E}{-3}$&
            $5.31\pm 2\mathrm{E}{-3} $\\[2pt]
            $\hat{A}$, $\hat{x}_0$ &
            $4.111 \pm  0.036$ &
            $4.146\pm 6\mathrm{E}{-5}$ &
            $4.137$ &
            $3.772 \pm  0.034$ &
            $3.698\pm 0.023$&
            $3.691$&&&\\[2pt]
         
            $\hat{\beta}$, $\hat{x}_0$ &
            $8.171 \pm  0.075 $&
            $8.043\pm 0.046$&
            $8.074 $&
            $3.711\pm 0.034 $&
            $3.696\pm 0.023$&
            $3.691$&&&\\[2pt]
         
            $\hat{x}_0$, $\hat{y}_0$ &
            $3.778 \pm  0.041 $&
            $3.702 \pm 0.023$ &
            $3.691 $&
            $3.720 \pm  0.040 $&
            $3.702\pm  0.023$ &
            $3.691$&&&\\[2pt]
         
            $\hat{A}$, $\hat{y}_0$ &
            $4.107\pm 0.037 $&
            $4.146\pm  1\mathrm{E}{-4}$&
            $4.137 $&
            $3.738\pm 0.034 $&
            $3.698 \pm 0.023$&
            $3.691$&&&\\[2pt]
         
         
            $\hat{\beta}$, $\hat{y}_0$ &
            $8.254 \pm  0.073 $&
            $8.043 \pm 0.046$ &
            $8.074 $&
            $3.720\pm 0.033 $&
            $3.696\pm 0.023$ &
            $3.691$
        \end{tabular}}
        \caption{$\hat{A}$ reported in units of $10^{-4}$ fractional units of total flux, $\hat{x}_0, \hat{y}_0$ reported in $10^{-3}$ pixels, $\beta$ reported in $10^{-3}$ dimensionless units. Covariance is reported in units of $10^{-7}$ fractional flux units. Theoretical, $\chi^2$ fit and histogram parameter values are labeled (t), ($\chi^2$), (h) respecively. Note the close accord among the three methods. Covariances for other pairs of parameters than $\beta, A$ were consistent with $0$ as expected and are omitted.  Realistic unit choices were not considered here; an unrealistic SNR was taken to maximize quadratic nature of $\chi^2$ about minimum.}
        \label{tab:4vartab}
    \end{table}
\end{landscape}
\restoregeometry

\end{document}

