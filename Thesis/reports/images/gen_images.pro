pro gen_images
; generate some pretty pictures!

range = 256

; single band images
specdens = spdensgen(range, 0.1, 3, 8.0/3, 1)
noise = gennoise(specdens, 1)
sig = addgauss(0.5, 5, 127.3, 127.3, noise)
sig2 = addfunc(noise, 0.5, 5, 15)
max_val = max([noise, sig, sig2.signal])
i = image(noise, margin=0, max_value=max_val, /buffer, aspect_ratio=1.0)
i.save, 'noise.png', border=0
i = image(sig, margin=0, max_value=max_val, /buffer, aspect_ratio=1.0)
i.save, 'single_beam.png', border=0
i = image(sig2.signal, margin=0, max_value=max_val, /buffer, aspect_ratio=1.0)
i.save, 'multi_beam.png', border=0

range = 64
; multi band images
freqs = [400, 352.94, 272.73, 230.77, 150] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, $
    [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, $
    replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2
noise = gennoise_multi(specdens0, 1, freqs)
sig = addgauss_multi(0.5, 5, range/2 + 0.5, range/2 + 0.5, noise)
max_val = max(sig.signal)
for i=0, num_bands - 1 do begin
    j = image(noise.signal[*,*,i], margin=0, max_value=max_val, /buffer, aspect_ratio=1.0)
    j.save, strcompress('noise' + string(i) + '.png', /remove_all), border=0
    j = image(sig.signal[*,*,i], margin=0, max_value=max_val, /buffer, aspect_ratio=1.0)
    j.save, strcompress('sig' + string(i) + '.png', /remove_all), border=0
endfor

end
