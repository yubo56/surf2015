    \documentclass[11pt]{article}
    \usepackage{fancyhdr, amsmath, amsthm, amssymb, mathtools, lastpage, hyperref, enumerate, graphicx, setspace, wasysym, upgreek}
    \usepackage[margin=0.5in, top=0.8in,bottom=0.8in]{geometry}
    \newcommand{\scinot}[2]{#1\times10^{#2}}
    \newcommand{\bra}[1]{\left<#1\right|}
    \newcommand{\ket}[1]{\left|#1\right>}
    \newcommand{\dotp}[2]{\left<\left.#1\right|#2\right>}
    \newcommand{\rd}[2]{\frac{\mathrm{d}#1}{\mathrm{d}#2}}
    \newcommand{\pd}[2]{\frac{\partial#1}{\partial#2}}
    \newcommand{\rtd}[2]{\frac{\mathrm{d}^2#1}{\mathrm{d}#2^2}}
    \newcommand{\ptd}[2]{\frac{\partial^2 #1}{\partial#2^2}}
    \newcommand{\norm}[1]{\left|\left|#1\right|\right|}
    \newcommand{\abs}[1]{\left|#1\right|}
    \newcommand{\pvec}[1]{\vec{#1}^{\,\prime}}
    \newcommand{\tensor}[1]{\overleftrightarrow{#1}}
    \let\Re\undefined
    \let\Im\undefined
    \newcommand{\ang}[0]{\text{\AA}}
    \newcommand{\mum}[0]{\upmu \mathrm{m}}
    \DeclareMathOperator{\Re}{Re}
    \DeclareMathOperator{\Tr}{Tr}
    \DeclareMathOperator{\Im}{Im}
    \newcommand{\expvalue}[1]{\left<#1\right>}
    \usepackage[labelfont=bf, font=scriptsize]{caption}\usepackage{tikz}
    \usepackage[font=scriptsize]{subcaption}
    \everymath{\displaystyle}

\tikzstyle{circle} = [draw, circle, fill=white, node distance=3cm, minimum height=2em]

\begin{document}

\title{Simulation Pipeline for Velocity Field Measurements using the Kinetic Sunyaev–--Zel'dovich Effect}

\author{Yubo Su\\
Mentor: Prof. Sunil R. Golwala}

\date{October 09, 2015}

\maketitle

\section{Motivation}

The kinetic Sunyaev-Zel'dovich (kSZ) effect is a scattering of cosmic microwave background (CMB) photons by the bulk motion of electrons in the interstellar medium. Only recently observationally detected, the kSZ effect is expected to provide exciting results for cosmology as detectors continue to improve in sensitivity. 

% kSZ effect is scattering of CMB photons by electrons' bulk motion
% Only statistically detected, new generations of instruments will push sensitivity to levels useful for cosmology

\subsection{Peculiar Velocity Measurements}

Precise line-of-sight peculiar velocity $v_z$ measurements yield tight constraints on cosmological parameters and allow for differentiation between competing cosmological models. For instance, such measurements have been used to constrain cosmological parameters like the total matter density $\Omega_m$ and the normalization of density fluctuations $\sigma_8$. 

Modern $v_z$ measurements are made using a combination of spectroscopy and distance measurements, in which uncertainties are proportional to the distance to the source. In contrast, kSZ measurements provide a direct measurement of $v_z$. Moreover, the kSZ is independent of redshift, dependent only on $v_z$ and $\tau_e$ the electron optical depth. While spectroscopy-based measurements have improved to competitive accuracy with projected kSZ sensitivity, the two techniques exhibit vastly different systematics that can be combined to produce tighter, more robust constraints.

Predicted kSZ sensitivity for the next generation of detectors is expected to yield strong limits on dark energy via the mean pairwise velocity. Studies also show that kSZ velocity measurements would be highly competitive with modern tests for deviations from general relativity while probing very different mass scales and exhibiting different systematic uncertainties. Finally, the kSZ signal can be used to measure internal motions of the intra-cluster medium (ICM), helping to pinpoint properties and structures of the ICM that are currently poorly understood and supplementing contemporary results in a redshift-independent manner.

% Do cosmology with peculiar velocity fields
    % \Omega_m + \sigma_8
    % dark matter, energy constraints, deviations GR.
    % internal cluster motions, 
% better than spectroscopy
    % uncertainties in spec proportional to distance
    % independent of redshift, depends only on v_z, \tau_e
    % different systematics

\subsection{Current and Future kSZ Measurements}

Recent kSZ detections have demonstrated both the feasibility and difficulty of future kSZ measurements. Studies have used the kSZ signal from \emph{WMAP} and \emph{Planck} to place upper limits on bulk flows and rms variations in $v_z$. More recently, ACT and Sloan Digital Sky Survey III data have been used to constrain the mean pairwise momentum of clusters by identify a kSZ signal inconsistent with noise at a confidence level of 99.8\%. The most significant kSZ measurement to date was able to constrain $v_z = 3450 \pm 900\mathrm{km/s}$ for one of the sub-clusters of MACS J0717.5+3745.

As various in-progress and projected sky surveys reach adequate sensitivity to use kSZ measurements for constraining cosmological parameters, previously unimportant sources of systematic error become non-negligible compared to noise levels. It is then important to understand the various sources of systematic error that contaminate a kSZ signal. For instance, a kSZ measurement requires subtraction of point sources from data, introducing systematic uncertainties currently not well understood. Additionally, the kSZ effect is a second-order pertubation to the CMB; the first-order thermal SZ (tSZ) effect is an order of magnitude stronger, and the relativistic corrections (rSZ) to the tSZ effect are of the same strength as the kSZ effect. As all three effects occupy the same spectral frequency range, systematic errors will be induced by separating the three and must also be characterized. To fully understand the effects of these systematic sources of error, a full-scale simulation is required.

% Current kSZ measurements met with success
    % bulk flows + rms of v_z
    % mean pairwise momentum, 99.8\%
    % Bolocam + SPIRE, constrain single cluster v_z 3450 \pm 900
% Various in-progress and projected sky surveys reach kSZ sensitivity
% random error down, -> need better understand of systematics
    % e.g. point source subtraction?
        % what systematics induced by point source subtraction?
        % will influence SZ detection?
    % separation of various SZ signals
        % tSZ, kSZ, rSZ all in same frequency band
        % tSZ order mag larger, kSZ rSZ same mag
% to understand full effect of all systematics, need full-scale simulation to optimize and predict 

\section{Proposal}

A simulation to study the systematic sources of error contributing to a kSZ measurement comprises three components: generating fake sky maps; collecting data from these maps, accounting for instrumental defects; and processing the simulated data to extract the kSZ effect. We propose to complete the third component of this simulation: \textbf{we propose to finish developing and implementing the formalism for point source subtraction in sky maps and to investigate the impacts of systematic uncertainties produced both by this subtraction and by the separation of the tSZ, kSZ and rSZ effects}.

% Simulation pipeline must: gen fake sky, gen instruments, process collected data to extract SZ effect
% We propose to finish: subtraction of point source, separation of SZ effect; third part

\subsection{Work to Date}

To date, we have completed a SURF developing and implementing the formalism for subtracting point sources from sky maps. The implementation is tested by generating many random map realisations and comparing the rms of the estimated source characteristics over many trials to the theoretically predicted values. Currently, the theory and implementation for maps generated in only a single spectral frequency are complete and tested. Work remains to fully understand the difficulties encountered in generalizing the procedure to multiple maps in different spectral frequencies. We have already successfully tested multiple frequency subtraction for known spectral energy distributions (SEDs) of sources. We are also close to understanding subtraction when the SEDs are parameterized as a Raleigh-Jeans or grey body spectrum with unknown parameters that differ among sources.

% Describe point source work from SURF
    % everything pinned down and characterized for single frequency
        % Derived theory for subtraction, implemented testing framework to make many map realisations and compare biasedness and variance of estimators to theory
        % Identified and explained various deviations from initial expectations
    % multi-band 
        % have testing framework in place
        % for fixed spectral frequency profile, agree with theory
        % beginning characterizing power law (RJ), grey body cases, few unexplained phenomena currently in progress

\subsection{Proposed Work and Timeline}

In the fall term of 2015, we propose to complete the extension of our procedure to the general grey body spectrum with unknown parameters. We anticipate difficulties in recovering the grey body parameters from multiple frequency maps because the source parameters are strongly degenerate, but existing studies have recommendations on how to handle such obstacles. After this generalization is understood, we will apply the procedure to maps generated with realistic source distributions from literature to understand how subtraction quality varies as a function of source amplitude. This will be important to characterize the impact of ``confusion noise,'' noise that arises not due to instrumental effects but from sources too dim to be identified and subtracted. It will also provide a bound on the minimum amplitude a source needs to have to be identified and subtracted confidently. Upon understanding these effects the point source subtraction will be complete. 

In the winter term of 2016, we will begin to treat point sources in the simulated maps as galaxy clusters and simulate various SZ effects. We will then develop and implement a procedure to separate the SZ effects and in particular identify the kSZ signal. This will be complicated by the fact that the tSZ and kSZ signals are not correlated in map space, only in spectral frequency space, and the rSZ signal will have an uncertain profile even in spectral frequency space. We will investigate the applications of principal component analysis and possibly other techniques from literature in separating these signals. The procedure will be tested in the same fashion as was the point source subtraction, by generating numerous map realisations and comparing the rms of the estimates to the theory.

In the spring term of 2016, we will combine the two procedures and apply them to numerous map realisations to characterize their cumulative impact on the kSZ signal. By using realistic characteristics for point source brightness, galaxy cluster characteristics and stochastic noise we will be able to make meaningful predictions about kSZ signal fidelity and prescribe how to optimize measurement precision.
% First term
    % Finish point source subtraction: RJ, grey body characterization
    % Determine subtraction accuracy as function of inserted amplitude with a realistic source distribution
        % realistic source distribution taken from simulations in literature
% Second term
    % Insert clusters, SZ effects
    % develop SZ identification algorithm
% Third term
    % analyze results of application of algorithm

\end{document}

