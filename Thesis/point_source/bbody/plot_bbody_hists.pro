pro plot_bbody_hists, FN
; sample script to pretty plot results from bbody (beta + tdust) histograms
; XPARAM          DOUBLE           127.70192
; YPARAM          DOUBLE           127.30112
; AEST            DOUBLE          0.99854191
; SIGM_X0         DOUBLE        0.0035347829
; SIGM_A          DOUBLE       0.00038415992
; CHI2            DOUBLE           329158.60
; EMISSIVITY      DOUBLE           1.5027582
; TDUST           DOUBLE           38.878437
; NUM_ITERS       LONG                 3
; SIGM_BETA       DOUBLE       0.00070085076
; SIGM_TDUST      DOUBLE         0.012644513
; DBETA           DOUBLE          -3895.2530
; DT_DUST         DOUBLE          -4.4201264


restore, FN
pos_filename=FN + '.ps'

; clean data
emissivities = emissivities[where(emissivities)] ; where returns nonzero indicies
tdusts = tdusts[where(tdusts)]

; theory values
sigm_beta = 0.00070085076
sigm_tdust = 0.012644513

; do fits
hist_beta = histogram(emissivities, binsize=stddev(emissivities)/6, locations = bins_beta)
gfit_beta = gaussfit(bins_beta, hist_beta, coeff_beta, nterms=3)
hist_tdust = histogram(tdusts, binsize=stddev(tdusts)/6, locations = bins_tdust)
gfit_tdust = gaussfit(bins_tdust, hist_tdust, coeff_tdust, nterms=3)



; gets correct dimensions for plot
set_plot, 'X'
device, decomposed=0
tek_color
window, 0, xsize=800, ysize=400
pageInfo = pswindow()
cleanplot, /silent
wdelete

; plot beta
set_plot, 'PS'
device, _Extra = pageInfo, /color, filename=pos_filename, language=2
!P.MULTI = [0, 2, 1] ; grids window into top, bottom [win_number, cols, rows]
plot, bins_beta, hist_beta, charsize=0.5, psym=10, xtitle='Emissivity',$
    ;xrange=[coeff_beta[1] - 5 * coeff_beta[2], coeff_beta[1] + 5 * coeff_beta[2]],$
    thick=2, xthick=2, ythick=2
oplot, bins_beta, gfit_beta, color=2, thick=6
; put xparams on plot
plot_xpos = !X.CRANGE[0] + 0.05 * (!X.CRANGE[1] - !X.CRANGE[0]); coordinates for top left corner
plot_ypos = !Y.CRANGE[0] + 0.9 * (!Y.CRANGE[1] - !Y.CRANGE[0])
str = 'mean = ' + string(format = '(G8.5,"!C")', coeff_beta[1])
str = str + 'rms = ' + string(format = '(E11.4, "!C")', coeff_beta[2])
str = str + 'theory = ' + string(format = '(E11.4)', sigm_beta)
xyouts, /data, plot_xpos, plot_ypos, str, charsize=0.5

; plot tdust
!P.MULTI = [1, 2, 1] ; grids window into top, bottom [win_number, cols, rows]
plot, bins_tdust, hist_tdust, charsize=0.5, psym=10, xtitle='Tdust (K)',$
    ;xrange=[coeff_tdust[1] - 5 * coeff_tdust[2], coeff_tdust[1] + 5 * coeff_tdust[2]], $
    thick=2, xthick=2, ythick=2
oplot, bins_tdust, gfit_tdust, color=2, thick=6
; put xparams on plot
plot_xpos = !X.CRANGE[0] + 0.05 * (!X.CRANGE[1] - !X.CRANGE[0]); coordinates for top left corner
plot_ypos = !Y.CRANGE[0] + 0.9 * (!Y.CRANGE[1] - !Y.CRANGE[0])
str = 'mean = ' + string(format = '(G8.5,"!C")', coeff_tdust[1])
str = str + 'rms = ' + string(format = '(E11.4, "!C")', coeff_tdust[2])
str = str + 'theory = ' + string(format = '(E11.4)', sigm_tdust)
xyouts, /data, plot_xpos, plot_ypos, str, charsize=0.5

; close file
device, /close_file



; restore plotting method
set_plot, 'X'

print, coeff_beta[2], sigm_beta
print, coeff_tdust[2], sigm_tdust
end
