pro char_covar
; characterize covariances using same calculations as chi_surf but with
; analytic estimates

emissivity = 1.5
tdust = indgen(75 - 7) + 7 ; temperatures to use
amp = 1
num_samples = n_elements(tdust)

fracs = findgen(num_samples) ; store fractional covariance
angles = findgen(num_samples) ; store angles

for i=0, num_samples - 1 do begin
    ; beta, tdust, covar
    vars = calc_vars(emissivity, tdust[i], amp)
    varmat = [[vars[0], vars[2]], [vars[2], vars[1]]]

    fracs[i] = sqrt(vars[2] / vars[0]) * sqrt(vars[2] / vars[1])

    evals = hqr(elmhes(varmat), /double)
    evecs = real_part(eigenvec(varmat, evals))
    angles[i] = atan((evecs[0,0] * emissivity) /$
        (evecs[0,1] * tdust[i])) / !dtor ; reciprocals of what we needed
endfor

p = plot(tdust, fracs, font_size=18)
p[0].title='covar/max_covar vs tdust, analytical'
a = p.axes
a[0].title='tdust'
a[1].title='covar'
p.save, 'images/analcovars.png', resolution=500
p.close

p = plot(tdust, angles, font_size=18)
p[0].title='rotation angle vs tdust, analytical'
a = p.axes
a[0].title='tdust'
a[1].title='angle'
p.save, 'images/analangle.png', resolution=500
stop
end
