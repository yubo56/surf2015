pro gencontscent, in_a
; input amp should be total flux of pt source in mJy
; plot chi^2 1-sigma contours out to 20sigma in each direction
; centered on GD minimum.

range = 256D ; map size
binwidth = 480 / range ; units of arcsec, total width 480 arcsec
sigm = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
sigm /= binwidth ; sigm in bins
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(0,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2
amp = in_a / (2 * !PI * sigm^2)  ; I = A * 2 * !PI * sigm^2, sigm in number bins

num_pts = 100.0
num_pts_beta = 20.0
numavg = 1
chi2s_bt = dblarr(num_pts_beta + 1, num_pts + 1)
sfit_bt = []
vars = []
angles = []
tdust = indgen(4) * 10 + 17 ; temperatures to use
emis = 1.5D
xpos = 63
ypos = 63

for i=0, n_elements(tdust) - 1 do begin
    ; noise realization to compute chi2 contours for
    noise = gennoise_multi(specdens0, binwidth, freqs)
    sig = addgauss_multi(amp, sigm, xpos, ypos, noise, emis, tdust[i], /bbody)

    ; plot about min of GD
    ret = bbody_multi(sig, specdens0, sigm, binwidth, autoder=1)
    emishat = ret.emissivity
    tdusthat = ret.tdust
    print, emis, tdust[i], emishat, tdusthat
    sigms = sqrt(calc_vars(emishat, tdusthat, amp))
    x_tdust = tdusthat + (findgen(num_pts + 1) - num_pts/2.0) * sigms[1] * 20 / num_pts
	x_beta = emishat + (findgen(num_pts_beta + 1) - num_pts_beta/2.0) * sigms[0] * 4 / num_pts_beta
    betarange = max(x_beta) - min(x_beta)
    tdustrange = max(x_tdust) - min(x_tdust)
    
    k_bt = findgen(6)  ; store six average elements
	for rep=1, numavg do begin
        for j=0, num_pts_beta do begin
            for k=0, num_pts do begin
                ; update for cuts
                ret = subtractknown_multi(sig, specdens0, sigm, binwidth, x_beta[j], x_tdust[k], real_pos=[xpos, ypos], real_amp=amp, /bbody)
                chi2s_bt[j,k] = ret.chi2
            endfor
        endfor
        res_bt = sfit_p(reform(chi2s_bt), 2, kx=k_bt2, /max_degree)
        k_bt += k_bt2
    endfor
    k_bt /= numavg
    ret = getcovars(k_bt[2], 0, k_bt[4], 0, k_bt[5], 0, sigms[0] / 3, sigms[1] / 3)
    temp = min(chi2s_bt, loc)
    print, 'Min at: ', to2d(num_pts + 1, loc)
    area = n_elements(where(chi2s_bt - min(chi2s_bt) - 1 < 0)) ; compute num pts
    area *= (20 / num_pts)^2 * sigms[0] / emis * sigms[1] / tdust[i] ; each pt has binwidth 20/numpts * sigma / true val
    print, 'Within 1 chi^2: ', area
    temp = contour(chi2s_bt,$
        x_beta / emishat,$
        x_tdust / tdusthat,$
        C_VALUE=min(chi2s_bt) + findgen(23)/10,$
        xtitle=strcompress('beta (' + string(emishat) + ')'),$
        ytitle=strcompress('tdust (' + string(tdusthat) + ')'),$
        COLOR='black',$
        title='Chi^2 surface at ' + strcompress(string(tdust[i]) + 'K'))
    stop
    temp.save, strcompress('images/surf' + string(tdust[i]) + '.png', /remove_all)
    temp.close
endfor
end
