pro chi_surf, in_a=in_a
; input amp should be total flux of pt source in mJy
; fit chi2 surfaces for tdust-beta out to 3 * sigma

; map parameters parameters
range = 256D ; map size
binwidth = 480 / range ; units of arcsec, total width 480 arcsec
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(0,num_bands), replicate(8.0/3, num_bands), binwidth) ; PSD in units of mJy^2

; source parameters
if ~keyword_set(in_a) then in_a = 0.181/2 * 5; default SNR 5:1
sigm = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
sigm /= binwidth ; sigm in bins
amp = in_a / (2 * !PI * sigm^2)  ; I = A * 2 * !PI * sigm^2, sigm in number bins

; simulation parameters
num_pts = 10.0
numavg = 1
tdust = indgen(10) * 5 + 13 ; temperatures to use

; declarations
chi2s_bt = dblarr(num_pts + 1, num_pts + 1)
sfit_bt = []
vars = []
angles = []
emis = 1.5D
xpos = 127
ypos = 127

for i=0, n_elements(tdust) - 1 do begin
    ; gen realization
    noise = gennoise_multi(specdens0, binwidth, freqs)
    sig = addgauss_multi(amp, sigm, xpos, ypos, noise, emis, tdust[i], /bbody)

    ; set up parameter search, center on GD values
    ret = bbody_multi(sig, specdens0, sigm, binwidth, 1.5, 15, autoder=1)
    emishat = ret.emissivity
    tdusthat = ret.tdust
    print, emis, tdust[i], emishat, tdusthat
    sigms = sqrt(calc_vars(emishat, tdusthat, amp))
    x_tdust = tdusthat + (findgen(num_pts + 1) - num_pts/2.0) * sigms[1] * 3 / num_pts ; sample out to 3sigma
	x_beta = emishat + (findgen(num_pts + 1) - num_pts/2.0) * sigms[0] * 3 / num_pts
    betarange = max(x_beta) - min(x_beta)
    tdustrange = max(x_tdust) - min(x_tdust)
    
    k_bt = findgen(6)  ; store six average elements
	for rep=1, numavg do begin
        for j=0, num_pts do begin
            for k=0, num_pts do begin
                ; update for cuts
                ret = subtractknown_multi(sig, specdens0, sigm, binwidth, x_beta[j], x_tdust[k], real_pos=[xpos, ypos], real_amp=amp, /bbody)
                chi2s_bt[j,k] = ret.chi2
            endfor
        endfor
        res_bt = sfit_p(reform(chi2s_bt), 2, kx=k_bt2, /max_degree)
        k_bt += k_bt2
    endfor
    k_bt /= numavg
    betastep = betarange / (n_elements(x_beta) - 1)
    tstep = tdustrange / (n_elements(x_tdust) - 1)
    ret = getcovars(k_bt[2], 0, k_bt[4], 0, k_bt[5], 0, betastep, tstep)
    ; get angles from eigenvector components
    evals = hqr(elmhes(ret.inv), /double)
    evecs = real_part(eigenvec(ret.inv, evals))
    angles = [angles, atan(evecs[0,1]/evecs[0,0]) / !dtor]
    vars = [[vars], [ret.inv[0,0], ret.inv[1,0], ret.inv[1,1]]]
    save, vars, angles, tdust, filename='chi_surf.sav'
endfor


maxcovars = sqrt(vars[0,*] * vars[2,*])
p = plot(tdust, abs(vars[1,*]) / maxcovars, font_size=18)
p[0].title='covar/max_covar vs tdust'
a = p.axes
a[0].title='tdust'
a[1].title='covar'
p.save, 'images/max_vs_actualcovars.png', resolution=500
p.close

p = plot(tdust, angles, font_size=18)
p[0].title='rotation angle vs tdust'
a = p.axes
a[0].title='tdust'
a[1].title='angle'
p.save, 'images/rotationangle.png', resolution=500
p.close
end
