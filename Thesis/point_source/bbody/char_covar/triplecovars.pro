pro triplecovars, SNR, tdust
; About chi2 minimum, generates chi2 cuts about beta-tdust, beta-amp, tdust-amp

range = 256D ; map size
binwidth = 480 / range ; units of arcsec, total width 480 arcsec
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(0,num_bands), replicate(8.0/3, num_bands), binwidth) ; PSD in units of mJy^2

; source parameters
sigm = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
sigm /= binwidth ; sigm in bins
a = sqrt((mean(specdens0[*,*,0]) / 2) / (!PI * sigm^2)) * SNR
emis = 1.5D
xpos = range/2 + 0.3
ypos = range/2 + 0.5

; normalize amplitude
amps_norm = amps_multi(1, freqs, emis, td, /bbody)
a /= amps_norm[0]

; simulation parameters
num_pts = 24.0



; make one realization
noise = gennoise_multi(specdens0, binwidth, freqs)
sig = addgauss_multi(a, sigm, xpos, ypos, noise, emis, tdust, /bbody)

; get best fit parameters
retbb = bbody_multi(sig, specdens0, sigm, binwidth, emis, tdust)
emishat = retbb.emissivity
tdusthat = retbb.tdust
rethat = subtractmax_multi(sig, specdens0, sigm, binwidth, emishat, tdusthat, /bbody)

while 1 do begin ; loop so can set different parameter scans
    ; simulation variables
    chi2s_bt = dblarr(num_pts + 1, num_pts + 1)
    chi2s_at = dblarr(num_pts + 1, num_pts + 1)
    chi2s_ba = dblarr(num_pts + 1, num_pts + 1)
    x_tdust = tdusthat + (findgen(num_pts + 1) - num_pts / 2.0) * 20 / num_pts
    x_tdust -= min([0, min(x_tdust)]) ; starts at 0
    x_beta = emishat + (findgen(num_pts + 1) - num_pts / 2.0) * 1.5 / num_pts
    x_beta -= min([0, min(x_beta)])
    x_amp = rethat.aest + (findgen(num_pts + 1) - num_pts / 2.0) * 0.1 / num_pts
    x_amp -= min([0, min(x_amp)])
    print, tdusthat, emishat, rethat.aest ; goto ref point
    stop

    for j=0, num_pts do begin
        for k=0, num_pts do begin
            ; update for cuts
            ret = subtractknown_multi(sig, specdens0, sigm, binwidth, x_beta[j], x_tdust[k], real_pos=[rethat.xparam, rethat.yparam], real_amp=rethat.aest, /bbody)
            chi2s_bt[j,k] = ret.chi2

            ; amp-tdust
            ret = subtractknown_multi(sig, specdens0, sigm, binwidth, betahat, x_tdust[k], real_pos=[rethat.xparam, rethat.yparam], real_amp=x_amp[j], /bbody)
            chi2s_at[j,k] = ret.chi2

            ; beta-amp
            ret = subtractknown_multi(sig, specdens0, sigm, binwidth, x_beta[j], tdusthat, real_pos=[rethat.xparam, rethat.yparam], real_amp=x_amp[k], /bbody)
            chi2s_ba[j,k] = ret.chi2
        endfor
    endfor

    c = contour(chi2s_bt, x_beta, x_tdust,$
        title=strcompress('Minimizing params:' + string(emishat) + ', ' + string(tdusthat)),$
        ytitle='Tdust',$
        xtitle='Beta',$
        C_VALUE=min(chi2s_bt) + [0.1, 0.5, 1.0, 2.3],$
        COLOR='black')
    stop
    c.close
    c = contour(chi2s_at, x_amp, x_tdust,$
        title=strcompress('Minimizing params:' + string(rethat.aest) + ', ' + string(tdusthat)),$
        ytitle='TDust',$
        xtitle='Amp',$
        C_VALUE=min(chi2s_at) + [0.1, 0.5, 1.0, 2.3],$
        COLOR='black')
    stop
    c.close
    c = contour(chi2s_ba, x_beta, x_amp,$
        title=strcompress('Minimizing params:' + string(emishat) + ', ' + string(rethat.aest)),$
        ytitle='Amp',$
        xtitle='Beta',$
        C_VALUE=min(chi2s_ba) + [0.1, 0.5, 1.0, 2.3],$
        COLOR='black')
    stop
    c.close
endwhile
end
