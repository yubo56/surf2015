pro chi_ellipse, in_a=in_a, tdust=tdust
; input amp should be total flux of pt source in mJy
; Overlay chi2 surface with ellipse of theoretical uncert, out to 3sigma

; map parameters parameters
range = 256D
binwidth = 480 / range
freqs = [400, 352.94, 272.73, 230.77, 150, 91] ; frequencies in GHz
num_bands = n_elements(freqs)

convrms = [0.181, 0.137, 0.112, 0.0947, 0.05, 0.024] / 2
sigm = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
sigm /= binwidth ; sigm in bins
if ~keyword_set(in_a) then in_a = convrms[0] * 20 / sqrt(!PI * sigm^2); SNR 5
specdens0 = spdensgen_multi(range, $
    convrms, $ ; convert noise/beam -> noise/pixel
    replicate(8,num_bands), replicate(8.0/3, num_bands), binwidth) ; PSD in units of mJy^2

; source parameters
if ~keyword_set(tdust) then tdust = 15; default tdust 15K

; simulation parameters
num_pts = 24.0

; declarations
chi2s_bt = dblarr(num_pts + 1, num_pts + 1)
emis = 1.5D
xpos = range/2 + 0.3
ypos = range/2 + 0.5
samplesigma = 10

; noise realization to compute chi2 contours for
noise = gennoise_multi(specdens0, binwidth, freqs)
sig = addgauss_multi(in_a, sigm, xpos, ypos, noise, emis, tdust, /bbody)

; get minimum
amp = in_a
ret = bbody_multi(sig, specdens0, sigm, binwidth, emis, tdust)
emishat = ret.emissivity
tdusthat = ret.tdust
rethat = subtractmax_multi(sig, specdens0, sigm, binwidth, emishat, tdusthat, /bbody)

; theoretical rotation, for ellipse plotting
thvars = [[rethat.sigm_beta^2, rethat.covar_bt], [rethat.covar_bt , rethat.sigm_tdust^2]]
thvals = hqr(elmhes(thvars), /double)
thvecs = real_part(eigenvec(thvars, thvals))
thangle = atan((thvecs[0,1] / emishat) / (thvecs[0,0] / tdusthat)) / !dtor
thsigms = sqrt(real_part(sqrt(thvals)))

; get bt surface
x_tdust = tdusthat + (findgen(num_pts + 1) - num_pts / 2.0) * 20 / num_pts
x_beta = emishat + (findgen(num_pts + 1) - num_pts / 2.0) * 1.5 / num_pts
stop

for j=0, num_pts do begin
    for k=0, num_pts do begin
        ; update for cuts
        ret = subtractknown_multi(sig, specdens0, sigm, binwidth, x_beta[j], x_tdust[k], real_pos=[rethat.xparam, rethat.yparam], real_amp=rethat.aest, /bbody)
        chi2s_bt[j,k] = ret.chi2
    endfor
endfor
; print, !PI * thsigms[0] * thsigms[1] ; area of ellipse

stop
; print, min(chi2s_bt, loc)
; plot
c = contour(chi2s_bt, x_beta, x_tdust,$
    title=strcompress('Inserted params:' + string(emis) + ', ' + string(tdust) + ', Minimizing params:' + string(emishat) + ', ' + string(tdusthat)),$
    ytitle='Tdust',$
    xtitle='Beta',$
    C_VALUE=min(chi2s_bt) + [0.1, 0.5, 1.0, 2.3],$
    C_LABEL_SHOW=0,$
    COLOR='black')
; print, to2d(num_pts + 1, loc)
; e = ellipse(1, 1, MAJOR=max(thsigms), MINOR=min(thsigms), theta=thangle, FILL_COLOR='red', /data)
c.save, 'images/thesisellipse' + strcompress(string(tdust) + 'K.png', /remove_all)
c.close
c = contour(chi2s_bt, x_beta / emishat, x_tdust / tdusthat,$
    title=strcompress('Inserted params:' + string(emis) + ', ' + string(tdust) + ', Minimizing params:' + string(emishat) + ', ' + string(tdusthat)),$
    ytitle='Fractional Tdust',$
    xtitle='Fractional Beta',$
    C_VALUE=min(chi2s_bt) + [0.1, 0.5, 1.0, 2.3],$
    COLOR='black')
c.save, 'images/newellipsefrac' + strcompress(string(tdust) + 'K.png', /remove_all)
stop
end
