pro histdatacut
; histograms of the various trials that we've run in parallel
; for i in $('ls' *.ps); do convert $i $i.png; rm -f $i; done
; for i in 0 1 2 3 4 5; do convert "$i"a.ps.png "$i"p.ps.png +append "$i"-c.png; rm "$i"a.ps.png "$i"p.ps.png -f; done


sigms_p = [0.03171, 0.02220, 0.01020, 0.00601, 0.00144, 0.00036]
sigms = [0.04800, 0.03750, 0.02577, 0.02036, 0.00864, 0.00247]

restore, '5cut.sav'
; sizePivs = size(pivot_amps)
; amps = reform(amps, [sizePivs[1], sizePivs[2]])
; save, amps, pivot_amps, filename='5cut.sav'

temp = where(amps[0,*] - amps[0,*], complement=inds) ; inds stores where not NaN's
for i=0,5 do begin
    hist_wrap, 2 * amps[i,inds], ps_file=strcompress(string(i) + 'a-c.ps', /REMOVE_ALL), theory=sigms[i] * 2, xtitle='Flux Estimator (mJy)'
    hist_wrap, 2 * pivot_amps[i,inds], ps_file=strcompress(string(i) + 'p-c.ps', /REMOVE_ALL), theory = sigms_p[i] * 2, xtitle='Flux Estimator (mJy)'
endfor

end
