pro histdata
; histograms of the various trials that we've run in parallel
; filenames refers to SNR=5
; for i in $('ls' *.ps); do convert $i $i.png; rm -f $i; done
; for i in 0 1 2 3 4 5; do convert "$i"a.ps.png "$i"p.ps.png +append "$i".png; rm "$i"a.ps.png "$i"p.ps.png -f; done
; rm 5-*.sav -f

; cumamps = []
; cumpivs = []

; ; restore, '5-1.sav'
; sizePivs = size(pivot_amps)
; amps = reform(amps, [sizePivs[1], sizePivs[2]])
; cumamps = [[cumamps], [amps]]
; cumpivs = [[cumpivs], [pivot_amps]]
; restore, '5-2.sav'
; sizePivs = size(pivot_amps)
; amps = reform(amps, [sizePivs[1], sizePivs[2]])
; cumamps = [[cumamps], [amps]]
; cumpivs = [[cumpivs], [pivot_amps]]
; restore, '5-3.sav'
; sizePivs = size(pivot_amps)
; amps = reform(amps, [sizePivs[1], sizePivs[2]])
; cumamps = [[cumamps], [amps]]
; cumpivs = [[cumpivs], [pivot_amps]]
; restore, '5-4.sav'
; sizePivs = size(pivot_amps)
; amps = reform(amps, [sizePivs[1], sizePivs[2]])
; cumamps = [[cumamps], [amps]]
; cumpivs = [[cumpivs], [pivot_amps]]
; restore, '5-5.sav'
; sizePivs = size(pivot_amps)
; amps = reform(amps, [sizePivs[1], sizePivs[2]])
; cumamps = [[cumamps], [amps]]
; cumpivs = [[cumpivs], [pivot_amps]]
; restore, '5-6.sav'
; sizePivs = size(pivot_amps)
; amps = reform(amps, [sizePivs[1], sizePivs[2]])
; cumamps = [[cumamps], [amps]]
; cumpivs = [[cumpivs], [pivot_amps]]
; restore, '5-7.sav'
; sizePivs = size(pivot_amps)
; amps = reform(amps, [sizePivs[1], sizePivs[2]])
; cumamps = [[cumamps], [amps]]
; cumpivs = [[cumpivs], [pivot_amps]]
; restore, '5-8.sav'
; sizePivs = size(pivot_amps)
; amps = reform(amps, [sizePivs[1], sizePivs[2]])
; cumamps = [[cumamps], [amps]]
; cumpivs = [[cumpivs], [pivot_amps]]

; ; forgot to save this, recompute it
; range = 256D
; bw = 480 / range
; emis = 1.7
; td = 13
; freqs = [400, 352.94, 272.73, 230.77, 150, 100] ; frequencies in GHz
; num_bands = n_elements(freqs)
; sd = spdensgen_multi(range, $
;     [0.181, 0.137, 0.112, 0.0947, 0.049, 0.009] / 2, $
;     replicate(0,num_bands), replicate(8.0/3, num_bands), bw) ; PSD in units of 0.01 mJy^2
; s = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
; s /= bw ; sigm in bins
; a = sqrt((0.181 / 2) / (!PI * s^2)) * 5 ; ref 2D.tex for why this is 5SNR in peak band
; noise = gennoise_multi(sd, bw, freqs)
; sig = addgauss_multi(a, s, range/2 + 0.5, range/2 + 0.5, noise, emis, td, /bbody, normband=0)
; ret = subtractmax_multi(sig,sd,s,bw,emis,td,/bbody)
; while ret.sigm_a ne ret.sigm_a do begin ; keep going until not nan
;     noise = gennoise_multi(sd, bw, freqs)
;     sig = addgauss_multi(a, s, range/2 + 0.5, range/2 + 0.5, noise, emis, td, /bbody, normband=0)
;     ret = subtractmax_multi(sig,sd,s,bw,emis,td,/bbody) 
; endwhile
; sigms_p = amps_multi(ret.sigm_a, freqs, emis, td, /bbody) ; take sigm_a in leading band and scale down for sigm_pivots

restore, '5.sav'
for i=0,5 do begin
    hist_wrap, cumamps[i,*], ps_file=strcompress(string(i) + 'a.ps', /REMOVE_ALL), theory=sigms[i]
    hist_wrap, cumpivs[i,*], ps_file=strcompress(string(i) + 'p.ps', /REMOVE_ALL), theory=sigms_p[i]
endfor

save, cumamps, cumpivs, sigms, sigms_p, filename='5.sav'
end
