pro hist_cat, FN
; gets SEDs for various fits to examine catastrophes observed in hist_varsrc
; store A - A_in / sigma_band in each band, beta & tdust (in/out) for each 

range = 256D
bw = 480 / range
emis = 1.7
td = 26
freqs = [400, 352.94, 272.73, 230.77, 150, 90.91] ; frequencies in GHz
num_bands = n_elements(freqs)

convrms = [0.181, 0.137, 0.112, 0.0947, 0.050, 0.024] / 2
s = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
s /= bw ; sigm in bins
a = convrms[0] * 5 / sqrt(!PI * s^2); SNR 5
sd = spdensgen_multi(range, $
    convrms, $ ; convert noise/beam -> noise/pixel
    replicate(0,num_bands), replicate(8.0/3, num_bands), bw) ; PSD in units of mJy^2

in_a = []

single_a = [] ; stores (A - A_in) / sigma_band
fix_a = []
emis_fix_a = []
td_fix_a = []
free_a = []

params_single_a = [] ; [in_beta, in_tdust, aest, xparam, yparam]
params_fix_a = [] ; [aest, xparam, yparam]
params_emis_fix_a = [] ; [out_tdust, aest, xparam, yparam]
params_td_fix_a = [] ; [out_beta, aest, xparam, yparam]
params_free_a = [] ; [out_beta, out_tdust, aest, xparam, yparam]
sigms = !NULL

for i=1, 100000 do begin
    noise = gennoise_multi(sd, bw, freqs)
    newemis = max([emis + randomn(SEED) * 0.3, 0.5])
    newtd = max([td + randomn(SEED) * 7, 5])
    sig = addgauss_multi(a, s, range/2 + 0.5, range/2 + 0.5, noise, newemis, newtd, /bbody, normband=0)
    in_a_one = amps_multi(a, freqs, newemis, newtd, /bbody)
    in_a = [[[in_a]], [in_a_one]]
    params_single_a = [[[params_single_a]], [newemis, newtd]]

    ; 5 different subtractions
    ret0 = subtractmax_multi(sig, sd, s, bw, emis, td, /bbody)
    ret1 = bbody_multi(sig, sd, s, bw, emis, td, /emis_fix)
    ret2 = bbody_multi(sig, sd, s, bw, emis, td, /td_fix)
    ret3 = bbody_multi(sig, sd, s, bw, emis, td)
    sed = getsedpts(sig, sd, s, bw, [ret0.xparam, ret0.yparam])

    ; get 5 sets of amplitudes
    single_a = [[[single_a]], [sed.amps]]
    if ret0.aest eq ret0.aest then begin ; not NaN
        fix_a_one = amps_multi(ret0.aest, freqs, emis, td, /bbody)
        fix_a = [[[fix_a]], [(fix_a_one - in_a_one) / sed.sigms]]
        params_fix_a = [[[params_fix_a]], [ret0.aest, ret0.xparam, ret0.yparam]]
    endif
    if ret1.tdust ne -1 then begin ; converged on smth
        emis_fix_a_one = amps_multi(ret1.aest, freqs, emis, ret1.tdust, /bbody)
        emis_fix_a = [[[emis_fix_a]], [(emis_fix_a_one - in_a_one) / sed.sigms]]
        params_emis_fix_a = [[[params_emis_fix_a]], [ret1.tdust, ret1.aest, ret1.xparam, ret1.yparam]]
    endif
    if ret2.tdust ne -1 then begin
        td_fix_a_one = amps_multi(ret2.aest, freqs, ret2.emissivity, td, /bbody)
        td_fix_a = [[[td_fix_a]], [(td_fix_a_one - in_a_one) / sed.sigms]]
        params_td_fix_a = [[[params_td_fix_a]], [ret2.emissivity, ret2.aest, ret2.xparam, ret2.yparam]]
    endif
    if ret3.tdust ne -1 then begin
        free_a_one = amps_multi(ret3.aest, freqs, ret3.emissivity, ret3.tdust, /bbody)
        free_a = [[[free_a]], [(free_a_one - in_a_one) / sed.sigms]]
        params_free_a = [[[params_free_a]], [ret3.emissivity, ret3.tdust, ret3.aest, ret3.xparam, ret3.yparam]]
    endif

    if sigms eq !NULL then sigms = sed.sigms
    save, single_a, fix_a, emis_fix_a, td_fix_a, free_a,$
        params_single_a, params_fix_a, params_emis_fix_a,$
        params_td_fix_a, params_free_a,$
        sigms, freqs, in_a, filename=FN
    print, i
endfor

end
; print, single_a[*,0], fix_a[*,0], emis_fix_a[*,0], td_fix_a[*,0], free_a[*,0]
; print, in_params[*,0], emis_fix_params[*,0], td_fix_params[*,0], free_params[*,0]
