pro hist_new, SNR, FN
; generates data for histogram comparing pivot approach to per-band fluxes
; inserts fixed source, considers various

range = 256D
bw = 480 / range
emis = 1.7
td = 13
freqs = [400, 352.94, 272.73, 230.77, 150, 100] ; frequencies in GHz
num_bands = n_elements(freqs)

convrms = [0.181, 0.137, 0.112, 0.0947, 0.049, 0.009] / 2
s = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
s /= bw ; sigm in bins
a = convrms[0] * SNR / sqrt(!PI * s^2); SNR
sd = spdensgen_multi(range, $
    convrms, $ ; convert noise/beam -> noise/pixel
    replicate(0,num_bands), replicate(8.0/3, num_bands), bw) ; PSD in units of mJy^2

cumamps = []
sigms = !NULL
pivot_amps = []
success = 0

for i=1, 100000 do begin
    noise = gennoise_multi(sd, bw, freqs)
    sig = addgauss_multi(a, s, range/2 + 0.5, range/2 + 0.5, noise, emis, td, /bbody, normband=0)
    ret0 = subtractmax_multi(sig, sd, s, bw, emis, td, normband=0)
    if ret0.tdust ne -1 then begin
        success += 1
        print, success, i
        if ret0.aest lt 0.02 then stop
        amps = amps_multi(ret0.aest, freqs, ret0.emissivity, ret0.tdust, /bbody)
        sed = getsedpts(sig, sd, s, bw, [ret0.xparam, ret0.yparam])

        cumamps = [[[cumamps]], [sed.amps]]
        if sigms eq !NULL then sigms = sed.sigms
        pivot_amps = [[[pivot_amps]], [amps]]

        if FN ne !NULL then save, cumamps, sigms, pivot_amps, freqs, filename=FN $
            else save, cumamps, sigms, pivot_amps, freqs, filename='hist.sav'
    endif
endfor

end
