pro histdata, FN
; histograms of the various trials that we've run in parallel
; filenames refers to SNR=5
; for i in $('ls' *.ps); do convert $i $i.png; rm -f $i; done
; for i in 0 1 2 3 4 5; do convert "$i"a.ps.png "$i"p.ps.png +append "$i".png; rm "$i"a.ps.png "$i"p.ps.png -f; done
; rm 5-*.sav -f

restore, FN
; oops messed up hist_new a bit
cumpivs = []
for i=0, n_elements(pivot_amps[0,*]) - 1 do $;if i MOD 2 eq 0 then 
    cumpivs = [[[cumpivs]], [pivot_amps[*,i]]]

for i=0,5 do begin
    ; hist_wrap, cumamps[i,*], ps_file=strcompress(FN + string(i) + 'a.ps', /REMOVE_ALL), theory=sigms[i]
    hist_wrap, cumpivs[i,*], ps_file=strcompress(FN + string(i) + 'p.ps', /REMOVE_ALL)
endfor
end


; uncertainties for pivot-based, single-band
; [0.066370085080283817, 0.037893015838200796, 0.018969089898594374, 0.011835893038358438, 0.0032119788444885896, 0.0010804450049187923]
; [0.097186816759284159, 0.059196970537043439, 0.038598377784384082, 0.027613822709152681, 0.010060837570702733, 0.0011216348257293645]
