pro hist_varsrc, SNR, FN
; generates data for histogram comparing pivot approach to per-band fluxes

range = 256D
bw = 480 / range
emis = 1.7
td = 26
freqs = [400, 352.94, 272.73, 230.77, 150, 90.9] ; frequencies in GHz
num_bands = n_elements(freqs)

convrms = [0.181, 0.137, 0.112, 0.0947, 0.049, 0.025] / 2
s = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
s /= bw ; sigm in bins
a = convrms[0] * SNR / sqrt(!PI * s^2); SNR
sd = spdensgen_multi(range, $
    convrms, $ ; convert noise/beam -> noise/pixel
    replicate(0,num_bands), replicate(8.0/3, num_bands), bw) ; PSD in units of mJy^2

in_a = []

single_a = []
fix_a = []
emis_fix_a = []
td_fix_a = []
free_a = []

sigm_single_a = []
sigm_fix_a = []
sigm_emis_fix_a = []
sigm_td_fix_a = []
sigm_free_a = []
sigms = !NULL

for i=1, 100000 do begin
    noise = gennoise_multi(sd, bw, freqs)
    newemis = max([emis + randomn(SEED) * 0.3, 0.5])
    newtd = max([td + randomn(SEED) * 7, 5])
    sig = addgauss_multi(a, s, range/2 + 0.5, range/2 + 0.5, noise, newemis, newtd, /bbody, normband=0)
    in_a = [[[in_a]], [amps_multi(a, freqs, newemis, newtd, /bbody)]]

    ; 5 different subtractions
    ret0 = subtractmax_multi(sig, sd, s, bw, emis, td, /bbody)
    ret1 = bbody_multi(sig, sd, s, bw, emis, td, /emis_fix)
    ret2 = bbody_multi(sig, sd, s, bw, emis, td, /td_fix)
    ret3 = bbody_multi(sig, sd, s, bw, emis, td)
    sed = getsedpts(sig, sd, s, bw, [ret0.xparam, ret0.yparam])

    ; get 5 sets of amplitudes
    single_a = [[[single_a]], [sed.amps]]
    sigm_single_a = [[[sigm_single_a]], [sed.sigms]]
    if ret0.aest eq ret0.aest then begin ; not NaN
        fix_a_one = amps_multi(ret0.aest, freqs, emis, td, /bbody)
        uncerts = amps_multi(ret0.sigm_a, freqs, emis, td, /bbody)
        fix_a = [[[fix_a]], [fix_a_one]]
        sigm_fix_a = [[[sigm_fix_a]], [uncerts]]
    endif
    if ret1.tdust ne -1 then begin ; converged on smth
        emis_fix_a_one = amps_multi(ret1.aest, freqs, emis, ret1.tdust, /bbody)
        uncerts = amps_multi(ret1.sigm_a, freqs, emis, ret1.tdust, /bbody)
        emis_fix_a = [[[emis_fix_a]], [emis_fix_a_one]]
        sigm_emis_fix_a = [[[sigm_emis_fix_a]], [uncerts]]
    endif
    if ret2.tdust ne -1 then begin
        td_fix_a_one = amps_multi(ret2.aest, freqs, ret2.emissivity, td, /bbody)
        uncerts = amps_multi(ret2.sigm_a, freqs, ret2.emissivity, td, /bbody)
        td_fix_a = [[[td_fix_a]], [td_fix_a_one]]
        sigm_td_fix_a = [[[sigm_td_fix_a]], [uncerts]]
    endif
    if ret3.tdust ne -1 then begin
        free_a_one = amps_multi(ret3.aest, freqs, ret3.emissivity, ret3.tdust, /bbody)
        uncerts = amps_multi(ret3.sigm_a, freqs, ret3.emissivity, ret3.tdust, /bbody)
        free_a = [[[free_a]], [free_a_one]]
        sigm_free_a = [[[sigm_free_a]], [uncerts]]
    endif

    if sigms eq !NULL then sigms = sed.sigms
    save, single_a, fix_a, emis_fix_a, td_fix_a, free_a,$
        sigm_single_a, sigm_fix_a, sigm_emis_fix_a,$
        sigm_td_fix_a, sigm_free_a,$
        sigms, freqs, in_a, filename=FN
    print, i;, ret0.chi2, ret1.chi2, ret2.chi2, ret3.chi2
endfor

end
; print, single_a[*,0], fix_a[*,0], emis_fix_a[*,0], td_fix_a[*,0], free_a[*,0]
; print, in_params[*,0], emis_fix_params[*,0], td_fix_params[*,0], free_params[*,0]
