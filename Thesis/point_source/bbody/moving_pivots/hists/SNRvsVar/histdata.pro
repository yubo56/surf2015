pro histdata, FNs
; compute stdevs for histogram estimators at various SNRs
; ['SNR10.sav',  'SNR25.sav',  'SNR45.sav',  'SNR5.sav',   'SNR7.sav', 'SNR100.sav',    'SNR15.sav',  'SNR3.sav',   'SNR4.sav',   'SNR70.sav']

freqs = [400, 352.94, 272.73, 230.77, 150, 91] ; frequencies in GHz
bw = 480/256
s = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
s /= bw ; sigm in bins
sigm_beam = sigm_multi(s, freqs)

num_files = n_elements(FNs)
stdevs_750 = []
stdevs_2m = []
SNRs = [10,  25,  45,  5,   7,  100,    15,  3,   4,   70]

for i=0, num_files - 1 do begin
    restore, FNs[i]
    hist_wrap, pivot_amps[0,*] * sigm_beam[0] * sqrt(!PI), /noplot, params=params
    stdevs_750 = [stdevs_750, params[1]]

    hist_wrap, pivot_amps[4,*] * sigm_beam[4] * sqrt(!PI), /noplot, params=params
    stdevs_2m = [stdevs_2m, params[1]]
endfor

inds = sort(SNRs)
temp = plot(SNRs[inds], stdevs_750[inds], xtitle='SNR', ytitle='Flux Uncertainty', title='750u')
stop
temp = plot(SNRs[inds], stdevs_2m[inds], xtitle='SNR', ytitle='Flux Uncertainty', title='2m')
stop
end
