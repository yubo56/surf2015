pro histdata, FNs
; histograms of the various trials that we've run in parallel
; filenames refers to SNR=5
; histdata, ['vs5-1.sav','vs5-2.sav','vs5-3.sav','vs5-4.sav']   
; histdata, ['vs10-1.sav','vs10-2.sav','vs10-3.sav','vs10-4.sav']
; for i in $('ls' *.ps); do convert $i $i.png; rm -f $i; done
; for i in 0 1 2 3 4 5; do convert "$i"single.ps.png "$i"fix.ps.png "$i"emis_fix.ps.png +append "$i"-1.png; convert "$i"td_fix.ps.png "$i"free.ps.png +append "$i"-2.png; convert "$i"-1.png "$i"-2.png -append SNR10-rel-"$i".png; rm "$i"*.ps.png "$i"-*.png -f; done

range = 256D
bw = 480 / range
s = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
s /= bw ; sigm in bins

; SINGLE_A        DOUBLE    = Array[6, *]
; FIX_A           DOUBLE    = Array[6, *]
; EMIS_FIX_A      DOUBLE    = Array[6, *]
; TD_FIX_A        DOUBLE    = Array[6, *]
; FREE_A          DOUBLE    = Array[6, *]

single_tot = [] ; single band
fix_tot = [] ; subtractmax
emis_fix_tot = [] ; bbody, emis_fix
td_fix_tot = [] ; bbody, td_fix
free_tot = [] ; bbody, no fix
for i=0, n_elements(FNs) - 1 do begin
    restore, FNs[i]
    single_tot = [[single_tot], [(single_a - in_a) / sigm_single_a]]
    fix_tot = [[fix_tot], [(fix_a - in_a) / sigm_single_a]]
    emis_fix_tot = [[emis_fix_tot], [(emis_fix_a - in_a) / sigm_single_a]]
    td_fix_tot = [[td_fix_tot], [(td_fix_a - in_a) / sigm_single_a]]
    free_tot = [[free_tot], [(free_a - in_a) / sigm_single_a]]
endfor

print, n_elements(single_tot),n_elements(fix_tot),n_elements(emis_fix_tot),n_elements(td_fix_tot),n_elements(free_tot)
for i=0,5 do begin
    print, i
    ; hist_wrap, cumamps[i,*], ps_file=strcompress(FN + string(i) + 'a.ps', /REMOVE_ALL), theory=sigms[i]
    hist_wrap, single_tot[i,*], ps_file=strcompress(string(i) + 'single.ps', /REMOVE_ALL), title='Single', /ylog
    hist_wrap, fix_tot[i,*], ps_file=strcompress(string(i) + 'fix.ps', /REMOVE_ALL), title='Fixed', /ylog
    hist_wrap, emis_fix_tot[i,*], ps_file=strcompress(string(i) + 'emis_fix.ps', /REMOVE_ALL), title='Emis Fixed', /ylog
    hist_wrap, td_fix_tot[i,*], ps_file=strcompress(string(i) + 'td_fix.ps', /REMOVE_ALL), title='Tdust Fixed', /ylog
    hist_wrap, free_tot[i,*], ps_file=strcompress(string(i) + 'free.ps', /REMOVE_ALL), title='Free both', /ylog
endfor
end
