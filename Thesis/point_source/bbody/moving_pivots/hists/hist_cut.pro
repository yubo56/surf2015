pro hist_cut, SNR, FN
; generates data for histogram comparing pivot approach to per-band fluxes
; at constant b, tdust to check A distributions

range = 256D
bw = 480 / range
emis = 1.7
td = 13
freqs = [400, 352.94, 272.73, 230.77, 150, 100] ; frequencies in GHz
num_bands = n_elements(freqs)

sd = spdensgen_multi(range, $
    [0.181, 0.137, 0.112, 0.0947, 0.049, 0.009] / 2, $
    replicate(0,num_bands), replicate(8.0/3, num_bands), bw) ; PSD in units of 0.01 mJy^2
s = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
s /= bw ; sigm in bins
a = sqrt((0.181 / 2) / (!PI * s^2)) * SNR ; ref 2D.tex for why this is 5SNR in peak band

amps = []
sigms = !NULL
pivot_amps = []
success = 0

for i=1, 100000 do begin
    noise = gennoise_multi(sd, bw, freqs)
    sig = addgauss_multi(a, s, range/2 + 0.5, range/2 + 0.5, noise, emis, td, /bbody, normband=0)
    ret0 = subtractmax_multi(sig, sd, s, bw, emis, td, normband=0, /bbody)
    if ret0.tdust ne -1 then begin
        success += 1
        print, success, i
        ret1 = subtractmax_multi(sig, sd, s, bw, normband=1, emis, td, real_x=ret0.xparam, real_y=ret0.yparam, /bbody)
        ret2 = subtractmax_multi(sig, sd, s, bw, normband=2, emis, td, real_x=ret0.xparam, real_y=ret0.yparam, /bbody)
        ret3 = subtractmax_multi(sig, sd, s, bw, normband=3, emis, td, real_x=ret0.xparam, real_y=ret0.yparam, /bbody)
        ret4 = subtractmax_multi(sig, sd, s, bw, normband=4, emis, td, real_x=ret0.xparam, real_y=ret0.yparam, /bbody)
        ret5 = subtractmax_multi(sig, sd, s, bw, normband=5, emis, td, real_x=ret0.xparam, real_y=ret0.yparam, /bbody)
        sed = getsedpts(sig, sd, s, bw, [ret0.xparam, ret0.yparam])

        amps = [[[amps]], [sed.amps]]
        if sigms eq !NULL then sigms = sed.sigms
        pivot_amps = [[[pivot_amps]], [ret0.aest, ret1.aest, ret2.aest, ret3.aest, ret4.aest, ret5.aest]]

        if FN ne !NULL then save, amps, sigms, pivot_amps, freqs, filename=FN $
            else save, amps, sigms, pivot_amps, freqs, filename='5cut.sav'
    endif
endfor

end

; inserted: 0.2498      0.1793     0.0867     0.0528     0.0138    0.00369
; results - A/sigm_a = 8.74569, 10638 trials
;     0.25403782     0.046346436
;     0.25889439     0.029602499
;     0.17515643     0.037650534
;     0.18121138     0.020720069
;    0.079818139     0.026386419
;    0.083234952    0.0095172498
;    0.047182428     0.019897936
;    0.049048849    0.0056083428
;    0.012509457    0.0085995655
;    0.011767069    0.0013454701
;   0.0030307207    0.0024875892
;   0.0028992266   0.00033150333
; chi2 manual cut estimates
; 0.0316835
; 0.0221767
; 0.0101863
; 0.00600926
; 0.00144005
; 0.000354808
