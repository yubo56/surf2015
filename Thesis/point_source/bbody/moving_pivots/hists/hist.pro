pro hist, SNR, FN
; generates data for histogram comparing pivot approach to per-band fluxes

range = 256D
bw = 480 / range
emis = 1.7
td = 13
freqs = [400, 352.94, 272.73, 230.77, 150, 100] ; frequencies in GHz
num_bands = n_elements(freqs)

convrms = [0.181, 0.137, 0.112, 0.0947, 0.049, 0.009] / 2
s = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
s /= bw ; sigm in bins
a = convrms[0] * SNR / sqrt(!PI * s^2); SNR
sd = spdensgen_multi(range, $
    convrms, $ ; convert noise/beam -> noise/pixel
    replicate(0,num_bands), replicate(8.0/3, num_bands), bw) ; PSD in units of mJy^2

amps = []
sigms = !NULL
pivot_amps = []
success = 0

for i=1, 100000 do begin
    noise = gennoise_multi(sd, bw, freqs)
    sig = addgauss_multi(a, s, range/2 + 0.5, range/2 + 0.5, noise, emis, td, /bbody, normband=0)
    ret0 = bbody_multi(sig, sd, s, bw, normband=0)
    if ret0.tdust ne -1 then begin
        success += 1
        print, success, i
        ret1 = bbody_multi(sig, sd, s, bw, normband=1, real_x=ret0.xparam, real_y=ret0.yparam)
        ret2 = bbody_multi(sig, sd, s, bw, normband=2, real_x=ret0.xparam, real_y=ret0.yparam)
        ret3 = bbody_multi(sig, sd, s, bw, normband=3, real_x=ret0.xparam, real_y=ret0.yparam)
        ret4 = bbody_multi(sig, sd, s, bw, normband=4, real_x=ret0.xparam, real_y=ret0.yparam)
        ret5 = bbody_multi(sig, sd, s, bw, normband=5, real_x=ret0.xparam, real_y=ret0.yparam)
        sed = getsedpts(sig, sd, s, bw, [ret0.xparam, ret0.yparam])

        amps = [[[amps]], [sed.amps]]
        if sigms eq !NULL then sigms = sed.sigms
        pivot_amps = [[[pivot_amps]], [ret0.aest, ret1.aest, ret2.aest, ret3.aest, ret4.aest, ret5.aest]]

        if FN ne !NULL then save, amps, sigms, pivot_amps, freqs, filename=FN $
            else save, amps, sigms, pivot_amps, freqs, filename='hist.sav'
    endif
endfor

end
