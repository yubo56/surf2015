pro pivot_seds, SNR

range = 256D
bw = 480 / range
emis = 1.7
td = 13
freqs = [400, 352.94, 272.73, 230.77, 150, 100] ; frequencies in GHz
num_bands = n_elements(freqs)

sd = spdensgen_multi(range, $
    [0.181, 0.137, 0.112, 0.0947, 0.049, 0.009] / 2, $
    replicate(0,num_bands), replicate(8.0/3, num_bands), bw) ; PSD in units of 0.01 mJy^2
noise = gennoise_multi(sd, bw, freqs)
s = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
s /= bw ; sigm in bins
a = sqrt((0.181 / 2) / (!PI * s^2)) * SNR ; ref 2D.tex for why this is 5SNR in peak band
sig = addgauss_multi(a, s, range/2 + 0.5, range/2 + 0.5, noise, emis, td, /bbody, normband=0)
ret0 = bbody_multi(sig, sd, s, bw, normband=0, autoder=0)
if ret0.tdust eq -1 then return
ret1 = bbody_multi(sig, sd, s, bw, normband=1, autoder=0, real_x=ret0.xparam, real_y=ret0.yparam)
ret2 = bbody_multi(sig, sd, s, bw, normband=2, autoder=0, real_x=ret0.xparam, real_y=ret0.yparam)
ret3 = bbody_multi(sig, sd, s, bw, normband=3, autoder=0, real_x=ret0.xparam, real_y=ret0.yparam)
ret4 = bbody_multi(sig, sd, s, bw, normband=4, autoder=0, real_x=ret0.xparam, real_y=ret0.yparam)
ret5 = bbody_multi(sig, sd, s, bw, normband=5, autoder=0, real_x=ret0.xparam, real_y=ret0.yparam)
sed = getsedpts(sig, sd, s, bw, [ret0.xparam, ret0.yparam])

emis_all = [emis, ret0.emissivity, ret1.emissivity, ret2.emissivity, ret3.emissivity, ret4.emissivity, ret5.emissivity]
tdust_all = [td, ret0.tdust, ret1.tdust, ret2.tdust, ret3.tdust, ret4.tdust, ret5.tdust]
amp_all = [a, ret0.aest, ret1.aest, ret2.aest, ret3.aest, ret4.aest, ret5.aest]
a = plot_bbody_snu(emis_all, tdust_all, amp_all,$
    [0,0,1,2,3,4,5], name=['Input', '400', '352', '272', '231', '150', '100','PerBand'], title='SNR 5')
l = legend(position=[540,0.2], /data)
a.save, 'SNR5-2.png'

end
