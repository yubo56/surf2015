pro comp_bands, SNR, FN
; compares the per-band flux to that predicted by the moving pivots approach (where black body using diff normalization factor per band is used)
; as function of SNR
range = 256D
bw = 480 / range
emis = 1.7
td = 13
freqs = [400, 352.94, 272.73, 230.77, 150, 91] ; frequencies in GHz
num_bands = n_elements(freqs)

convrms = [0.181, 0.137, 0.112, 0.0947, 0.05, 0.024] / 2
s = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
s /= bw ; sigm in bins
a = convrms[0] * SNR / sqrt(!PI * s^2); SNR 5
sd = spdensgen_multi(range, $
    convrms, $ ; convert noise/beam -> noise/pixel
    replicate(0,num_bands), replicate(8.0/3, num_bands), bw) ; PSD in units of mJy^2
noise = gennoise_multi(sd, bw, freqs)
sig = addgauss_multi(a, s, range/2 + 0.5, range/2 + 0.5, noise, emis, td, /bbody, normband=0)

ret0 = bbody_multi(sig, sd, s, bw, normband=0)
while ret0.tdust eq -1 do begin
    noise = gennoise_multi(sd, bw, freqs)
    sig = addgauss_multi(a, s, range/2 + 0.5, range/2 + 0.5, noise, emis, td, /bbody, normband=0)
    ret0 = bbody_multi(sig, sd, s, bw, normband=0)
endwhile
; ret1 = bbody_multi(sig, sd, s, bw, normband=1, real_x=ret0.xparam, real_y=ret0.yparam)
; ret2 = bbody_multi(sig, sd, s, bw, normband=2, real_x=ret0.xparam, real_y=ret0.yparam)
; ret3 = bbody_multi(sig, sd, s, bw, normband=3, real_x=ret0.xparam, real_y=ret0.yparam)
; ret4 = bbody_multi(sig, sd, s, bw, normband=4, real_x=ret0.xparam, real_y=ret0.yparam)
; ret5 = bbody_multi(sig, sd, s, bw, normband=5, real_x=ret0.xparam, real_y=ret0.yparam)
sed = getsedpts(sig, sd, s, bw, [ret0.xparam, ret0.yparam])

; print, sed.amps, sed.sigms
; retamps = [ret0.aest, ret1.aest, ret2.aest, ret3.aest, ret4.aest, ret5.aest]
; retsigms = [ret0.sigm_a, ret1.sigm_a, ret2.sigm_a, ret3.sigm_a, ret4.sigm_a, ret5.sigm_a]
retamps = amps_multi(ret0.aest, freqs, ret0.emissivity, ret0.tdust, /bbody)
retsigms = amps_multi(ret0.sigm_a, freqs, ret0.emissivity, ret0.tdust, /bbody)
amps = amps_multi(a, freqs, emis, td, /bbody)
; q1 = errorplot(freqs, sed.amps, sed.sigms, '.db', xrange=[50, 450],$
    ; XTITLE='Frequency (GHz)', YTITLE='Flux (mJy)', TITLE='Band-by-band vs. Pivot',$
    ; name='Per Band')
; q2 = errorplot(freqs, retamps, retsigms, '.hr', xrange=[50, 450], thick=2, /overplot,$
    ; name='Pivot')
; q2.sym_filled = 1
; q3 = plot(freqs, amps, '-g', thick=2, /overplot, name='In SED')
; l = legend(position=[440, 0.01], /data)
; if FN ne !NULL then q2.save, FN


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;           PLOTTING NIGHTMARE                ;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
!P.FONT = 1
set_plot, 'X'
device, decomposed=0
tek_color
window, 0, xsize=800, ysize=800
pageInfo = pswindow()
cleanplot, /silent
wdelete
set_plot, 'PS'
device, _Extra = pageInfo, /color, filename=FN, language=2, SET_FONT='Helvetica Bold', /TT_font
!P.MULTI = [0, 1, 1] ; grids window into top, bottom [win_number, cols, rows]

plot, freqs, amps, color=0, xrange=[50, 450], yrange=[1e-4, 1.5 * max(sed.amps)], /ylog, /ystyle, charsize=1.5, charthick=2, font=1, ytitle='Flux (mJy)', xtitle='Frequency (GHz)', title='Band-by-band vs. Pivot', thick=2
oploterror, freqs, color=2, sed.amps, sed.sigms, xrange=[50, 450], yrange=[1e-4, 1.5 * max(sed.amps)], /ylog, /ystyle, psym=2, errcolor=2, charsize=1.5, charthick=2, font=1, ytitle='Flux (mJy)', xtitle='Frequency (GHz)', title='Band-by-band vs. Pivot'
oploterror, freqs, retamps, retsigms, color=4, psym=2, errcolor=4, font=1
device, /close_file
set_plot, 'X'

stop
end
