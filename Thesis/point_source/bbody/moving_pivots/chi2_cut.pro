pro chi2_cut, SNR
; computes cut along chi_2 at input tdust, beta for a single noise realization for amplitude in each of the bands.
; averaging num_trials doesn't help, curvature is independent of noise (as expected from formula)

range = 256D
bw = 480 / range
emis = 1.7
td = 13
freqs = [400, 352.94, 272.73, 230.77, 150, 100] ; frequencies in GHz
num_bands = n_elements(freqs)

sd = spdensgen_multi(range, $
    [0.181, 0.137, 0.112, 0.0947, 0.049, 0.009] / 2, $
    replicate(0,num_bands), replicate(8.0/3, num_bands), bw) ; PSD in units of 0.01 mJy^2
s = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
s /= bw ; sigm in bins
a = sqrt((0.181 / 2) / (!PI * s^2)) * SNR ; ref 2D.tex for why this is 5SNR in peak band

; adds a signal with SNR
noise = gennoise_multi(sd, bw, freqs)
sig = addgauss_multi(a, s, 127.5, 127.5, noise, emis, td, /bbody, normband=0)

; gets chi2-minimizing amplitudes in each band
ret0 = subtractmax_multi(sig, sd, s, bw, emis, td, /bbody, normband=0)
while ret0.aest ne ret0.aest do begin ; keep going until not NaN
    noise = gennoise_multi(sd, bw, freqs)
    sig = addgauss_multi(a, s, 127.5, 127.5, noise, emis, td, /bbody, normband=0)
    ret0 = subtractmax_multi(sig, sd, s, bw, emis, td, /bbody, normband=0)
endwhile
amps_min = amps_multi(ret0.aest, freqs, emis, td, /bbody, normband=0)
sed = getsedpts(sig, sd, s, bw, [ret0.xparam,ret0.yparam])

num_pts = 100
x = make_array(num_pts + 1)
chi2 = make_array(num_pts + 1)
for i=0, num_bands-1 do begin
    for j=0, num_pts do begin
        x[j] = amps_min[i] * (3.0 / 5 + (4.0 * j) / (5 * num_pts))
        ret = subtractknown_multi(sig, sd, s, bw, emis, td, /bbody, real_pos=[ret0.xparam, ret0.yparam], real_amp=$
            x[j], normband=i)
        chi2[j] = ret.chi2
    endfor
    coeffs = poly_fit(x, chi2, 2)
    print, sqrt(1/coeffs[2])
endfor
end
; one realization
; inserted: 0.2498      0.1793     0.0867     0.0528     0.0138    0.00369
; retarr = []
; for i=0, num_bands-1 do retarr = [retarr,  subtractmax_multi(sig, sd, s, bw, emis, td, /bbody, normband=i)]
; IDL> print, retarr.sigm_a
;      0.031714526     0.022198368     0.010196270    0.0060084769    0.0014414643   0.00035515484
;
; chi2 cut estimates
; 0.0316835
; 0.0221767
; 0.0101863
; 0.00600926
; 0.00144005
; 0.000354808
;
;
; agreement!!!!
