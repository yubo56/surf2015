pro test_bbody, tdust
; runs GD on 5SNR for a given tdust value
SNR = 10

range = 256L
emissivity = 1.7D; real emissivity
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

binwidth = 480 / range
sigm = 15D / (2 * sqrt(2 * alog(2)))
sigm /= binwidth
amp = sqrt((0.181 / 2) / (!PI * sigm^2)) * SNR
amps_norm = amps_multi(1, freqs, emissivity, tdust, /bbody)
amp /= amps_norm[0]

; emissivity part
emissivities = []
tdusts = []
n_iters = []
amplist = []
xlist = []
ylist = []
chi2list = []

oldtime = systime(1)
for i=0, 300 do begin
    noise = gennoise_multi(specdens, binwidth, freqs)
    ; generate random 
    sig = addgauss_multi(amp, sigm, 127.7, 127.3, noise, emissivity, tdust, /bbody)

    ; get params
    ret = bbody_multi(sig, specdens, sigm, binwidth) ; start on top of correct values whatever
    if ret.tdust ne -1 then begin ; sometimes something screws up and tnmin bugs out
        ; store values
        emissivities = [emissivities, ret.emissivity]
        tdusts = [tdusts, ret.tdust]
        n_iters = [n_iters, ret.num_iters]
        amplist = [amplist, ret.aest]
        xlist = [xlist, ret.xparam]
        ylist = [ylist, ret.yparam]
        chi2list = [chi2list, ret.chi2]
        newtime = systime(1)
        print, string(i) + ': ' + string(ret.dbeta) + ' | ' + string(ret.dt_dust) + ' | time:' + string(newtime - oldtime)
        oldtime = newtime
        stop
        save, emissivities, tdusts, n_iters, amplist, xlist, ylist, chi2list, amp, sigm, emissivity, tdust, filename=strcompress('bbody-' + string(fix(tdust)) + 'K.sav', /REMOVE_ALL)
    endif
endfor
end
