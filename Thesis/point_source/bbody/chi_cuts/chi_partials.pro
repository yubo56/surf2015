pro gen_chi
; generates two chi^2 contours marginalizing over everything but emissivity, tdust
; each contour holds one of emissivity, tdust at correct value and generates other

range = 256
binwidth = 1D
sigm = 5D
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

num_pts = 40
emis = dblarr(num_pts + 1)
tdusts = dblarr(num_pts + 1)

for i=1, 100 do begin
    for j=0, num_pts do begin
        noise = gennoise_multi(specdens0, binwidth, freqs)
        sig = addgauss_multi(1, sigm, 127.7, 127.3, noise, emissivity=1.5D, tdust=40D, /bbody)
        
        ; subtract fixed tdust
        ret1 = subtractmax_multi(sig, specdens0, sigm, binwidth, emissivity=(1.25 + j * 0.5/num_pts), tdust=40D, /bbody)
        emis[j] += ret1.chi2

        ; subtract fixed emiss
        ret1 = subtractmax_multi(sig, specdens0, sigm, binwidth, emissivity=1.5D, tdust=(35 + j * 10/num_pts), /bbody)
        tdusts[j] += ret1.chi2
    endfor
    print, i
    emis /= i
    tdusts /= i
    save, emis, tdusts, filename='chi_partials.sav'
    emis *= i
    tdusts *= i
endfor

end
