pro fixed_uncerts
; gets uncertainties on histogram parameters

restore, 'hist_4fixed.sav'
poswidth = 0.001
betawidth = 0.0002
ampwidth = 0.0001

xamp_params = []
xbeta_params = []
xy_params = []
yamp_params = []
ybeta_params = []
betaamp_params = []
hist_xamp = hist_2d(xamp[0,*]-127.7, xamp[1,*]-1, bin1=poswidth, bin2=ampwidth)
hist_xbeta = hist_2d(xbeta[0,*]-127.7, xbeta[1,*]-1.5, bin1=poswidth, bin2=betawidth)
hist_xy = hist_2d(xy[0,*]-127.7, xy[1,*]-127.3, bin1=poswidth, bin2=poswidth)
hist_yamp = hist_2d(yamp[0,*]-127.3, yamp[1,*]-1, bin1=poswidth, bin2=ampwidth) 
hist_ybeta = hist_2d(ybeta[0,*]-127.3, ybeta[1,*]-1.5, bin1=poswidth, bin2=betawidth)
hist_betaamp = hist_2d(betaamp[0,*]-1.5, betaamp[1,*]-1, bin1=ampwidth, bin2=ampwidth) ; use same width, smaller one

for i=1, 10000 do begin
    hist_xamp2 = hist_xamp + RANDOMN(SEED, n_elements(hist_xamp)) * sqrt(hist_xamp)
    hist_xbeta2 = hist_xbeta + RANDOMN(SEED, n_elements(hist_xbeta)) * sqrt(hist_xbeta)
    hist_xy2 = hist_xy + RANDOMN(SEED, n_elements(hist_xy)) * sqrt(hist_xy)
    hist_yamp2 = hist_yamp + RANDOMN(SEED, n_elements(hist_yamp)) * sqrt(hist_yamp)
    hist_ybeta2 = hist_ybeta + RANDOMN(SEED, n_elements(hist_ybeta)) * sqrt(hist_ybeta)
    hist_betaamp2 = hist_betaamp + RANDOMN(SEED, n_elements(hist_betaamp)) * sqrt(hist_betaamp)


    gauss_xamp = gauss2dfit(hist_xamp2, k_xamp, /tilt)
    gauss_xbeta = gauss2dfit(hist_xbeta2, k_xbeta, /tilt)
    gauss_xy = gauss2dfit(hist_xy2, k_xy, /tilt)
    gauss_yamp = gauss2dfit(hist_yamp2, k_yamp, /tilt)
    gauss_ybeta = gauss2dfit(hist_ybeta2, k_ybeta, /tilt)
    gauss_betaamp = gauss2dfit(hist_betaamp2, k_betaamp, /tilt)

    xamp_params = [[xamp_params], [k_xamp]]
    xbeta_params = [[xbeta_params], [k_xbeta]]
    xy_params = [[xy_params], [k_xy]]
    yamp_params = [[yamp_params], [k_yamp]]
    ybeta_params = [[ybeta_params], [k_ybeta]]
    betaamp_params = [[betaamp_params], [k_betaamp]]

    if i MOD 100 eq 0 then begin
        save, xamp_params, xbeta_params,xy_params, yamp_params, ybeta_params, betaamp_params, filename='fixed_uncerts.sav'
    endif
    print, i
endfor

end
