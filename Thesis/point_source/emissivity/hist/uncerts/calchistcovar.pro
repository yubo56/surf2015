function calchistcovar, a, sa, b, sb, t, st
; calculates the covariance matrix from mean/stdev of hist fit params
; inputs are uncertainties
; don't use diff step sizes, confusing

c00 = a^2 * cos(t)^2 + b^2 * sin(t)^2
c10 = (b^2 - a^2) * cos(t) * sin(t)
c11 = a^2 * sin(t)^2 + b^2 * cos(t)^2

sc00 = sqrt(4 * a^2 * sa^2 * cos(t)^4 + 4 * b^2 * sb^2 * sin(t)^4 + st^2 * sin(2*t)^2*(b^2-a^2)^2)
sc10 = sqrt((b^2 * sb^2 - a^2 * sa^2) * sin(2*t)^2 + (b^2 - a^2) * st^2 * cos(2*t)^2)
sc11 = sqrt(4*b^2*sb^2 * cos(t)^4 + 4*a^2*sa^2 * sin(t)^4 + st^2 * sin(2*t)^2*(a^2-b^2)^2)

print, sqrt(c00), sc00 / (2 * sqrt(c00))
print, c10, sc10
print, sqrt(c11), sc11 / (2 * sqrt(c11))

return, [sqrt(c00), sc00 / (2 * sqrt(c00)), c10, sc10, sqrt(c11), sc11 / (2 * sqrt(c11))]
end



; redo calculations with equal step sizes
