pro hist_4fixed
; gets histograms for amps, hist, xpos, ypos
; fixes two variables at a time and generates histograms for remaining 2
emissivity = 1.5D


range = 256
binwidth = 1D
sigm = 5D
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

; store results
xy = []
xamp = []
xbeta = []
yamp = []
ybeta = []
betaamp = []


oldtime = systime(1)
for i=0, 100000 do begin
    noise = gennoise_multi(specdens0, binwidth, freqs)
    ; generate random signal
    sig = addgauss_multi(1, sigm, 127.7, 127.3, noise, emissivity)

    ; xy
    ret = subtractmax_multi(sig, specdens0, sigm, binwidth, emissivity, real_amp=1)
    xy = [[xy], [ret.xparam, ret.yparam]]

    ; xamp
    ret = subtractmax_multi(sig, specdens0, sigm, binwidth, emissivity, real_y=127.3)
    xamp = [[xamp], [ret.xparam, ret.aest]]

    ; xbeta
    ret = emissivity_multi(sig, specdens0, sigm, binwidth, emissivity - 0.5 + randomu(SEED), real_y=127.3, real_amp=1) ; start with a random seed up to 0.5 off
    if ret.emissivity eq 0 or ret.emissivity eq 1.5 then print, "ERROR"
    xbeta = [[xbeta], [ret.xparam, ret.emissivity]]

    ; yamp
    ret = subtractmax_multi(sig, specdens0, sigm, binwidth, emissivity, real_x=127.7)
    yamp = [[yamp], [ret.yparam, ret.aest]]

    ; ybeta
    ret = emissivity_multi(sig, specdens0, sigm, binwidth, emissivity - 0.5 + randomu(SEED), real_x=127.7, real_amp=1) ; start with a random seed up to 0.5 off
    if ret.emissivity eq 0 or ret.emissivity eq 1.5 then print, "ERROR"
    ybeta = [[ybeta], [ret.yparam, ret.emissivity]]

    ; betaamp
    ret = emissivity_multi(sig, specdens0, sigm, binwidth, emissivity - 0.5 + randomu(SEED), real_x=127.7, real_y=127.3) ; start with a random seed up to 0.5 off
    if ret.emissivity eq 0 or ret.emissivity eq 1.5 then print, "ERROR"
    betaamp = [[betaamp], [ret.emissivity, ret.aest]]

    ; store results
    sigm_beta = ret.sigm_beta
    sigm_x0 = ret.sigm_x0
    sigm_a = ret.sigm_a
    print, i
    save, xy, xamp, xbeta, yamp, ybeta, betaamp, sigm_beta, sigm_x0, sigm_a, filename='hist_4fixed.sav'
endfor
end
