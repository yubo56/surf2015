pro test_amps
; tests amplitude, beta histogram in subtraction
emissivity = 1.5D
; 10000 trials
; FIT_MEAN        DOUBLE          0.99999188
; FIT_RMS         DOUBLE       0.00060227623
; FIT_MEAN        DOUBLE           1.4999879
; FIT_RMS         DOUBLE        0.0011753483


range = 256
binwidth = 1D
sigm = 5D
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

; emissivity part
emissivities = []
n_iters = []
amps = []

oldtime = systime(1)
for i=0, 10000 do begin
    noise = gennoise_multi(specdens0, binwidth, freqs)
    ; generate random 
    sig = addgauss_multi(1, sigm, 127.7, 127.3, noise, emissivity=emissivity)

    ; get emissivity
    ret = emissivity_multi(sig, specdens0, sigm, binwidth, init=emissivity - 0.5 + randomu(SEED)) ; start with a random seed up to 0.5 off
    emissivities = [emissivities, ret.emissivity]
    amps = [amps, ret.aest]
    n_iters = [n_iters, ret.num_iters]
    newtime = systime(1)
    print, 'Trial' + string(i) + ' calc:' + string(ret.emissivity) + ' time:' + string(newtime - oldtime)
    oldtime = newtime
    sigm_beta = ret.sigm_beta
    save, emissivities, amps, sigm_beta, n_iters, filename='test_amps.sav'
endfor
end
