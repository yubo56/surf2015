pro pos_beta
; co]lects pos_beta tests to examine the unexpected ridge in the hist

; was for debugging purposes, understood 01/20/16: setting init=0 was
; interpreted as no init passed, would default to 1.5, so sharp ridge of
; emissivity about 1.5 since oftentimes would be very close to vanishing (as
; beta_real = 1.5)

range = 256
binwidth = 1D
sigm = 5D
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

xparams = []
eparams = []

while 1 do begin
    noise = gennoise_multi(specdens0, binwidth, freqs)
    sig = addgauss_multi(1, sigm, 127.7, 127.3, noise, emissivity=1.0)

    retpos = subtractmax_multi(sig, specdens0, sigm, binwidth, emissivity=1.5, real_y=127.3, real_amp=1)
    init = 0.5 + randomu(SEED)
    retemis = emissivity_multi(sig, specdens0, sigm, binwidth, init=init, real_y=127.3, real_x=127.7, real_amp=1)

    xdiff = (retpos.xparam - 127.7) / retpos.sigm_x0
    ediff = (retemis.emissivity - 1.0) / retemis.sigm_beta
    if abs(ediff) lt 0.0001 then begin
        stop
    endif
    print, xdiff, ediff

    xparams = [xparams, xdiff]
    eparams = [eparams, ediff]
endwhile
end
