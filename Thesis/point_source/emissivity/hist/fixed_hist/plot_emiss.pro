pro plot_emiss, FN
; sample script to pretty plot results from emissivity histograms

restore, FN
pos_filename=FN + '.ps'

; theory values

; do fits
hist_beta = histogram(emissivities, binsize=sigm_beta/30, locations = bins_beta)
gfit_beta = gaussfit(bins_beta, hist_beta, coeff_beta, nterms=3)




; gets correct dimensions for plot
set_plot, 'X'
device, decomposed=0
tek_color
window, 0, xsize=800, ysize=400
pageInfo = pswindow()
cleanplot, /silent
wdelete

; plot beta
set_plot, 'PS'
device, _Extra = pageInfo, /color, filename=pos_filename, language=2
!P.MULTI = [0, 0, 1] ; grids window into top, bottom [win_number, cols, rows]
plot, bins_beta, hist_beta, charsize=1.0, title='X position estimation deviations', psym=10, xtitle='X position (bin)', xrange=[coeff_beta[1] - 5 * coeff_beta[2], coeff_beta[1] + 5 * coeff_beta[2]], thick=4, xthick=4, ythick=4
oplot, bins_beta, gfit_beta, color=2, thick=6
; put xparams on plot
plot_xpos = !X.CRANGE[0] + 0.05 * (!X.CRANGE[1] - !X.CRANGE[0]); coordinates for top left corner
plot_ypos = !Y.CRANGE[0] + 0.9 * (!Y.CRANGE[1] - !Y.CRANGE[0])
str = 'mean = ' + string(format = '(G8.2,"!C")', coeff_beta[1])
str = str + 'rms = ' + string(format = '(E11.4, "!C")', coeff_beta[2])
str = str + 'theory = ' + string(format = '(E11.4)', sigm_beta)
xyouts, /data, plot_xpos, plot_ypos, str, charsize=1.0

; close file
device, /close_file



; restore plotting method
set_plot, 'X'

print, coeff_beta[2], sigm_beta
end
