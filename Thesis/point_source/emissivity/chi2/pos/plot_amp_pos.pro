pro plot_amp_pos

restore, 'chi_pos.sav'

; gets correct dimensions for plot
set_plot, 'X'
device, decomposed=0
tek_color
window, 0, xsize=800, ysize=800
pageInfo = pswindow()
cleanplot, /silent
wdelete

; plot beta, pos contours
set_plot, 'PS'
device, _Extra = pageInfo, /color, filename='chi_pos.ps', language=2

; plot first contour
!P.MULTI = [0, 2, 2] ; grids window into top, bottom [win_number, cols, rows]
contour, chi2s[*,*,20], x_beta, x_pos, charsize=1.0, title='Chi2 contour plot, beta, x', ytitle='X position (bin)', xtitle='Beta', thick=4, xthick=4, ythick=4

; plot second contour
!P.MULTI = [1, 2, 2] ; grids window into top, bottom [<idk>, cols, rows]
contour, reform(chi2s[*,20,*]), x_beta, x_pos, charsize=1.0, title='Chi2 contour plot, beta, y', ytitle='Y position (bin)', xtitle='Beta', thick=4, xthick=4, ythick=4

!P.MULTI = [2, 2, 2] ; grids window into top, bottom [<idk>, cols, rows]
contour, reform(chi2s[10,*,*]), x_pos, x_pos, charsize=1.0, title='Chi2 contour plot, x, y', ytitle='Y position (bin)', xtitle='X position (bin)', thick=4, xthick=4, ythick=4

; close file
device, /close_file




restore, 'chi_amp.sav'

; gets correct dimensions for plot
set_plot, 'X'
device, decomposed=0
tek_color
window, 0, xsize=400, ysize=400
pageInfo = pswindow()
cleanplot, /silent
wdelete

; plot beta, amp contours
set_plot, 'PS'
device, _Extra = pageInfo, /color, filename='chi_amp.ps', language=2

; plot first contour
!P.MULTI = [0, 1, 1] ; grids window into top, bottom [win_number, cols, rows]
contour, chi2s[*,*], x_beta, x_amp, charsize=1.0, title='Chi^2 contour plot, beta, amp', ytitle='Amplitude', xtitle='Beta', thick=4, xthick=4, ythick=4

; close file
device, /close_file

; restore plotting method
set_plot, 'X'
end
