pro chi_4var_cuts
; generates six pairwise cuts of chi^2 along x, y, amp, beta directions

range = 256
binwidth = 1D
sigm = 5D
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

num_pts = 20
chi2s_xy = dblarr(num_pts + 1, num_pts + 1)
chi2s_xamp = dblarr(num_pts + 1, num_pts + 1)
chi2s_yamp = dblarr(num_pts + 1, num_pts + 1)
chi2s_xbeta = dblarr(num_pts + 1, num_pts + 1)
chi2s_ybeta = dblarr(num_pts + 1, num_pts + 1)
chi2s_betaamp = dblarr(num_pts + 1, num_pts + 1)
x_beta = 1.35 + findgen(num_pts + 1) * 0.3 / num_pts
x_amp = 0.98 + findgen(num_pts + 1) * 0.04 / num_pts
x_pos = 124 + findgen(num_pts + 1) * 5 / num_pts

for i=1, 500 do begin
    for j=0, num_pts do begin
        for k=0, num_pts do begin
            noise = gennoise_multi(specdens0, binwidth, freqs)
            sig = addgauss_multi(1, sigm, 127, 127, noise, emissivity=1.5D)
            
            ; update for cuts
            ret1 = subtractknown_multi(sig, specdens0, sigm, binwidth, emissivity=1.5D, real_pos=[x_pos[j], x_pos[k]], real_amp=1.0)
            chi2s_xy[j,k] += ret1.chi2

            ret2 = subtractknown_multi(sig, specdens0, sigm, binwidth, emissivity=1.5D, real_pos=[x_pos[j], 127], real_amp=x_amp[k])
            chi2s_xamp[j,k] += ret2.chi2

            ret3 = subtractknown_multi(sig, specdens0, sigm, binwidth, emissivity=1.5D, real_pos=[127, x_pos[j]], real_amp=x_amp[k])
            chi2s_yamp[j,k] += ret3.chi2

            ret4 = subtractknown_multi(sig, specdens0, sigm, binwidth, emissivity=x_beta[k], real_pos=[x_pos[j], 127], real_amp=1.0)
            chi2s_xbeta[j,k] += ret4.chi2

            ret5 = subtractknown_multi(sig, specdens0, sigm, binwidth, emissivity=x_beta[k], real_pos=[127, x_pos[j]], real_amp=1.0)
            chi2s_ybeta[j,k] += ret5.chi2

            ret6 = subtractknown_multi(sig, specdens0, sigm, binwidth, emissivity=x_beta[j], real_pos=[127, 127], real_amp=x_amp[k])
            chi2s_betaamp[j,k] += ret6.chi2
        endfor
    endfor
    print, i
    chi2s_xy /= i
    chi2s_xamp /= i
    chi2s_yamp /= i
    chi2s_xbeta /= i
    chi2s_ybeta /= i
    chi2s_betaamp /= i
    save, chi2s_xy, chi2s_xamp, chi2s_yamp, chi2s_xbeta, chi2s_ybeta, chi2s_betaamp, x_beta, x_amp, x_pos, filename='chi_4var_cuts.sav'
    chi2s_xy *= i
    chi2s_xamp *= i
    chi2s_yamp *= i
    chi2s_xbeta *= i
    chi2s_ybeta *= i
    chi2s_betaamp *= i
endfor

end
