pro chi_4var_cutssfit
; For each noise realization, compute chi^2 surface along each of 6 cuts
; and sfit for parameters, save parameters over each iteration

range = 256
binwidth = 1D
sigm = 5D
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

num_pts = 20
chi2s_xy = dblarr(num_pts + 1, num_pts + 1)
chi2s_xamp = dblarr(num_pts + 1, num_pts + 1)
chi2s_yamp = dblarr(num_pts + 1, num_pts + 1)
chi2s_xbeta = dblarr(num_pts + 1, num_pts + 1)
chi2s_ybeta = dblarr(num_pts + 1, num_pts + 1)
chi2s_betaamp = dblarr(num_pts + 1, num_pts + 1)
sfit_betaamp = []
sfit_xbeta = []
sfit_ybeta = []
sfit_xamp = []
sfit_yamp = []
sfit_xy = []
x_beta = 1.3 + findgen(num_pts + 1) * 0.4 / num_pts
x_amp = 0.9 + findgen(num_pts + 1) * 0.2 / num_pts
x_pos = 126 + findgen(num_pts + 1) * 2 / num_pts

for i=1, 500 do begin
    ; noise realization to compute chi2 contours for
    noise = gennoise_multi(specdens0, binwidth, freqs)
    sig = addgauss_multi(1, sigm, 127, 127, noise, emissivity=1.5D)
    
    for j=0, num_pts do begin
        for k=0, num_pts do begin
            ; update for cuts
            ret1 = subtractknown_multi(sig, specdens0, sigm, binwidth, emissivity=1.5D, real_pos=[x_pos[j], x_pos[k]], real_amp=1.0)
            chi2s_xy[j,k] = ret1.chi2

            ret2 = subtractknown_multi(sig, specdens0, sigm, binwidth, emissivity=1.5D, real_pos=[x_pos[j], 127], real_amp=x_amp[k])
            chi2s_xamp[j,k] = ret2.chi2

            ret3 = subtractknown_multi(sig, specdens0, sigm, binwidth, emissivity=1.5D, real_pos=[127, x_pos[j]], real_amp=x_amp[k])
            chi2s_yamp[j,k] = ret3.chi2

            ret4 = subtractknown_multi(sig, specdens0, sigm, binwidth, emissivity=x_beta[k], real_pos=[x_pos[j], 127], real_amp=1.0)
            chi2s_xbeta[j,k] = ret4.chi2

            ret5 = subtractknown_multi(sig, specdens0, sigm, binwidth, emissivity=x_beta[k], real_pos=[127, x_pos[j]], real_amp=1.0)
            chi2s_ybeta[j,k] = ret5.chi2

            ret6 = subtractknown_multi(sig, specdens0, sigm, binwidth, emissivity=x_beta[j], real_pos=[127, 127], real_amp=x_amp[k])
            chi2s_betaamp[j,k] = ret6.chi2
        endfor
    endfor
    res_betaamp = sfit_p(reform(chi2s_betaamp), 2, kx=k_betaamp, /max_degree)
    res_xbeta = sfit_p(reform(chi2s_xbeta), 2, kx=k_xbeta, /max_degree)
    res_ybeta = sfit_p(reform(chi2s_ybeta), 2, kx=k_ybeta, /max_degree)
    res_xamp = sfit_p(reform(chi2s_xamp), 2, kx=k_xamp, /max_degree)
    res_yamp = sfit_p(reform(chi2s_yamp), 2, kx=k_yamp, /max_degree)
    res_xy = sfit_p(reform(chi2s_xy), 2, kx=k_xy, /max_degree)
    sfit_betaamp = [[sfit_betaamp], [k_betaamp]]
    sfit_xbeta = [[sfit_xbeta], [k_xbeta]]
    sfit_ybeta = [[sfit_ybeta], [k_ybeta]]
    sfit_xamp = [[sfit_xamp], [k_xamp]]
    sfit_yamp = [[sfit_yamp], [k_yamp]]
    sfit_xy = [[sfit_xy], [k_xy]]
    save, sfit_xy, sfit_xamp, sfit_yamp, sfit_xbeta, sfit_ybeta, sfit_betaamp, x_beta, x_amp, x_pos, filename='chi_4var_cutssfits.sav'
    print, i
endfor

end
