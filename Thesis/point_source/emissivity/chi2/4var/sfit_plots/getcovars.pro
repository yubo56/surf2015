function getcovars, x2, sigm_x2, xy, sigm_xy, y2, sigm_y2, xstep, ystep
; takes coefficients of fit to chi^2 and spits out covariance matrix terms
; x2, xy, y2 - coefficients of chi2 fit
; sigm_x2,... - uncertainties on fit coeffs
; xstep, ystep - step sizes of the chi2 surfaces

x2 = double(x2)
sigm_x2 = double(sigm_x2)
xy = double(xy)
sigm_xy = double(sigm_xy)
y2 = double(y2)
sigm_y2 = double(sigm_y2)

; correct for step size and factors, see A.3 in 2D.pdf
x2 /= xstep^2
sigm_x2 /= xstep^2
xy /= xstep * ystep * 2
sigm_xy /= xstep * ystep * 2
y2 /= ystep^2
sigm_y2 /= ystep^2

mat = [[x2, xy], [xy, y2]]
sigm_mat = [[sigm_x2, sigm_xy], [sigm_xy, sigm_y2]]

; sigm_x, covar, sigm_y
; print, 1/sqrt(x2), 1/xy, 1/sqrt(y2)
; print, sigm_x2 / (2 * (x2)^(3.0/2)), sigm_xy / (xy^2), sigm_y2 / (2 * (y2)^(3.0/2))

temp = invert_matuncert(mat, sigm_mat)
print, sqrt([temp.inv[0,0], temp.uncerts[0,0]])
print, sqrt([temp.inv[1,1], temp.uncerts[1,1]])
print, mean([temp.inv[1,0], temp.inv[0,1]]), mean([temp.uncerts[1,0], temp.uncerts[0,1]])
return, temp
end
