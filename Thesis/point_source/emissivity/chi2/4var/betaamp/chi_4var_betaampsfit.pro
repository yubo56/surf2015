pro chi_4var_betaampsfit
; For each noise realization, compute chi^2 surface along each of 6 cuts
; and sfit for parameters, save parameters over each iteration

range = 256
binwidth = 1D
sigm = 5D
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

num_pts = 20
chi2s_betaamp = dblarr(num_pts + 1, num_pts + 1)
sfit_betaamp = []
x_beta = 1.49 + findgen(num_pts + 1) * 0.02 / num_pts
x_amp = 0.995 + findgen(num_pts + 1) * 0.01 / num_pts

for i=1, 1000 do begin
    ; noise realization to compute chi2 contours for
    noise = gennoise_multi(specdens0, binwidth, freqs)
    sig = addgauss_multi(1, sigm, 127, 127, noise, emissivity=1.5D)
    
    for j=0, num_pts do begin
        for k=0, num_pts do begin
            ; update for cuts
            ret6 = subtractknown_multi(sig, specdens0, sigm, binwidth, emissivity=x_beta[j], real_pos=[127, 127], real_amp=x_amp[k])
            chi2s_betaamp[j,k] = ret6.chi2
        endfor
    endfor
    res_betaamp = sfit_p(reform(chi2s_betaamp), 2, kx=k_betaamp, /max_degree)
    sfit_betaamp = [[sfit_betaamp], [k_betaamp]]
    save, sfit_betaamp, x_beta, x_amp, filename='chi_4var_betaampsfit.sav'
    print, i
endfor

end
