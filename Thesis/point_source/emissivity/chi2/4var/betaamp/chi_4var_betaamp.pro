pro chi_4var_betaamp
; generates cuts solely along betaamp direction, since params seem weird
; chi_4var_betaamp[i] means using 10^[-i] as the domain size for x_amp below

range = 256
binwidth = 1D
sigm = 5D
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

num_pts = 30
chi2s_betaamp = dblarr(num_pts + 1, num_pts + 1)
x_beta = 1.49 + findgen(num_pts + 1) * 0.02 / num_pts
x_amp = 0.995 + findgen(num_pts + 1) * 0.01 / num_pts

for i=1, 1000 do begin
    for j=0, num_pts do begin
        for k=0, num_pts do begin
            noise = gennoise_multi(specdens0, binwidth, freqs)
            sig = addgauss_multi(1, sigm, 127, 127, noise, emissivity=1.5D)
            ret6 = subtractknown_multi(sig, specdens0, sigm, binwidth, emissivity=x_beta[j], real_pos=[127, 127], real_amp=x_amp[k])
            chi2s_betaamp[j,k] += ret6.chi2
        endfor
    endfor
    print, i
    chi2s_betaamp /= i
    save, chi2s_betaamp, x_beta, x_amp, filename='chi_4var_betaamp.sav'
    chi2s_betaamp *= i
endfor

end
