pro chi_amp
; generates chi^2 surface WRT beta, known positions

range = 256
binwidth = 1D
sigm = 5D
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

num_pts = 30
num_pts_amp = 40
chi2s = dblarr(num_pts + 1, num_pts_amp + 1)
x_beta = 1.25 + findgen(num_pts + 1) * 0.5 / num_pts
x_amp = 0.95 + findgen(num_pts_amp + 1) * 0.1 / num_pts_amp

for i=1, 1000 do begin
    for j=0, num_pts do begin
        for k=0, num_pts_amp do begin
            noise = gennoise_multi(specdens0, binwidth, freqs)
            sig = addgauss_multi(1, sigm, 127, 127, noise, emissivity=1.5D)
            
            ; subtract correct params
            ret1 = subtractknown_multi(sig, specdens0, sigm, binwidth, emissivity=x_beta[j], real_pos=[127, 127], real_amp=x_amp[k])
            chi2s[j,k] += ret1.chi2
        endfor
    endfor
    print, i
    chi2s /= i
    save, chi2s, x_beta, x_amp, filename='chi_amp.sav'
    chi2s *= i
endfor

end
