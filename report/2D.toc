\contentsline {section}{\numberline {1}Fourier Transform and Power Spectral Density Conventions}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Fourier Transforms --- Continuous Case}{2}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Proofs for Continuous Transform Formulae}{2}{subsubsection.1.1.1}
\contentsline {subsection}{\numberline {1.2}Discrete case}{3}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Proof for Discrete Transform Formulae}{4}{subsubsection.1.2.1}
\contentsline {section}{\numberline {2}Noise}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Continuous noise formalism}{4}{subsection.2.1}
\contentsline {section}{\numberline {3}Optimal filters}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Continuous filtering}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Offset Filtering}{6}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Discrete filtering}{7}{subsection.3.3}
\contentsline {section}{\numberline {4}Multiple beams}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}Continuous Solution}{7}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Coding in IDL}{8}{subsubsection.4.1.1}
\contentsline {section}{\numberline {5}Non-centered beams}{9}{section.5}
\contentsline {subsection}{\numberline {5.1}Performing fit}{10}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Peak Position Uncertainty}{10}{subsection.5.2}
\contentsline {section}{\numberline {6}Distinguishing blended sources}{11}{section.6}
\contentsline {subsection}{\numberline {6.1}Iterative Improvement}{11}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Mean-based (incorrect) method}{12}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}F-test}{13}{subsection.6.3}
\contentsline {section}{\numberline {7}Multi-band}{13}{section.7}
\contentsline {subsection}{\numberline {7.1}Including SED fitting}{15}{subsection.7.1}
\contentsline {section}{\numberline {8}Single Parameter $\beta $ Fit}{16}{section.8}
\contentsline {subsection}{\numberline {8.1}Covariance Matricies and $\chi ^2$}{16}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}$\beta $ uncertainty}{16}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Comparison to $\chi ^2$ curvature surface}{18}{subsection.8.3}
\contentsline {subsection}{\numberline {8.4}Comparison to 2D histograms}{18}{subsection.8.4}
\contentsline {section}{\numberline {9}Full Grey Body Fit, Optically Thin}{18}{section.9}
\contentsline {subsection}{\numberline {9.1}Filter amplitude}{19}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Full covariance matrix}{20}{subsection.9.2}
\contentsline {section}{\numberline {A}Miscellaneous}{21}{appendix.A}
\contentsline {subsection}{\numberline {A.1}Generating Noise}{21}{subsection.A.1}
\contentsline {subsection}{\numberline {A.2}Expected subtraction quality}{21}{subsection.A.2}
\contentsline {section}{\numberline {B}Dimensionalizing equations}{23}{appendix.B}
