    \documentclass[12pt]{article}
    \usepackage{fancyhdr, amsmath, amsthm, amssymb, mathtools, lastpage, hyperref, enumerate, graphicx, setspace, wasysym, upgreek}
    \usepackage[margin=0.5in, top=0.8in,bottom=0.8in]{geometry}
    \newcommand{\scinot}[2]{#1\times10^{#2}}
    \newcommand{\bra}[1]{\left<#1\right|}
    \newcommand{\ket}[1]{\left|#1\right>}
    \newcommand{\dotp}[2]{\left<\left.#1\right|#2\right>}
    \newcommand{\rd}[2]{\frac{\mathrm{d}#1}{\mathrm{d}#2}}
    \newcommand{\pd}[2]{\frac{\partial#1}{\partial#2}}
    \newcommand{\rtd}[2]{\frac{\mathrm{d}^2#1}{\mathrm{d}#2^2}}
    \newcommand{\ptd}[2]{\frac{\partial^2 #1}{\partial#2^2}}
    \newcommand{\norm}[1]{\left|\left|#1\right|\right|}
    \newcommand{\abs}[1]{\left|#1\right|}
    \newcommand{\pvec}[1]{\vec{#1}^{\,\prime}}
    \newcommand{\tensor}[1]{\overleftrightarrow{#1}}
    \let\Re\undefined
    \let\Im\undefined
    \newcommand{\ang}[0]{\text{\AA}}
    \newcommand{\mum}[0]{\upmu \mathrm{m}}
    \DeclareMathOperator{\Re}{Re}
    \DeclareMathOperator{\Tr}{Tr}
    \DeclareMathOperator{\Im}{Im}
    \newcommand{\expvalue}[1]{\left<#1\right>}
    \usepackage[labelfont=bf, font=scriptsize]{caption}\usepackage{tikz}
    \usepackage[font=scriptsize]{subcaption}
    \usepackage[numbers]{natbib}
    \everymath{\displaystyle}

\tikzstyle{circle} = [draw, circle, fill=white, node distance=3cm, minimum height=2em]

\begin{document}

\pagestyle{fancy}
\rhead{Yubo Su --- SURF Progress Report \#1}
\cfoot{\thepage/\pageref{LastPage}}

\title{Simulation Pipeline for Velocity Field Measurements using the Kinetic Sunyaev–--Zel'dovich Effect\\Progress Report 1}

\author{Yubo Su\\
Mentor: Prof. Sunil R. Golwala}

\date{February 22, 2015}

\maketitle

\section{Scientific Merit: Large-Scale Structure Velocity Measurements}

There are many cosmological models right now that depend on parameters we are currently unable to determine to a good accuracy, such as those describing long-length-scale behavior of gravity and dark energy characteristics. Being able to obtain stronger constraints via more precise measurements so would be a first step in determining the accuracy and correctness of these models. In particular more accurate measurements of velocity fields (the velocities of various stars, galaxies and other observable light sources) would provide powerful insight into the nature of many current models. For example, more accurate measurements of the \emph{peculiar velocity} (velocity deviations from Hubble's Law, the law that says galaxies recede from us at a rate proportional to their distance from us) would help distinguish between dark energy predictions and predictions of theories involving modifications to the laws of gravity.

Existing techniques such as spectroscopy for constraining cosmological parameters has been proposed (e.g. the total matter density $\Omega_m$ and the normalization of density fluctuations $\sigma_8$ \cite{42, 71, 92}). However, these existing techniques quickly become increasingly imprecise with sources at farther distances and requires increasing amounts of spectral data to adequately extract reasonable data, a shortcoming that renders them ineffective outside of our immediate universe. It is helpful to have a technique that can probe interesting features at a cosmological scale, to complement existing observations and to observe phenomena either too exotic or of too large a scale to be seen with shorter-range measurements..

\subsection{The Kinetic Sunyaev-Zel'dovich Effect}

In contrast with established measurement techniques, the kinetic Sunyaev-Zel'dovich effect (kSZ) provides a direct measurement of line-of-sight velocities $v_z$ of hot electrons within galaxy clusters \cite{131}. Because this measurement is independent of redshift and depends only on the electron line-of-sight velocity and optical depth, local characteristics, it provides a much more versatile and scalable probe at longer distances than existing techniques. Moreover, the scaling on the precision of kSZ measurements is more favorable than existing techniques; its dependences on the uncertainties of observational data scale such that, as measurements improve, kSZ measurements will obtain stricter constraints than other techniques. For example, studies conclude that in applications to dark energy constraints and general relativistic deviations, kSZ measurements would probe different mass scales and exhibit different systematic uncertainties while still maintaining a similar level of accuracy, which would be a strong way to obtain independent measurements to better constrain parameters \cite{10}. All these reasons are why kSZ measurements are some of the most exciting prospects on the horizon for cosmology.

\subsection{Current difficulties, existing Infrastructure}

Current observations of the kSZ effect have proven difficult but feasible. Some recent measurements are listed in the following paragraph. Data from the \emph{WMAP} and \emph{Planck} satellites have been used to place upper limits on the bulk flows and rms variations of $v_z$ via the kSZ signal \cite{59,93}. A combination of ACT and Sloan Digital Sky Survey III data was also used to constrain the mean pairwise momentum of clusters that is inconsistent with noise at a confidence level of $99.8\%$ \cite{48}. Most recently, kSZ measurements using Bolocam and \emph{Herschel}-SPIRE data have been able to constrain $v_z$ measurements to $3450 \pm 900 \mathrm{km/s}$ \cite{121}.

These measurements above all demonstrate both the feasibility of kSZ measurements and their potential sensitivity to moderate improvements in instrument calibration. However, there are many characterstics of of the detection pipeline that resist characterization at the present. Firstly, the kSZ effect is generally dominated by its thermal cousin the tSZ effect, and is on the same order as the relativistic corrections to the tSZ effect, the rSZ signal, both of which also occupy the same frequency band as the kSZ effect. Moreover, other galactic sources such as sub-milimeter galaxies and radio galaxies also have significant emissions in the same band, alongside CMB fluctuations. All of these sources must be identified and subtracted to glean any information from a kSZ detection. Difficulties are further compounded by the relative calibration between bands. To fully understand the obstacles to reliable kSZ detection, it is necessary to conduct an end-to-end simulation of the kSZ data collection process, from generating sky maps to collecting data from these synthetic maps with simulated instruments, including various effects such as gravitational lensing, atmospheric fluctuations and imperfect instrument effects. Since the next generation of instruments will have the requisite sensitivity for a successful kSZ detection, it is of \textbf{immediate importance} to implement an end-to-end simulation of the kSZ data collection process, to quantify the impat of the various expected effects on the signal detectability.

With performing this full simulation in mind, the current research group has much of the simulation pipeline already complete, including a full simulation infrastructure to generate a sky map from first principles and models of instruments that can be scanned across the simulated sky map. Two major tasks remain, a robust architecture for identifying point sources across multi-band data and accurate simulations of various effects that non-negligibly influence a kSZ detection.

\section{Present work}

Current work has focused on subtraction of point sources from a noisy background, with the remaining pipeline to identify the kSZ signal the subject of future work. First, we developed the mathematical formalism to compute the brightness of a single source and to subtract it. After implementing the technique in code, we confirmed that the subtraction was theoretically optimal given the characteristics of the noisy background.

After the approach to subtract a single source was verified, we attempted to generalize the approach to multiple sources but ran into a problem where overlapping sources interfered with one another. Specifically, overlapping sources would result in an overestimation of some sources' brightnesses and an underestimation of others', resulting in suboptimal subtraction that would underperform compared to the theoretical limit. Instead, we worked out a technique to simultaneously estimate the brightnesses of all sources in a map, which got rid of this problem. Doing so proved a bit trickier than expected, since naive approaches proved in testing to be unstable in that estimates got worse as we tried to improve them. However, a simple characteristic of the problem, namely its being linear in the brightness of the sources, allowed us to reformulate the estimation as a simple matrix multiplication, which simultaneously computes all the source brightnesses without incurring the aforementioned issues. Indeed, this simultaneous estimation proved in testing to be as optimal as the single-source subtraction and the theoretical limit.

We then generalized the above method to beams that may be centered \emph{between} pixels. Previously, the algorithm simply identified the brightest pixel in an image and simply centered the source on this pixel. However, in order to subtract accurately, the center of the source must be known to sub-pixel resolution. To do this, we first obtain the estimate of the source center to the nearest pixel. Then, we take a few pixels of the image around this best estimate and then perform a fit of this subregion of the image to the beam shape. This fit computes the best estimate of the inter-pixel location of the point source, and then the previously prescribed techniques for estimation of the brightness of a source remain applicable in determining the best estimates of the brightnesses of these point sources.

Over the course of developing this technique, a new problem surfaced: point sources can grow too close such that they are difficult to distinguish numerically from one another, e.g. Figure~\ref{fig:beams}. The effect of this phenomenon can be clearly seen in the below plots in Figure~\ref{fig:skew_hists}; while the correct amplitude of a source when there is only one source in the map is estimated cleanly and well, when there are many sources with higher probability of overlap the estimators fail much more often. The observed widening and skew are because the code takes two very close sources and estimates them as one very bright and one very dim, the sum of which is slightly less bright than the original two sources.
\begin{figure}[!h]
    \centering
    \begin{subfigure}{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{plots/10000-1.png}
        \caption{Estimated amplitude of single beam in map over 10000 noise realisations. Histogram bins are values of amplitude estimations over multiple noise backgrounds, logarithmic in number of counts. Bottom plot is a Gaussian fit. True amplitude is $0.5$ arbitrary units.}
        \label{fig:10000-1}
    \end{subfigure}
    ~
    \begin{subfigure}{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{plots/1000-20.png}
        \caption{Estimated amplitude of twenty beams in a map over 1000 different noise realisations. Histogram bins are values of amplitude estimations over multiple noise backgrounds. Bottom plot is a Gaussian fit to the histogram; with perfect estimation the estimators would fit perfectly to the Gaussian fit as in \ref{fig:10000-1}, but it is evident the introduction of too many beams significantly hinders accurate estimation. True amplitude is $0.5$ arbitrary units. Note the significant count of instances far away from the correct amplitude and the asymmetry of the estimators.}
    \end{subfigure}
    \caption{Histograms of estimated amplitudes in two different configurations. Note that the configuration where beams are permitted to overlap produce significantly poorer amplitude estimation as a result of being unable to detect whether a detection is a single beam or a coincidence peak and performing the appropriate subtraction.}
    \label{fig:skew_hists}
\end{figure}

It turns out however that simply going back through and re-estimating the positions of the sources \emph{with the knowledge of the other beams in the map in hand} brings the estimates much closer to the correct values. In other words, if after having gone through the image and extracted estimators for each source we go back through the map and attempt to add each source back in and re-subtract it, obtaining a new estimator for the source, the general behavior is convergent. In Figure~\ref{fig:conv} we see that over many such re-subtractions the source brightnesses converge to the correct value. Such an approach is conventionally referred to as \emph{iterative deepening}.
\begin{figure}[!h]
    \centering
    \includegraphics[width=0.3\textwidth]{plots/conv.png}
    \caption{Plot of amplitude estimators of two beams placed two FWHM apart as both position and amplitude estimators are iteratively improved. Note that while we initially grossly mischaracterise the beams, iterations produce a rapid convergence towards the correct values of two beams of amplitude $0.5$ arbitrary units.}
    \label{fig:conv}
\end{figure}

An examination of how well this iterative improvement strategy resolves the skewing exhibited in the setup in Figure~\ref{fig:skew_hists} is ongoing.

\section{Future work}

Subtraction of point sources is still an ongoing investigation. Ideally, it should be possible to determine whether a single beam is actually two very close beams or a single bright beam; for example, it should be possible to determine that the two beams in Figure~\ref{fig:beams} are two separate beams rather than treating them as a single beam and missubtracting. Currently some approaches have been discussed but none implemented and tested with sufficient robustness to warrant immediate discussion.
\begin{figure}[!h]
    \centering
    \includegraphics[width=0.3\textwidth]{plots/beams.png}
    \caption{Plot of view of two beams separated by two FWHM. Notice how tantalizing it is even for a human eye to mistaken this for a single Gaussian, but our code has shown ability to isolate the two beams via iterative improvement.}
    \label{fig:beams}
\end{figure}

A final way to improve point source subtraction is to correlate observational data over multiple frequencies of the same sources. Since the sources in the maps are well understood astrophysical phenomena such as stars and galaxy clusters, the relative brightnesses of these sources over different frequencies is well known, and so detections across multiple frequencies decreases sensitivity to noise. Moreover, weak detections in any individual image can contribute to a strong detection over all observation frequencies. Thus, both a mathematical and computational framework must be built for cross-correlating multiple maps like this. To work out both the math and the programming is a challenge that has already been encountered many times.

Finally, once these two improvements to point source subtraction are accounted for, the insertion and subsequent detection of the kSZ signal will begin. Again, it is important to keep in mind that we wish not to just subtract the point sources to noise levels but sufficiently low that the kSZ is the dominant signal in the map. 

\bibliographystyle{plainnat}
\renewcommand{\bibname}{References}
\bibliography{Su_SURFProposal2015}

\end{document}

