    \documentclass[10pt]{article}
    \usepackage{fancyhdr, amsmath, amsthm, amssymb, mathtools, lastpage}
    \usepackage[margin=0.5in, top=0.8in,bottom=0.8in]{geometry}
    \newcommand{\scinot}[2]{#1\times10^{#2}}
    \newcommand{\bra}[1]{\left<#1\right|}
    \newcommand{\ket}[1]{\left|#1\right>}
    \newcommand{\dotp}[2]{\left<\left.#1\right|#2\right>}
    \newcommand{\rd}[2]{\frac{\mathrm{d}#1}{\mathrm{d}#2}}
    \newcommand{\pd}[2]{\frac{\partial#1}{\partial#2}}
    \newcommand{\rtd}[2]{\frac{\mathrm{d}^2#1}{\mathrm{d}#2^2}}
    \newcommand{\ptd}[2]{\frac{\partial^2 #1}{\partial#2^2}}
    \newcommand{\norm}[1]{\left|\left|#1\right|\right|}
    \newcommand{\abs}[1]{\left|#1\right|}
    \newcommand{\pvec}[1]{\vec{#1}^{\,\prime}}
    \newcommand{\tensor}[1]{\overleftrightarrow{#1}}
    \let\Re\undefined
    \let\Im\undefined
    \DeclareMathOperator{\Re}{Re}
    \DeclareMathOperator{\Tr}{Tr}
    \DeclareMathOperator{\Im}{Im}
    \newcommand{\expvalue}[1]{\left<#1\right>}
    \usepackage[labelfont=bf, font=scriptsize]{caption}\usepackage{tikz}
    % \usepackage[version=3]{mhchem}
    % \usepackage{hyperref}
    \usepackage{enumerate}
    \usepackage[numbers]{natbib}
    \usepackage{setspace}
    \everymath{\displaystyle}

\begin{document}

\cfoot{\thepage/\pageref{LastPage}}

\title{Simulation Pipeline for Velocity Field Measurements using the Kinetic Sunyaev–--Zel'dovich Effect}

\author{Yubo Su\\
Mentor: Prof. Sunil R. Golwala}

\date{February 22, 2015}

\maketitle

\section{Scientific Merit: Large-Scale Structure Velocity Measurements}

There are many cosmological models right now that depend on parameters we are currently unable to determine to a good accuracy, such as those describing long-length-scale behavior of gravity and dark energy characteristics. Being able to obtain stronger constraints via more precise measurements so would be a first step in determining the accuracy and correctness of these models. In particular more accurate measurements of velocity fields (the velocities of various stars, galaxies and other observable light sources) would provide powerful insight into the nature of many current models. For example, more accurate measurements of the \emph{peculiar velocity} (velocity deviations from Hubble's Law, the law that says galaxies recede from us at a rate proportional to their distance from us) would help distinguish between dark energy predictions and predictions of theories involving modifications to the laws of gravity.

Existing techniques such as spectroscopy for constraining cosmological parameters has been proposed (e.g. the total matter density $\Omega_m$ and the normalization of density fluctuations $\sigma_8$ \cite{42, 71, 92}). However, these existing techniques quickly become increasingly imprecise with sources at farther distances and requires increasing amounts of spectral data to adequately extract reasonable data, a shortcoming that renders them ineffective outside of our immediate universe. It is helpful to have a technique that can probe interesting features at a cosmological scale, to complement existing observations and to observe phenomena either too exotic or of too large a scale to be seen with shorter-range measurements..

\subsection{The Kinetic Sunyaev-Zel'dovich Effect}

In contrast with established measurement techniques, the kinetic Sunyaev-Zel'dovich effect (kSZ) provides a direct measurement of line-of-sight velocities $v_z$ of hot electrons within galaxy clusters \cite{131}. Because this measurement is independent of redshift and depends only on the electron line-of-sight velocity and optical depth, local characteristics, it provides a much more versatile and scalable probe at longer distances than existing techniques. Moreover, the scaling on the precision of kSZ measurements is more favorable than existing techniques; its dependences on the uncertainties of observational data scale such that, as measurements improve, kSZ measurements will obtain stricter constraints than other techniques. For example, studies conclude that in applications to dark energy constraints and general relativistic deviations, kSZ measurements would probe different mass scales and exhibit different systematic uncertainties while still maintaining a similar level of accuracy, which would be a strong way to obtain independent measurements to better constrain parameters \cite{10}. All these reasons are why kSZ measurements are some of the most exciting prospects on the horizon for cosmology.

\subsection{Current difficulties, existing Infrastructure}

Current observations of the kSZ effect have proven difficult but feasible. Some recent measurements are listed in the following paragraph. Data from the \emph{WMAP} and \emph{Planck} satellites have been used to place upper limits on the bulk flows and rms variations of $v_z$ via the kSZ signal \cite{59,93}. A combination of ACT and Sloan Digital Sky Survey III data was also used to constrain the mean pairwise momentum of clusters that is inconsistent with noise at a confidence level of $99.8\%$ \cite{48}. Most recently, kSZ measurements using Bolocam and \emph{Herschel}-SPIRE data have been able to constrain $v_z$ measurements to $3450 \pm 900 \mathrm{km/s}$ \cite{121}.

These measurements above all demonstrate both the feasibility of kSZ measurements and their potential sensitivity to moderate improvements in instrument calibration. However, there are many characterstics of of the detection pipeline that resist characterization at the present. Firstly, the kSZ effect is generally dominated by its thermal cousin the tSZ effect, and is on the same order as the relativistic corrections to the tSZ effect, the rSZ signal, both of which also occupy the same frequency band as the kSZ effect. Moreover, other galactic sources such as sub-milimeter galaxies and radio galaxies also have significant emissions in the same band, alongside CMB fluctuations. All of these sources must be identified and subtracted to glean any information from a kSZ detection. Difficulties are further compounded by the relative calibration between bands. To fully understand the obstacles to reliable kSZ detection, it is necessary to conduct an end-to-end simulation of the kSZ data collection process, from generating sky maps to collecting data from these synthetic maps with simulated instruments, including various effects such as gravitational lensing, atmospheric fluctuations and imperfect instrument effects. Since the next generation of instruments will have the requisite sensitivity for a successful kSZ detection, it is of \textbf{immediate importance} to implement an end-to-end simulation of the kSZ data collection process, to quantify the impat of the various expected effects on the signal detectability.

With performing this full simulation in mind, the current research group has much of the simulation pipeline already complete, including a full simulation infrastructure to generate a sky map from first principles and models of instruments that can be scanned across the simulated sky map. Two major tasks remain, a robust architecture for identifying point sources across multi-band data and accurate simulations of various effects that non-negligibly influence a kSZ detection.

\section{Proposed Project}

It is proposed to expand the simulation pipeline and develop a robust framework to identify and subtract unwanted sources from multi-band simulated observational data and to detect a kSZ signal in the remaining signal. Furthermore it is proposed to augment the pipeline by introducing various other contributions to the signal, possibly including but not limited to gravitational lensing, atmospheric fluctuations.

\subsection{Work Objectives}

Prior to the beginning of the research period at the end of the 2014-15 school year, the student will be performing research with the professor and will be thoroughly exposed to the basics of signal processing in the context of the simulation and also to the simulation pipeline. By the start of summer the student will have begun implementing procedures that will take data and identify/subtract point sources of a known profile. Thus, during the summer the student will develop procedures to identify and subtract point sources from multi-band simulated instrument scans, including correlating detections across multiple bands. The remaining signal will then be parsed to identify the kSZ signal. If time permits additional improvements to the pipeline can be introduced as per the described above.

\subsection{10-week Plan}

\begin{table}[!h]
    \centering
    \begin{tabular}{l|l}
        Week & Objective\\\hline
        Week 1 --- 3 & Develop framework to identify point sources across multiple bands in simulated data.\\
        Week 4 --- 6 & Run simulations with identification pipeline on simulated data, improve identification algorithm.\\
        Week 7 --- 10 & (If previous objectives are met) Begin implementation of extra signal contributions.
    \end{tabular}
\end{table}

\subsection{Anticipated Challenges}

Principally there are two source subtraction techniques that are applicable to multi-band identification of point sources, cross-correlation among bands or identifying point sources exclusively in the highest frequency band. The former can give for more robust detections when a weak signal across all bands cross-correlates to a strong detection, but the latter technique has been met with success due to the fact that the highest frequency observational bands have little noise and thus point sources are the easiest to identify. However, exactly what approach (or combination of approaches) will yield the best results for point sources of which type are problems that are not yet answered. We plan to probe the full range of approaches to determine what point sources yield best to what techniques.

\section{Intellectual Merit}

The present student is fully prepared and well qualified to take on the demands of the project. He will be researching with Professor Golwla throughout Winter and Spring academic terms of the school year preceeding the project and will be well acquainted with both the signal processing formalism and the pipeline. He is also well versed in both physics/astrophysics aspects of the project, having taken courses in both subjects that will lend intuition and understanding of the models, as well as in computer science, which will be extremely useful in programming the various algorithms/simulations in an optimal way to ensure reasonable running time on the large maps.

\section{References}

\bibliographystyle{plainnat}
\renewcommand{\bibname}{References}
\bibliography{Su_SURFProposal2015}


\end{document}

