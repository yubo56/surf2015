pro gen_interp
; makes plot showing interpolation

; stuff to plot
a = dblarr(130)
a[0] = gaussian_function(20)
x = dindgen(n_elements(a)) / n_elements(a) * 10
b = rebin(a, 10, /sample)

set_plot, 'X'
device, decomposed=0
tek_color
window, 1, xsize=800, ysize=300
pageInfo = pswindow()
cleanplot, /silent
wdelete

; plot a
set_plot, 'PS'
device, _Extra = pageInfo, /color, filename='fit.ps', language=2
!P.MULTI = [0, 2, 1] ; grids window into top, bottom [win_number, cols, rows]
; stuff to plot
plot, b, thick=3

; plot other half
!P.MULTI = [1, 2, 1] ; grids window into top, bottom [win_number, cols, rows]
; stuff to plot
plot, b, thick=3
oplot, x, a, color=2, thick=2

; close file
device, /close_file

; restore plotting method
set_plot, 'X'

end
