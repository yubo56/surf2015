pro gen_beamplot
; makes plot of overlapping beams in 1D

; stuff to plot
a = dblarr(50)
b = dblarr(50)
a[5] = gaussian_function(4)
b[17] = gaussian_function(4)
c = a + b

!P.FONT = 1
set_plot, 'X'
device, decomposed=0
tek_color
window, 1, xsize=800, ysize=800
pageInfo = pswindow()
cleanplot, /silent
wdelete

; plot a
set_plot, 'PS'
device, _Extra = pageInfo, /color, filename='beams_close.ps', language=2, SET_FONT='Helvetica Bold', /TT_font
!P.MULTI = [0, 1, 2] ; grids window into top, bottom [win_number, cols, rows]
; stuff to plot
a = dblarr(50)
b = dblarr(50)
a[5] = gaussian_function(4)
b[17] = gaussian_function(4)
c = a + b
plot, c, thick=3, font=1
oplot, a, color=2
oplot, b, color=2

; plot other half
!P.MULTI = [1, 1, 2] ; grids window into top, bottom [win_number, cols, rows]
; stuff to plot
a = dblarr(50)
b = dblarr(50)
a[8] = gaussian_function(4)
b[14] = gaussian_function(4)
c = a + b
plot, c, thick=3, font=1
oplot, a, color=2
oplot, b, color=2

; close file
device, /close_file

; restore plotting method
set_plot, 'X'

end
