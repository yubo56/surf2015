    \documentclass[xcolor=dvipsnames]{beamer}
    \usepackage{amsmath, amsthm, amssymb, lmodern, graphicx, subcaption, wasysym}
    \usetheme{Madrid}
    \usefonttheme{professionalfonts}
    \newcommand{\scinot}[2]{#1\times10^{#2}}
    \newcommand{\bra}[1]{\left<#1\right|}
    \newcommand{\ket}[1]{\left|#1\right>}
    \newcommand{\dotp}[2]{\left<\left.#1\right|#2\right>}
    \newcommand{\rd}[2]{\frac{\mathrm{d}#1}{\mathrm{d}#2}}
    \newcommand{\pd}[2]{\frac{\partial#1}{\partial#2}}
    \newcommand{\rtd}[2]{\frac{\mathrm{d}^2#1}{\mathrm{d}#2^2}}
    \newcommand{\ptd}[2]{\frac{\partial^2 #1}{\partial#2^2}}
    \newcommand{\norm}[1]{\left|\left|#1\right|\right|}
    \newcommand{\abs}[1]{\left|#1\right|}
    \newcommand{\pvec}[1]{\vec{#1}^{\,\prime}}
    \newcommand{\tensor}[1]{\overleftrightarrow{#1}}
    \let\Re\undefined
    \let\Im\undefined
    \newcommand{\ang}[0]{\text{\AA}}
    \newcommand{\mum}[0]{\upmu \mathrm{m}}
    \DeclareMathOperator{\Re}{Re}
    \DeclareMathOperator{\Tr}{Tr}
    \DeclareMathOperator{\Im}{Im}
    \newcommand{\expvalue}[1]{\left<#1\right>}
    \everymath{\displaystyle}

\title[SURF 2015 Final Presentation]{Simulation Pipeline for Velocity Field Measurements using the Kinetic Sunyaev-Zel'dovich Effect}
% \subtitle[]{Subtitle}
\author[Yubo Su]{Yubo Su\inst{1}\\
Mentor: Sunil R. Golwala\inst{1}}
\institute[Caltech]{\inst{1}California Institute of Technology}
\date{August 20, 2015}
\logo{\includegraphics[width=0.15\textwidth]{images/logo.png}}
\setbeamertemplate{caption}{\insertcaption\par}
\begin{document}

\frame{\titlepage}


\begin{frame}
    \frametitle{Introduction}
    \framesubtitle{The kSZ effect}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Cosmic Microwave Background (CMB) is thermal radiation left over from the Big Bang.
                \item Variations are one part in $10^4$, highly homogeneous.
            \end{itemize}
            \begin{figure}
                \centering
                \includegraphics[width=0.8\textwidth]{images/CMB.png}
            \end{figure}
        \end{column}

        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item SZ effects blueshift CMB due to scattering off energetic electrons.
            \end{itemize}
            \begin{figure}
                \centering
                \includegraphics[width=0.5\textwidth]{images/SZ.png}
                \caption{{\scriptsize http://astro.uchicago.edu/sza/primer.html}}
            \end{figure}
        \end{column}
    \end{columns}
    % talk about what is kSZ, what separates from other techniques
    % kSZ observations have been feasible of late
\end{frame}

\begin{frame}
    \frametitle{Introduction}
    \framesubtitle{The kSZ effect}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Three kinds of SZ effects: thermal (tSZ), kinetic (kSZ), relativistic (rSZ).
                \item kSZ gives peculiar velocity measurements.
                \item Independent of redshift; only depends on electron velocity, optical depth, local characteristics.
                \item Different systematics.
            \end{itemize}
        \end{column}

        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Velocity fields are important to measure precisely to identify cosmological models (e.g. dark energy vs. modifications to gravity).
                \item kSZ recently measured peculiar velocities in a single cluster\footnote{Sayers et. al. 2013}.
                \item Next generation of detectors will have improved sensitivity, larger surveys.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Introduction}
    \framesubtitle{Characterizing Detectors}
    \begin{itemize}
        \item Need simulation to understand how known sources of error affect next generation of detectors.
        \item Generate mock data containing CMB, fake sources, SZ effects.
        \item Include instrumental effects.
        \item Feed through data processing pipeline.

            \begin{itemize}
                \item \only<1>{Subtract foreground sources.}\only<2>{\textbf{Subtract foreground sources.}}
                \item Examine kSZ effect.
            \end{itemize}

        \item Understand how kSZ detection depends on various factors.
    \end{itemize}
    % Talk about why kSZ is important
        % different systematics from modern techniques
    % talk about what we are doing
        % simulation pipeline to see how effects 
    % Talk about how my project is source subtraction
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Setup}
    \begin{itemize}
        \item Multiple detectors survey same area in different frequencies.
            \begin{itemize}
                \item Correlate data across frequencies to identify point sources to subtract.
            \end{itemize}
        \item Data is represented as an image in multiple frequencies, noise + signal

            $$v_\nu(x,y) = n_\nu(x,y) + \sum\limits_{i}^{}A_i s_\nu(x,y)$$

        \item Sources are all point sources, same profile $s_\nu$.
        \item Confusion noise.
    \end{itemize}
    % talk about how multi-band data
    % introduce concept of confusion noise, mention that integration time such that confusion noise dominates
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Approach}
    \begin{itemize}
        \item Source subtraction as a fitting procedure.
        \item Goodness of fit is described by $\chi^2$

            $$\chi^2 = \frac{\abs{\text{observed} - \text{fit}(\hat{\xi})}^2}{(\text{expected deviation})^2} = \sum\limits_{\nu}^{}\frac{\abs{\tilde{v}_\nu(k_x, k_y) - \sum\limits_{i}^{}\hat{A}_i \tilde{s}_\nu(k_x, k_y; \hat{\xi})}^2}{J_\nu(k_x, k_y)}$$
        \item Noise has point-wise correlations\footnote{Thermal noise is approximately white but atmospheric noise is angular frequency-dependent (Sayers et. al. 2009).}, so have to work in angular frequency space. $J$ is the power spectral density of the noise.
        \item Progressively build up to full model, start with single source, single frequency $\to$ multiple source, multiple frequency.
    \end{itemize}
    % Talk about how source subtraction is really just a fitting procedure
    % Talk about goodness of fit being described by chi^2
        % chi^2 = \frac{\abs{observed - fit}^2}{expected deviation}
    % Talk about non-local interactions and so we have to do this in frequency space
    % outline progression (single band, single source -> single band, multi source -> multi band, single source -> multi band, multi source)
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency, single source}
    \begin{figure}
        \centering
        \begin{subfigure}{0.3\textwidth}
            \includegraphics[width=\textwidth]{images/single_beam.png}
            \caption{Turn this}
        \end{subfigure}
        \begin{subfigure}{0.3\textwidth}
            \includegraphics[width=\textwidth]{images/noise.png}
            \caption{into this.}
        \end{subfigure}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency, single source}
    \begin{itemize}
        \item Sources are characterized solely by their position $x_0$ and brightness $A$.
        \item $\chi^2$ is then

            $$\chi^2 = \frac{\abs{\tilde{v}(k_x, k_y) - \hat{A}\tilde{s}(k_x, k_y; \hat{x}_0)}^2}{J(k_x, k_y)}$$
        \item Minimize $\chi^2$ to find $\hat{A}$ brightness estimator
        \item To find $\hat{x}_0$, want to convolve $v(x,y)$ with source profile $s(x,y)$, ``maximum overlap.'' 
        \item Instead convolve with ``optimal filter'' $\frac{s(x,y)}{J}$.
    \end{itemize}
    % frame the chi^2
    % talk about minimization WRT A
    % talk about convolving for x_0
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency, single source}
    \begin{center}
        Optimal Filter
    \end{center}
    \begin{figure}[!h]
        \centering
        \begin{subfigure}{0.3\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/pre_conv.png}
        \end{subfigure}
        ~
        \begin{subfigure}{0.3\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/post_conv.png}
        \end{subfigure}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency, single source}
    \begin{itemize}
        \item Convolving only identifies to nearest pixel!
        \item Since $s(x,y)$ is known (Gaussian), shape of convolution is known, can fit near peak to extract inter-pixel maximum.
            \includegraphics[width=0.8\textwidth]{plots/fit.pdf}
    \end{itemize}
    % talk about fitting technique for finding sub-pixel
    % hist
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency, single source}
    \begin{itemize}
        \item How do we verify our subtraction is optimal?
        \item Using the general result $\sigma_\xi^2 = \left[\frac{1}{2}\rtd{\chi^2}{\xi}\right]^{-1}$, can compute expected variance of $\hat{A}, \hat{x}_0$, agrees with simulations.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency, single source}
    \begin{figure}
        \begin{subfigure}{0.6\textwidth}
            \includegraphics[width=\textwidth]{plots/10000-1.pdf}
        \end{subfigure}
        ~
        \begin{subfigure}{0.6\textwidth}
            \includegraphics[width=\textwidth]{plots/1000-1posdevs.pdf}
        \end{subfigure}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency, multiple source}
    \begin{figure}
        \centering
        \begin{subfigure}{0.3\textwidth}
            \includegraphics[width=\textwidth]{images/multi_beam.png}
            \caption{Turn this}
        \end{subfigure}
        \begin{subfigure}{0.3\textwidth}
            \includegraphics[width=\textwidth]{images/noise.png}
            \caption{into this.}
        \end{subfigure}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency, multiple source}
    \begin{itemize}
        \item Found that iterative subtraction mis-estimates brightness due to mixing.
        \item Problem is linear in brightnesses, can be formulated in matricies and vectors, de-couples.
    \end{itemize}
    \begin{figure}
        \centering
        \includegraphics[width=0.65\textwidth]{plots/1000-20.pdf}
    \end{figure}
    % Talk about coupling of sources
        % matrix subtraction
    % hist
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency, multiple source}
    \begin{figure}
        \centering
        \includegraphics[width=0.65\textwidth]{plots/1000-20.pdf}
    \end{figure}
    \begin{itemize}
        \item Mean still not correct, and variance is high. Why?
        \item Blends
    \end{itemize}
    % Talk about coupling of sources
        % matrix subtraction
    % hist
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency, multiple source (blends)}
    \begin{figure}[!h]
        \centering
        \includegraphics[width=0.6\textwidth]{plots/beams_close.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency, multiple source (blends)}
    \begin{itemize}
        \item Thankfully, if blend is identified, iterative subtraction converges.
        \item Must identify which sources are blends and which are not.
        \item Intuitively: mean?
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency, multiple source (blends)}
    \begin{figure}
        \centering
        \begin{subfigure}{0.6\textwidth}
            \centering
            \includegraphics[width=\textwidth]{plots/1000-20nomeansub.pdf}
        \end{subfigure}
        ~
        \begin{subfigure}{0.6\textwidth}
            \centering
            \includegraphics[width=\textwidth]{plots/1000-20meansub.pdf}
        \end{subfigure}
    \end{figure}
    % blends
        % briefly discuss meansub, ftest
    % show plot
    % discuss that blends are not important b/c bright-bright is rare
        % and bright-dim is unavoidable
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Single frequency, multiple source (blends)}
    \begin{itemize}
        \item Problems with mean:
            \begin{itemize}
                \item Images are generally mean-subtracted; mean is meaningless.
                \item No clear criterion for what mean is sufficiently ``close to zero.''
            \end{itemize}
        \item F-test of additional parameter.
            \begin{itemize}
                \item Ran into peculiar problems \frownie
                \item Test suggested whether another beam is required \emph{depends on size of image}, unreasonable.
            \end{itemize}
        \item[]
        \item Blends are not too big of a problem in general, bright-bright is rare and bright-dim inevitable.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Multiple frequency}
    \begin{figure}[!h]
        \centering
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/sig0.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/sig1.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/sig2.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/sig3.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/sig4.png}
        \end{subfigure}
        ~
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/noise0.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/noise1.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/noise2.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/noise3.png}
        \end{subfigure}
        \begin{subfigure}{0.19\textwidth}
            \centering
            \includegraphics[width=\textwidth]{images/noise4.png}
        \end{subfigure}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Multiple frequency}
    \begin{itemize}
        \item Multiple frequencies is just setting $\chi^2 = \sum_i \chi^2_i$ and applying previous formalism.
        \item Few catches because beam profile is function of frequency:
            \begin{itemize}
                \item Source width increases linearly with wavelength.
                \item Source amplitude follows grey body spectrum --- power law approximation.
            \end{itemize}
    \end{itemize}
    $$A_\nu \propto \frac{\nu^{3 + \beta}}{e^{\frac{h\nu}{kT_{dust}}} - 1} \;\;\;\xrightarrow[T_{dust} \to \infty]{}\;\;\; \propto \nu^{2+\beta}$$ 
    % Quantify the new chi^2
    % Discuss widening of beam width
    % Discuss grey body frequency dependence of amplitude
        % power law approximation
    \begin{itemize}
        \item Let's first assume a known power law dependence, for simplicity.
    \end{itemize}
\end{frame}

\begin{frame}
    % \twocolumn, RECALL blah fit for inter-pixel position
    \frametitle{Methods}
    \framesubtitle{Multiple frequency}
    \begin{itemize}
        \item Recall in one frequency, fit for inter-pixel location.
        \item No longer holds with multiple frequencies! $\hat{x}_0$ is not the same in every frequency.
        \item Solution: weighted average works.
    \end{itemize}
    \begin{figure}[!h]
        \centering
        \begin{subfigure}{0.45\textwidth}
            \centering
            \includegraphics[width=\textwidth]{plots/pos.pdf}
        \end{subfigure}
        \begin{subfigure}{0.45\textwidth}
            \centering
            \includegraphics[width=\textwidth]{plots/amp.pdf}
        \end{subfigure}
    \end{figure}
    % discuss issue about how fitting fails when not sure of shape!
        % Formalism of weighted averages tells us how to proceed
    % Show off histograms
\end{frame}

\begin{frame}
    \frametitle{Methods}
    \framesubtitle{Moving forward}
    \begin{itemize}
        \item Recall $A_\nu \propto \frac{\nu^{3 + \beta}}{e^{\frac{h\nu}{kT_{dust}}} - 1} \xrightarrow[T_{dust} \to \infty]{} \propto \nu^{2+\beta}$
        \item $\beta, T_{dust}$ are independent for each source.
        \item Power law approximation
            \begin{itemize}
                \item $\beta$ is non-linear parameter like $x_0$, but nothing like convolution to do.
                \item Gradient search on $\chi$ with respect to $\beta$.
                \item Preliminary results are in line with expectations but not ready for presentation.
                    %TODO either mention prelim results or put in
            \end{itemize}
        \item Full grey body calculation
            \begin{itemize}
                \item Gradient search over $\beta, T_{dust}$.
                \item Preliminary results require more characterization but are encouraging.
            \end{itemize}
    \end{itemize}
    % Discuss how to go forward
        % Discuss beta fitting for power law
        % gradient search, preliminary results are in line with expectations but not ready for presentation
    % Discuss TDust fitting for grey body
        % prelim results are also in line with expectations but need more theoretical background
\end{frame}

\begin{frame}
    \frametitle{Where next?}
    \begin{itemize}
        \item Characterize the effect of confusion noise on subtraction with realistic source distribution.
        \item Examine subtraction threshold in presence of confusion noise.
        \item[]
        \item Devise pipeline for kSZ detection.
    \end{itemize}
    % Characterizing confusion noise by putting in realistic source distribution
        % specifically, examine ability to identify sources as function of amplitude in presence of confusion noise, instrumental noise
    % inserting SZ effect
\end{frame}

\begin{frame}
    \frametitle{Acknowledgements}
    \begin{itemize}
        \item Prof. Sunil R. Golwala --- Thank you for your infinite patience and consistently illuminating discussions.
        \item Caltech SFP/SURF program --- Thank you for funding this piece of research.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{References}
    {\small
    \begin{itemize}
        \item \url{http://astro.uchicago.edu/sza/primer.html}

        \item Sayers, J., S. R. Golwala, P. A. R. Ade, J. E. Aguirre, J. J. Bock, S. F. Edgington, J. Glenn, A. Goldin, D. Haig, A. E. Lange, G. T. Laurent, P. D. Mauskopf, H. T. Nguyen, and P. Rossinot. "Studies of Atmospheric Noise on Mauna Kea at 143 GHz with Bolocam." \emph{Millimeter and Submillimeter Detectors and Instrumentation for Astronomy IV} (2008): n. pag. Web.

        \item Sayers, J., T. Mroczkowski, M. Zemcov, P. M. Korngut, J. Bock, E. Bulbul, N. G. Czakon, E. Egami, S. R. Golwala, P. M. Koch, K.-Y. Lin, A. Mantz, S. M. Molnar, L. Moustakas, E. Pierpaoli, T. D. Rawle, E. D. Reese, M. Rex, J. A. Shitanishi, S. Siegel, and K. Umetsu. "A Measurement Of The Kinetic Sunyaev-Zel'dovich Signal Toward Macs J0717.5 3745." \emph{ApJ The Astrophysical Journal} 778.1 (2013): 52. Web.
\end{itemize}}
\end{frame}

\begin{frame}
    \frametitle{Questions?}
    \begin{center}
        {\Large Questions?}
    \end{center}
\end{frame}

% acknowledgements
% references
    % sayers
    % chicago picture

\end{document}

