    \documentclass[iop]{emulateapj}
    \usepackage{fancyhdr, amsmath, amsthm, amssymb, mathtools, lastpage, hyperref, enumerate, graphicx, epstopdf, setspace, wasysym, upgreek}
    \newcommand{\scinot}[2]{#1\times10^{#2}}
    \newcommand{\bra}[1]{\left<#1\right|}
    \newcommand{\ket}[1]{\left|#1\right>}
    \newcommand{\dotp}[2]{\left<\left.#1\right|#2\right>}
    \newcommand{\rd}[2]{\frac{\mathrm{d}#1}{\mathrm{d}#2}}
    \newcommand{\pd}[2]{\frac{\partial#1}{\partial#2}}
    \newcommand{\rtd}[2]{\frac{\mathrm{d}^2#1}{\mathrm{d}#2^2}}
    \newcommand{\ptd}[2]{\frac{\partial^2 #1}{\partial#2^2}}
    \newcommand{\norm}[1]{\left|\left|#1\right|\right|}
    \newcommand{\abs}[1]{\left|#1\right|}
    \newcommand{\pvec}[1]{\vec{#1}^{\,\prime}}
    \let\Re\undefined
    \let\Im\undefined
    \newcommand{\ang}[0]{\text{\AA}}
    \newcommand{\mum}[0]{\upmu \mathrm{m}}
    \DeclareMathOperator{\Re}{Re}
    \DeclareMathOperator{\Tr}{Tr}
    \DeclareMathOperator{\Im}{Im}
    \newcommand{\expvalue}[1]{\left<#1\right>}
    \usepackage[labelfont=bf, font=scriptsize]{caption}\usepackage{tikz}
    \usepackage[font=scriptsize]{subcaption}
    \everymath{\displaystyle}

\tikzstyle{circle} = [draw, circle, fill=white, node distance=3cm, minimum height=2em]

\begin{document}

\pagestyle{fancy}
\cfoot{\thepage/\pageref{LastPage}}

\title{Simulation Pipeline for Velocity Field Measurements using the Kinetic Sunyaev–--Zel'dovich Effect\\Progress Report 2}

\author{Yubo Su\\
Mentor: Prof. Sunil R. Golwala}

\date{August 3, 2015}

\maketitle

\section{Scientific Merit: Large-Scale Structure Velocity Measurements}

Various competing cosmological models are distinguishable only through precision measurements because their differences are small. However, understanding their correctness is pivotal for understanding phenomena such as the accelerating expansion of the universe. In particular more accurate measurements of velocity fields (the velocities of various galaxies, galaxy clusters and other observable light sources of inter-galactic scales) would provide powerful insight into distinguishing current models. For example, more accurate measurements of the \emph{peculiar velocity} (velocity deviations from Hubble's Law, the law that says galaxies recede from us at a rate proportional to their distance from us) would help distinguish between dark energy predictions and predictions of theories involving modifications to the laws of gravity.

Existing techniques such as spectroscopy for constraining cosmological parameters has been proposed (e.g. the total matter density $\Omega_m$ and the normalization of density fluctuations $\sigma_8$ \citep{42, 71, 92}). Moreover, recent advances in deection technology have pushed the sensitivity of spectroscopy to levels comparable with peculiar velocity techniques, though the two techniques exhibit different systematic uncertainties. Being able to take measurements with different and perhaps better systematic errors improves overall precision.

\subsection{The Kinetic Sunyaev-Zel'dovich Effect}

In contrast with established measurement techniques, the kinetic Sunyaev-Zel'dovich effect (kSZ) provides a direct measurement of line-of-sight velocities $v_z$ of hot electrons within galaxy clusters \citep{131}. Because this measurement is independent of redshift and depends only on the electron line-of-sight velocity and optical depth, local characteristics, it provides a probe with different systematic errors than conventional techniques. For example, studies conclude that in applications to dark energy constraints and general relativistic deviations, kSZ measurements would probe different mass scales and exhibit different systematic uncertainties while still maintaining a similar level of accuracy, which would be a strong way to obtain independent measurements to better constrain parameters \citep{10}. All these reasons are why kSZ measurements are some of the most exciting prospects on the horizon for cosmology.

\subsection{Current difficulties, existing Infrastructure}

Current observations of the kSZ effect have proven difficult but feasible. Some recent measurements are listed in the following paragraph. Data from the \emph{WMAP} and \emph{Planck} satellites have been used to place upper limits on the bulk flows and rms variations of $v_z$ via the kSZ signal \citep{59,93}. A combination of ACT and Sloan Digital Sky Survey III data was also used to constrain the mean pairwise momentum of clusters that is inconsistent with noise at a confidence level of $99.8\%$ \citep{48}. Most recently, kSZ measurements using Bolocam and \emph{Herschel}-SPIRE data have been able to constrain $v_z$ measurements to $3450 \pm 900 \mathrm{km/s}$ \citep{121}.

These measurements above all demonstrate both the feasibility of kSZ measurements and their potential sensitivity to moderate improvements in instrument calibration. However, there are many characterstics of the detection pipeline that resist characterization at the present. Firstly, the kSZ effect is generally dominated by its thermal cousin the tSZ effect, and is on the same order as the relativistic corrections to the tSZ effect, the rSZ signal, both of which also occupy the same frequency range as the kSZ effect. Moreover, other galactic sources such as sub-millimeter galaxies and radio galaxies also have significant emissions in the same frequencies, alongside CMB fluctuations. All of these sources must be identified and subtracted to measure a kSZ signal. Difficulties are further compounded by the relative calibration between instruments measuring in different frequency ranges. To fully understand the obstacles to reliable kSZ detection, it is necessary to conduct an end-to-end simulation of the kSZ data collection process, from generating sky maps to collecting data from these synthetic maps with simulated instruments, including various effects such as gravitational lensing, atmospheric fluctuations and imperfect instrument effects. Since the next generation of instruments will have the requisite sensitivity for a successful kSZ detection, it is of \textbf{immediate importance} to implement an end-to-end simulation of the kSZ data collection process, to quantify the impact of the various expected effects on the signal detectability.

With performing this full simulation in mind, the current research group has much of the simulation pipeline already complete, including a full simulation infrastructure to generate a sky map from first principles and models of instruments that can be scanned across the simulated sky map. This work focuses on two tasks, a robust architecture for identifying point sources across multi-band data and accurate simulations of various effects that non-negligibly influence a kSZ detection.

\section{Present work}

\subsection{Single Frequency Subtraction}

Current work has focused on subtraction of point sources from a noisy background, with the remaining pipeline to identify the kSZ signal the subject of future work. First, we developed the mathematical formalism to estimate the brightness of a single source and to subtract it. After implementing the technique in code, we confirmed that the subtraction was theoretically optimal given the characteristics of the noisy background.

After the approach to subtract a single source was verified, we attempted to generalize the approach to multiple sources but ran into a problem where overlapping sources interfered with one another. Specifically, overlapping sources would result in an overestimation of some sources' brightnesses and an underestimation of others', resulting in suboptimal subtraction that would underperform compared to the theoretical limit. Instead, we worked out a technique to simultaneously estimate the brightnesses of all sources in a map, avoiding this problem. Doing so proved a bit trickier than expected, since naive approaches proved in testing to be unstable in that estimates got worse as we tried to improve them. However, a simple characteristic of the problem, namely its being linear in the brightness of the sources, allowed us to reformulate the estimation as a simple matrix multiplication, which simultaneously estimates all the source brightnesses without incurring the aforementioned issues. Indeed, this simultaneous estimation proved in testing to be as optimal as the single-source subtraction and the theoretical limit.

We then generalized the above method to beams that may not be centered exactly on the center of a pixel. Previously, the algorithm simply identified the brightest pixel in an image and centered the source on this pixel. However, in order to subtract accurately, the center of the source must be known to sub-pixel resolution. To do this, we first obtain the estimate of the source center to the nearest pixel. Then, we take a few pixels of the image around this best estimate and then perform a fit of this subregion of the image to the beam shape. This fit computes the best estimate of the inter-pixel location of the point source, and then the previously prescribed techniques for estimation of the brightness of a source remain applicable in determining the best estimates of the brightnesses of these point sources.

\subsection{Blends}

Over the course of developing this technique, a new problem surfaced: point sources can be too close that they are difficult to distinguish numerically from one another, e.g. Figure~\ref{fig:beams}. We term these sources \emph{blends}.
\begin{figure}[!h]
    \centering
    \includegraphics[width=0.3\textwidth]{plots/beams_close.pdf}
    \caption{Plot of how two sources that are two close together can easily look like a single source, shown in cross section for clarity. The red lines represent two sources, and the black line their sum. The top image contains sources separated by 3 FWHM and the bottom image sources separated by 1.5 FWHM.}
    \label{fig:beams}
\end{figure}

The effect of this phenomenon can be clearly seen in the below plots in Figure~\ref{fig:skew_hists}; while the correct amplitude of a source when there is only one source in the map is estimated cleanly and well, when there are many sources with higher probability of overlap the estimators fail much more often. The observed widening and skew are because the code takes two very close sources and estimates them as one very bright and one very dim, the sum of which is slightly less bright than the original two sources.

\begin{figure}[!h]
    \centering
    \begin{subfigure}{0.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{plots/10000-1.pdf}
        \caption{Estimated amplitude of single source in map over 10000 noise realizations. The histogram bins are values of amplitude estimations over multiple noise realizations, logarithmic in number of counts. The bottom plot is a Gaussian fit. The true amplitude is 1 arbitrary unit.}
        \label{fig:10000-1}
    \end{subfigure}
    ~
    \begin{subfigure}{0.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{plots/1000-20.pdf}
        \caption{Estimated amplitude of twenty sources in a map over 1000 different noise realizations. Histogram bins are values of amplitude estimations over multiple noise realizations. The bottom plot is a Gaussian fit to the histogram; with perfect estimation the estimators would fit perfectly to the Gaussian fit as in \ref{fig:10000-1}, but it is evident the introduction of too many beams degrades accuracy compared to theory. The true amplitude is 1 arbitrary unit. Note both the significant number of instances far away from the correct amplitude and the asymmetry of the distribution.}
    \end{subfigure}
    \caption{Histograms of estimated amplitudes in two different configurations. Note that allowing beams to overlap produces poorer amplitude estimation as a result of being unable to detect whether a source is a single source or a blend of multiple sources.}
    \label{fig:skew_hists}
\end{figure}

It turns out however that simply going back through and re-estimating the positions of the sources \emph{with the knowledge of the other beams in the map in hand} brings the estimates much closer to the correct values. In other words, if after having gone through the image and extracted estimators for each source we go back through the map and attempt to add each source back in and re-subtract it, obtaining a new estimator for the source, the general behavior is convergent. In Figure~\ref{fig:conv} we see that over many such re-subtractions the source brightnesses converge to the correct value. Such an approach is generally termed \emph{iterative}.
\begin{figure}[!h]
    \centering
    \includegraphics[width=0.3\textwidth]{plots/conv.png}
    \caption{Plot of amplitude estimators of two beams placed two FWHM apart as both position and amplitude estimators are iteratively improved. Note that while we initially grossly misestimate the sources, iterative estimation rapidly converges towards the correct values of two beams of amplitude $0.5$ arbitrary units.}
    \label{fig:conv}
\end{figure}

Further investigation concluded sadly though that such an iterative approach would be difficult to implement. Specifically, a criterion must be developed to determine whether a source is a blend or not. Two approaches were investigated, one using the mean of the residual image as a criterion to determine whether the correct number of sources have been subtracted and one using the statistical F-test. The former, while showing great empirical success, is hardly well-grounded in rigor, as the mean value of an image holds little physical significance. Moreover, the threshold for determining whether a mean is sufficiently zero cannot be obtained from first principles, particularly in the presence of ``confusion noise,'' noise introduced due to sources whose brightness is below the subtraction threshold. The latter criterion, the F-test, produced inexplicable results that showed that the quality of a subtraction depended on how large the full image is. This is counterintuitive because subtraction quality should be local around the source and not depend on the characteristics of the remainder of the image; no further useful insights have been made.

It must be noted that blends do not pose a significant issue generally due to the aforementioned ``confusion noise,'' where bright source blends with dim sources will be inevitable, and bright source blends with bright sources that are identified as a single even brighter source is expected to have the residual mis-subtraction dominated by confusion noise as well.

\section{Multiple Frequencies}

As discussed in the introduction, being able to correlate detection across multiple frequencies improves detection ability. First, the technique developed before was extended to allow for detection across multiple frequencies, assuming a Rayleigh-Jeans dependence of the sources' brightnesses on frequency. A small difficulty arose during this work: at some point, computing the maximum of a sum across all the frequencies was required, but both because the maximum was not centered on a pixel and because the sum was non-Gaussian at its peak, existing techniques produced a inaccurate calculation of the maximum. Instead, a workaround was found to take weighted sums of each individual frequency, in which the signal is Gaussian, which again restored the calculation to the theoretical limit. The success of the algorithm is shown in Figure \ref{fig:mult}.
\begin{figure}[!h]
    \centering
    \begin{subfigure}{0.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{plots/amp.pdf}
        \caption{Histogram of amplitude estimators extracted from a multiple-frequency dataset. 1000 images, each with 8 sources of amplitude 1 were used to generate this image. As seen from the parameters in the top-left corner of the plot, the fit reproduces the theoretical limit.}
    \end{subfigure}
    \begin{subfigure}{0.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{plots/pos.pdf}
        \caption{Histogram of deviations of position estimators extracted from a multiple-frequency dataset. 1000 images, each with 8 sources were used to generate this image. The algorithm estimated their positions and compared the estimations with the real positions of the sources. Again, the algorithm produces the theoretical limit.}
    \end{subfigure}
    \caption{Histograms of subtraction algorithm's performance across multiple frequencies. Both position and amplitude estimation reach the theoretical limits on the variance of the estimators from the true values.}
    \label{fig:mult}
\end{figure}

\section{Future work}

Now that the framework for identifying multiple Rayleigh-Jeans sources across multiple frequencies is developed, the next step is to improve the utility of the algorithm by allowing sources to have a black body frequency distribution, introducing two new parameters that must be fit to the data, the emissivity $\beta$ of the source and the temperature $T$ of the source galaxies' dust. The difficulty of this procedure will be far greater computationally than before; while the position and amplitude were linear parameters and thus had closed-form estimators, these parameters have no closed form and will have to be estimated by more numerically intensive minimum-finding techniques.

Once this last estimation is implemented, a full, robust point source subtraction algorithm will be complete, at which point its performance as a function of the brightness of the sources and in the presence of ``confusion noise,'' noise introduced by sources too dim to be subtracted, will be characterized. To generate a realistic distribution of sources in any image, images will be generated with real noise parameters of detectors and simulated sources using the latest galactic models \citep{bethermin}. It is vitally important to know how subtraction quality varies with brightness to understand the overall quality of the residual image after the subtraction.

Finally, with all this done, the kSZ signal will be added to the map and the ability to detect it will be examined as a function of various unknown parameters, to maximally understand whether future detectors will be able to detect the kSZ effect.

Future work that lies beyond the scope of this project but will be relevant for future improvements to the pipeline include simulating instrumental effects, atmospheric effects and various other sources of noise that arise not simply from astronomical sources.

\clearpage
\bibliographystyle{plainnat}
\renewcommand{\bibname}{References}
\bibliography{Su_SURFProposal2015}

\end{document}

