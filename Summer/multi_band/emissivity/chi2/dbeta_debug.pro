pro dbeta_debug, num_trials
compile_opt idl2
; checking dbeta calculation against chi^2 curve
; averaged over 1000 trials


; will iterate between bounds with step size step and compare dbeta to numerical estimate
step = 0.04
bounds = [1.0, 2.0] / step



range = 256
freqs = [ 857.0, 667.0, 400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [ 0.202, 0.218, 0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

; loop to calculate
dbeta = []
chi2s = []
x = [] ; store for derivative
for emissivity=bounds[0], bounds[1] do begin
    dbeta_sub = 0
    chi2s_sub = 0
    for i=1, num_trials do begin
        noise = gennoise_multi(specdens0, 1, freqs)
        sig = addgauss_multi(1, 5, range/2 + 0.5, range/2 + 0.5, noise, emissivity=mean(bounds) * step) ; always add with same emissivity

        ; dbeta calculation
        ret = subtractmax_multi(sig, specdens0, 5, 1, emissivity=emissivity * step) ; subtract with variable emissivity
        dbeta_sub += ret.dbeta
        chi2s_sub += ret.chi2
    endfor
    dbeta = [dbeta, dbeta_sub / num_trials]
    chi2s = [chi2s, chi2s_sub/ num_trials]
    x = [x, emissivity * step]
    print, bounds[0], emissivity, bounds[1]
endfor

save, x, chi2s, filename='chi7.sav'

freqs = [  667.0, 400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [ 0.218, 0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

; loop to calculate
dbeta = []
chi2s = []
x = [] ; store for derivative
for emissivity=bounds[0], bounds[1] do begin
    dbeta_sub = 0
    chi2s_sub = 0
    for i=1, num_trials do begin
        noise = gennoise_multi(specdens0, 1, freqs)
        sig = addgauss_multi(1, 5, range/2 + 0.5, range/2 + 0.5, noise, emissivity=mean(bounds) * step) ; always add with same emissivity

        ; dbeta calculation
        ret = subtractmax_multi(sig, specdens0, 5, 1, emissivity=emissivity * step) ; subtract with variable emissivity
        dbeta_sub += ret.dbeta
        chi2s_sub += ret.chi2
    endfor
    dbeta = [dbeta, dbeta_sub / num_trials]
    chi2s = [chi2s, chi2s_sub/ num_trials]
    x = [x, emissivity * step]
    print, bounds[0], emissivity, bounds[1]
endfor
save, x, chi2s, filename='chi6.sav'

freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

; loop to calculate
dbeta = []
chi2s = []
x = [] ; store for derivative
for emissivity=bounds[0], bounds[1] do begin
    dbeta_sub = 0
    chi2s_sub = 0
    for i=1, num_trials do begin
        noise = gennoise_multi(specdens0, 1, freqs)
        sig = addgauss_multi(1, 5, range/2 + 0.5, range/2 + 0.5, noise, emissivity=mean(bounds) * step) ; always add with same emissivity

        ; dbeta calculation
        ret = subtractmax_multi(sig, specdens0, 5, 1, emissivity=emissivity * step) ; subtract with variable emissivity
        dbeta_sub += ret.dbeta
        chi2s_sub += ret.chi2
    endfor
    dbeta = [dbeta, dbeta_sub / num_trials]
    chi2s = [chi2s, chi2s_sub/ num_trials]
    x = [x, emissivity * step]
    print, bounds[0], emissivity, bounds[1]
endfor
save, x, chi2s, filename='chi5.sav'

freqs = [272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

; loop to calculate
dbeta = []
chi2s = []
x = [] ; store for derivative
for emissivity=bounds[0], bounds[1] do begin
    dbeta_sub = 0
    chi2s_sub = 0
    for i=1, num_trials do begin
        noise = gennoise_multi(specdens0, 1, freqs)
        sig = addgauss_multi(1, 5, range/2 + 0.5, range/2 + 0.5, noise, emissivity=mean(bounds) * step) ; always add with same emissivity

        ; dbeta calculation
        ret = subtractmax_multi(sig, specdens0, 5, 1, emissivity=emissivity * step) ; subtract with variable emissivity
        dbeta_sub += ret.dbeta
        chi2s_sub += ret.chi2
    endfor
    dbeta = [dbeta, dbeta_sub / num_trials]
    chi2s = [chi2s, chi2s_sub/ num_trials]
    x = [x, emissivity * step]
    print, bounds[0], emissivity, bounds[1]
endfor
save, x, chi2s, filename='chi3.sav'
end
