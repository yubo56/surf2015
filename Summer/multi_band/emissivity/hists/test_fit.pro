pro test_fit
; tests whether we can fit for emissivity
; FIT_RMS         DOUBLE        0.0059733414 (autoderivative = 0, n_el = 5466)
; FIT_RMS         DOUBLE        0.0057229795 (autoderivative = 1, n_el = 732)
; SIGM_BETA       DOUBLE        0.0059611183 (theory)
;       recall this was performed with 8 bands instead of 5, and high bands
;       dominated hard so sigm_beta was higher
;
;
; binwidth = 0.5
; SIGM_BETA       DOUBLE         0.011747459
; FIT_RMS         DOUBLE         0.012210383

emissivity = 1.5D; real emissivity


range = 256
binwidth = 1D
sigm = 5D
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

; emissivity part
emissivities = []
n_iters = []

oldtime = systime(1)
for i=0, 100 do begin
    noise = gennoise_multi(specdens0, binwidth, freqs)
    ; generate random 
    sig = addgauss_multi(1, sigm, 127.7, 127.3, noise, emissivity=emissivity)

    ; get emissivity
    ret = emissivity_multi(sig, specdens0, sigm, binwidth, init=emissivity - 0.5 + randomu(SEED)) ; start with a random seed up to 0.5 off
    emissivities = [emissivities, ret.emissivity]
    n_iters = [n_iters, ret.num_iters]
    newtime = systime(1)
    print, 'Trial' + string(i) + ' calc:' + string(ret.emissivity) + ' time:' + string(newtime - oldtime)
    oldtime = newtime
    sigm_beta = ret.sigm_beta
    save, emissivities, n_iters, sigm_beta, filename='emissivity_test.sav'
endfor
end

; 8 BAND VERSION

; range = 256
; binwidth = 1D
; sigm = 5D
; freqs = [1500.0, 857.0, 667.0, 400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
; num_bands = n_elements(freqs)
; specdens0 = spdensgen_multi(range, [0.105, 0.202, 0.218, 0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

; ; emissivity part
; emissivities = []
; n_iters = []

; oldtime = systime(1)
; for i=0, 10000 do begin
;     noise = gennoise_multi(specdens0, binwidth, freqs)
;     ; generate random 
;     sig = addgauss_multi(1, sigm, 127.7, 127.3, noise, emissivity=emissivity)

;     ; get emissivity
;     ret = emissivity_multi(sig, specdens0, sigm, binwidth, init=emissivity - 0.5 + randomu(SEED)) ; start with a random seed up to 0.5 off
;     emissivities = [emissivities, ret.emissivity]
;     n_iters = [n_iters, ret.num_iters]
;     newtime = systime(1)
;     print, 'Trial' + string(i) + ' calc:' + string(ret.emissivity) + ' time:' + string(newtime - oldtime)
;     oldtime = newtime
;     save, emissivities, n_iters, filename='emissivity_test.sav'
; endfor
; end
