pro chi_pos
; generates chi^2 surface WRT beta, known positions

range = 256
binwidth = 1D
sigm = 5D
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

num_pts = 20
num_pts_pos = 40
chi2s = dblarr(num_pts + 1, num_pts_pos + 1, num_pts_pos + 1)
x_beta = 1.25 + findgen(num_pts + 1) * 0.5 / num_pts
x_pos = 122 + findgen(num_pts_pos + 1) * 10 / num_pts_pos

for i=1, 100 do begin
    for j=0, num_pts do begin
        for k=0, num_pts_pos do begin
            for l=0, num_pts_pos do begin
                noise = gennoise_multi(specdens0, binwidth, freqs)
                sig = addgauss_multi(1, sigm, 127, 127, noise, emissivity=1.5D)
                
                ; subtract correct params
                ret1 = subtractmax_multi(sig, specdens0, sigm, binwidth, emissivity=x_beta[j], real_pos=[x_pos[k], x_pos[l]])
                chi2s[j,k,l] += ret1.chi2
            endfor
        endfor
    endfor
    print, i
    chi2s /= i
    save, chi2s, x_beta, x_pos, filename='chi_vals.sav'
    chi2s *= i
endfor

end
