pro test_single, num_trials
; tests whether the multi-band subtraction is correctly centered
; 0.0022510441    0.0022801531   0.00032283902 - experimental (x0, y0, a)
; 0.00220965                     0.000320743   - theoretical (x0/y0, a)


range = 256
bw = 480 / range
freqs = [400, 352.94, 272.73, 230.77, 150, 90.91] ; frequencies in GHz
num_bands = n_elements(freqs)

convrms = [0.181, 0.137, 0.112, 0.0947, 0.05, 0.024] / 2
specdens0 = spdensgen_multi(range, $
    convrms, replicate(8,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

s = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
s /= bw ; sigm in bins
a = convrms[0] * 20 / sqrt(!PI * s^2); SNR 5
stop

; store amps
amps = []
posx = []
posy = []
for i=0, num_trials do begin
    noise = gennoise_multi(specdens0, bw, freqs)
    sig = addgauss_multi(a, s, 127, 127, noise) ; place beam in dead center
    ret = subtractmax_multi(sig, specdens0, s, bw)
    amps = [amps, ret.aest]
    posx = [posx, ret.xparam]
    posy = [posy, ret.yparam]
    if (i mod 100) eq 0 then save, amps, posx, posy, filename='test_single.sav'
    print, i
endfor

print, ret.sigm_x0, ret.sigm_a
print, stddev(posx), stddev(posy), stddev(amps)
stop
end
