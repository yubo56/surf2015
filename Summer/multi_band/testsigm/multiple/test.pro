pro test, num_trials
; tests whether the multi-band subtraction is correctly centered with multiple beams


range = 256
bw = 480 / range
freqs = [400, 352.94, 272.73, 230.77, 150, 90.91] ; frequencies in GHz
num_bands = n_elements(freqs)

convrms = [0.181, 0.137, 0.112, 0.0947, 0.05, 0.024] / 2
specdens0 = spdensgen_multi(range, $
    convrms, replicate(8,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

s = 15D / (2 * sqrt(2 * alog(2))) ; sigm in arcsec, in terms of FWHM
s /= bw ; sigm in bins
a = convrms[0] * 20 / sqrt(!PI * s^2); SNR 5


; store amps
amps = []
devx = []
devy = []
for i=0, num_trials do begin
    ; perform subtraction
    noise = gennoise_multi(specdens0, bw, freqs)
    sig = addfunc_multi(noise, a, s, 10, minsep = 6) ; place beam in dead center
    ; sig = {sig:addgauss_multi(0.6, 3, 127.5, 127.5, noise), x0:127.5, y0:127.5}
    ret = submatwrap_multi(sig.sig, specdens0, s, bw)

    ; store results
    sort_amps = sort(-ret.as) 
    amps = [[amps], [ret.as[sort_amps[0: num_beams - 1]]]]

    ; storing position deviations is a bit trickier; thanks to minsep we can guess at which beam is which by minimizing cartesian distance without fear of mis-assigning
    xdevs = []
    ydevs = []
    for j=0, num_beams - 1 do begin
        temp = min((ret.xs - sig.x0[j])^2 + (ret.ys - sig.y0[j])^2, loc) ; loc gives minimum distance
        xdevs = [xdevs, ret.xs[loc] - sig.x0[j]] ; store deviation to minimum distance point
        ydevs = [ydevs, ret.ys[loc] - sig.y0[j]]
    endfor
    devx = [[devx], [xdevs]]
    devy = [[devy], [ydevs]]
    save, amps, devx, devy, filename='test.sav'
    print, i
    print, ret.sigm_x0, ret.sigm_a
    print, stddev(devx), stddev(devy), stddev(amps)
endfor

stop
end
