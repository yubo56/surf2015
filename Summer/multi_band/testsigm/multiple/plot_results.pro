pro plot_results
; sample script to pretty plot results from sigm_x0, sigm_a tests

restore, 'test.sav'
pos_filename='pos.ps'
amp_filename='amp.ps'

; theory values
sigm_x0 = 0.0073288255
sigm_a = 0.0010338286

; do fits
hist_x0 = histogram(devx, binsize=sigm_x0/30, locations = bins_x0)
gfit_x0 = gaussfit(bins_x0, hist_x0, coeff_x0, nterms=3)
hist_y0 = histogram(devy, binsize=sigm_x0/30, locations = bins_y0)
gfit_y0 = gaussfit(bins_y0, hist_y0, coeff_y0, nterms=3)
; handle amps, since stddev(amps) isn't very nice due to a few mis-subtractions
amps -= 0.6
amps = amps[where(amps lt mean(amps) + 5 * stddev(amps))]
amps = amps[where(amps gt mean(amps) - 5 * stddev(amps))]
hist_a = histogram(amps, binsize=sigm_a/30, locations = bins_a)
gfit_a = gaussfit(bins_a, hist_a, coeff_a, nterms=3)





; gets correct dimensions for plot
set_plot, 'X'
device, decomposed=0
tek_color
window, 0, xsize=400, ysize=400
pageInfo = pswindow()
cleanplot, /silent
wdelete

; plot x0
!P.FONT = 1
set_plot, 'PS'
device, _Extra = pageInfo, /color, filename=pos_filename, language=2, SET_FONT='Helvetica Bold', /TT_font
!P.MULTI = [0, 1, 2] ; grids window into top, bottom [win_number, cols, rows]
plot, bins_x0, hist_x0, charsize=1.0, title='X position estimation deviations', psym=10, xtitle='X position (bin)', xrange=[coeff_x0[1] - 5 * coeff_x0[2], coeff_x0[1] + 5 * coeff_x0[2]], thick=4, xthick=4, ythick=4, yrange=[0.1, max(gfit_x0)], /ylog
oplot, bins_x0, gfit_x0, color=2, thick=6
; put xparams on plot
plot_xpos = !X.CRANGE[0] + 0.05 * (!X.CRANGE[1] - !X.CRANGE[0]); coordinates for top left corner
plot_ypos = 10^(!Y.CRANGE[0] + 0.9 * (!Y.CRANGE[1] - !Y.CRANGE[0]))
str = 'mean = ' + string(format = '(G8.2,"!C")', coeff_x0[1])
str = str + 'rms = ' + string(format = '(G8.4, "!C")', coeff_x0[2])
str = str + 'theory = ' + string(format = '(G8.4)', sigm_x0)
xyouts, /data, plot_xpos, plot_ypos, str, charsize=1.0

; plot y0
!P.MULTI = [1, 1, 2] ; grids window into top, bottom [<idk>, cols, rows]
plot, bins_y0, hist_y0, charsize=1.0, title='Y position estimation deviations', psym=10, xtitle = 'Y position (bin)', xrange=[coeff_y0[1] - 5 * coeff_y0[2], coeff_y0[1] + 5 * coeff_y0[2]], thick=4, xthick=4, ythick=4, yrange=[0.1, max(gfit_y0)], /ylog
oplot, bins_y0, gfit_y0, color=2, thick=6
; put yparams on plot
plot_xpos = !X.CRANGE[0] + 0.05 * (!X.CRANGE[1] - !X.CRANGE[0]); coordinates for top left corner
plot_ypos = 10^(!Y.CRANGE[0] + 0.9 * (!Y.CRANGE[1] - !Y.CRANGE[0]))
str = 'mean = ' + string(format = '(G8.2,"!C")', coeff_y0[1])
str = str + 'rms = ' + string(format = '(G8.4, "!C")', coeff_y0[2])
str = str + 'theory = ' + string(format = '(G8.4)', sigm_x0)
xyouts, /data, plot_xpos, plot_ypos, str, charsize=1.0

; close file
device, /close_file



; get new dimensions for plot (no need to be so tall anymore)
set_plot, 'X'
device, decomposed=0
tek_color
window, 1, xsize=800, ysize=400
pageInfo = pswindow()
cleanplot, /silent
wdelete

; plot a
set_plot, 'PS'
!P.FONT = 1
device, _Extra = pageInfo, /color, filename=amp_filename, language=2, SET_FONT='Helvetica Bold', /TT_font
!P.MULTI = [0, 1, 1] ; grids window into top, bottom [win_number, cols, rows]
plot, bins_a, hist_a, charsize=1.0, title='Amplitude estimators', psym=10, xtitle='Amplitude of beam (mJy)', xrange=[coeff_a[1] - 5 * coeff_a[2], coeff_a[1] + 5 * coeff_a[2]], thick=4, xthick=4, ythick=4, yrange=[0.1, max(gfit_a)], /ylog
oplot, bins_a, gfit_a, color=2, thick=6
; put fit params on plot
plot_xpos = !X.CRANGE[0] + 0.05 * (!X.CRANGE[1] - !X.CRANGE[0]); coordinates for top left corner
plot_ypos = 10^(!Y.CRANGE[0] + 0.9 * (!Y.CRANGE[1] - !Y.CRANGE[0]))
print, coeff_a[1]
str = 'mean = ' + string(format = '(G8.2,"!C")', coeff_a[1])
str = str + 'rms = ' + string(format = '(G8.3, "!C")', coeff_a[2])
str = str + 'theory = ' + string(format = '(G8.3)', sigm_a)
xyouts, /data, plot_xpos, plot_ypos, str, charsize=1

; close file
device, /close_file

; restore plotting method
set_plot, 'X'

print, coeff_x0[2], coeff_y0[2], coeff_a[2]
print, sigm_x0, sigm_a
end
