pro gen_chi2
; generates chi^2 surface WRT beta, tdust

range = 256
binwidth = 1D
sigm = 5D
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

num_pts = 50
chi2s = dblarr(num_pts + 1, num_pts + 1)
x = transpose([[10D + findgen(num_pts + 1) * 90/num_pts],[1D + findgen(num_pts + 1) * 1 / num_pts]])

for i=1, 100 do begin
    for j=0, num_pts do begin
        for k=0, num_pts do begin
            noise = gennoise_multi(specdens0, binwidth, freqs)
            sig = addgauss_multi(1, sigm, 127.7, 127.3, noise, emissivity=1.5D, tdust=40D, /bbody)
            
            ; subtract correct params
            ret1 = subtractmax_multi(sig, specdens0, sigm, binwidth, emissivity=x[1,k], tdust=x[0,j], /bbody)
            chi2s[j,k] += ret1.chi2
        endfor
    endfor
    print, i
    chi2s /= i
    save, chi2s, x, filename='big_gen_chi2.sav'
    chi2s *= i
endfor

end
