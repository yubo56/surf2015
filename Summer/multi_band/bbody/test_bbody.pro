pro test_bbody
; tests whether we can fit for emissivity + tdust

emissivity = 1.5D; real emissivity
tdust = 40D ; real tdust


range = 256
binwidth = 1D
sigm = 5D
freqs = [400.0, 352.94, 272.73, 230.77, 150.0] ; frequencies in GHz
num_bands = n_elements(freqs)
specdens0 = spdensgen_multi(range, [0.181, 0.137, 0.112, 0.0947, 0.049] / 2, replicate(3,num_bands), replicate(8.0/3, num_bands), 1) ; PSD in units of mJy^2

; emissivity part
emissivities = []
tdusts = []
n_iters = []

oldtime = systime(1)
for i=0, 10000 do begin
    noise = gennoise_multi(specdens0, binwidth, freqs)
    ; generate random 
    sig = addgauss_multi(1, sigm, 127.7, 127.3, noise, emissivity=emissivity, tdust=tdust, /bbody)

    ; get correct chi2
    ret = subtractmax_multi(sig, specdens0, sigm, binwidth, emissivity=emissivity, tdust=tdust, /bbody)
    chi2 = ret.chi2

    ; get params
    ret = bbody_multi(sig, specdens0, sigm, binwidth, emissivity=emissivity - 0.5 + randomu(SEED), tdust=tdust - 5 + 10 * randomu(SEED)) ; start with random seeds
    if ret.tdust ne -1 then begin ; sometimes something screws up and tnmin bugs out
        emissivities = [emissivities, ret.emissivity]
        tdusts = [tdusts, ret.tdust]
        n_iters = [n_iters, ret.num_iters]
        newtime = systime(1)
        print, string(i) + ': ' + string(ret.emissivity) + ' | ' + string(ret.tdust) + ' chi2: ' + string(chi2) + ' | ' +  string(ret.chi2) + ' time:' + string(newtime - oldtime)
        oldtime = newtime
        save, emissivities, tdusts, n_iters, filename='bbody.sav'
    endif
endfor
end
