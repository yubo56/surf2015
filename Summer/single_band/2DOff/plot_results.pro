pro plot_results, FN
; sample script to pretty plot results from sigm_x0, sigm_a tests

restore, FN

; gets correct dimensions for plot
set_plot, 'X'
device, decomposed=0
tek_color
window, 0, xsize=400, ysize=200
pageInfo = pswindow()
cleanplot, /silent
wdelete

; plot x0
set_plot, 'PS'
device, _Extra = pageInfo, /color, filename=FN + '.ps', language=2
!P.MULTI = [0, 1, 1] ; grids window into top, bottom [win_number, cols, rows]
plot, bins, hist, charsize=1.0, title='X position estimators', psym=10, xtitle='X position (bin)', thick=4, xthick=4, ythick=4
oplot, bins, gfit, color=2, thick=6
; put xparams on plot
plot_xpos = !X.CRANGE[0] + 0.05 * (!X.CRANGE[1] - !X.CRANGE[0]); coordinates for top left corner
plot_ypos = !Y.CRANGE[0] + 0.9 * (!Y.CRANGE[1] - !Y.CRANGE[0])
str = 'mean = ' + string(format = '(F8.5,"!C")', coeff[1])
str = str + 'rms = ' + string(format = '(G8.4, "!C")', coeff[2])
str = str + 'theory = ' + string(format = '(G8.4)', sigm_x0)
xyouts, /data, plot_xpos, plot_ypos, str, charsize=1.0

; close file
device, /close_file

; restore plotting method
set_plot, 'X'
end
