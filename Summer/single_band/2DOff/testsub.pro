pro testsub, numTests
; Inputs
;   numTests    - number of tests to run at each ampFact
; Outputs - None
;
; Computes ratio of RMS(residual)/RMS(noise) for list of SNRs (stored in ampFact variable)
; Modified to examine ratio of RMS(theoreticalRes)/RMS(noise) as well.

; specdens params
power = 1
binwidth = 0.5D ; units of arcminutes; arcmin/bin
constPow = 3  ; how many times const is the 1/k^alpha term contribute
const = 0.1D ; units of Jy
range = 128D / binwidth
; gaussian parameters
sigm = 3D / binwidth
fitRange = 3        ; fit an area of +- fitRange around max
; store deviations
rmsDevMeans = []
theoryDevMeans = []
tempa = [] ; store amplitudes
tempx = [] ; store devs

; set up some stuff to compute Gaussians
rand = RANDOMU(seed, 2 * numTests)
x0 = rand[0 : numTests - 1] * (range - 2 * fitRange - 1) + fitRange
y0 = rand[numTests : 2 * numTests - 1] * (range - 2 * fitRange - 1) + fitRange
sigm = replicate(sigm, numTests)


; ampFact = [3, 5, 10, 15, 20, 30, 40, 50, 70, 100] ; try with these SNR ratios
ampFact = [5]

for numAmp=0, (n_elements(ampFact) - 1) do begin
    ; store deviations per SNR ratio
    rmsDevs = [] ; ratio of RMS(submap - noise) to RMS(noise)
    theoryDevs = [] ; ratio of RMS(theory map) to RMS(noise)

    ; set up amps
    Amp = ampFact[numAmp] * const   ; height of Gaussian; now simeq 5 * noise height
    A = replicate(Amp, numTests)


    ; make specdens
    k = fft_shift(dist(range))
    k[(range - 1) / 2, (range - 1) / 2 ] = 1
    specDens = make_array(range, range, VALUE=const^2) ;+ (constPow * const^2 / k ^ power)
        ; use white noise for now
        ; units of Jy^2
    specDens *= (binwidth * range)^2 ; units of Jy^2 / arcmin^-2
    specDens /= range^2 ; needed b/c specDens is off by range^2 in correspondence from cont. case

    ; go through tests
    for i=0, numtests - 1 do begin
        ; make signal
        noise = genNoise(specDens, binwidth) ; different realizations of noise each time
                                   ; currently has units Jy
        signal = addgauss(a[i], sigm[i], x0[i], y0[i], noise) ; add gaussians
            ; still units of Jy

        ; convolve map
        kSignal = fft_shift(fft(signal))
            ; no need to manually insert units, since taken care of with L^2 correspondence
        paddedKernel = addgauss(1, sigm[i], 0, 0, dblarr(range, range)) ; kernel is normalized
            ; no units
        kkernel = fft_shift(fft(paddedKernel))
        ; convolve and figure out position
        convSig = fft(fft_shift(conj(kkernel),/REVERSE) * fft_shift(kSignal, /REVERSE) / specDens, /INVERSE)
             ; optimal filter = kernel/specDens

        ; fit map
        temp = max(convSig, loc)
        loc = to2d(range, loc)
        xmax = (loc[0] + range) MOD range ; forces positive index
        ymax = (loc[1] + range) MOD range

        ; fit peak on convolved map
        minx = max([xmax - fitRange, 0]) ; bounds checking
        maxx = min([xmax + fitRange, range - 1])
        miny = max([ymax - fitRange, 0])
        maxy = min([ymax + fitRange, range - 1])
        submap = convSig[minx:maxx, miny:maxy]
        params = fitquad(alog(submap))

        ; extract centers; backwards from expected because IDL x,y axis are flipped
        xparam = params[3] / (2 * params[1]) + xmax - fitRange
        xparam = (xparam + range) mod range
            ; last term compensates for non-centered kernel
        yparam = params[2] / (2 * params[1]) + ymax - fitRange
        yparam = (yparam + range) mod range
        filter = addgauss(1, sigm[i], xparam, yparam, dblarr(range, range)) ; start with gaussian in correct place
        kFilter = fft_shift(fft(filter))
            ; no units to insert; filter is unitless, and /arcmin^-2 comes with L substitution
        Aest = real_part(TOTAL(conj(kFilter) * kSignal / specDens) / TOTAL(conj(kFilter) * kFilter / specDens))

        ; get deviations
        subtractedMap = signal - Aest * filter - noise
        rmsDevs = [rmsDevs, sqrt(mean(subtractedMap[minx:maxx, miny:maxy]^2)) / sqrt(mean(noise^2))]

        ; compare to expected map
        sigm_x0 = SQRT( REAL_PART(1 / (TOTAL( (fft_shift(dist(range, 1)^2) # replicate(1.0 / (binwidth * range)^2, range)) * conj(kfilter) * kfilter / specDens )))) / (2 * !PI * Amp * (binwidth * range))
        sigm_y0 = SQRT( REAL_PART(1 / (TOTAL( (replicate(1.0 / (binwidth * range)^2, range) # fft_shift(dist(range, 1)^2)) * conj(kfilter) * kfilter / specDens )))) / (2 * !PI * Amp * (binwidth * range))
            ; replicate contains a 1/(binwidth * range)^2 b/c nu_x needs units of 1/(binwidth * range), 1/arcmin
        sigm_A = REAL_PART(SQRT(1  / ( (binwidth * range)^2 * TOTAL(kfilter * CONJ(kfilter) / specDens))))

        grad_x = (SHIFT(filter, -1, 0) - SHIFT(filter, 1, 0))/(2 * binwidth) ; grad in x direction
        grad_y = (SHIFT(filter, 0, -1) - SHIFT(filter, 0, 1))/(2 * binwidth)
        expectedMap = Aest * (sigm_x0 * grad_x + sigm_y0 * grad_y) + sigm_A * filter
        ; expectedMapMod = Aest * binwidth * ((xparam - x0[i]) * grad_x + (yparam - y0[i]) * grad_y) + (a[i] - aest) * filter
            ; x0, xparam are nondimensional, so dimensionalize with binwidth
        ; stop; [round(xparam-10):round(xparam +10), round(yparam-10): round(yparam+10)]

        theoryDevs = [theoryDevs, sqrt(mean(expectedMap[minx:maxx, miny:maxy]^2)) / sqrt(mean(noise^2))]
        tempa = [tempa, aest]
        tempx = [tempx, (xparam - x0[i]) * binwidth]
    endfor

    rmsDevMeans = [rmsDevMeans, mean(rmsDevs)]
    theoryDevMeans = [theoryDevMeans, mean(theoryDevs)]
    print, ampFact[numAmp], mean(rmsDevs), mean(theoryDevs)

    ; plot the rmsdevs!
    hist = histogram(rmsdevs, binsize=stdev(rmsdevs) / 50, locations=bins)
    gfit = gaussfit(bins, hist, coeff, nterms = 3)
    plot, bins, hist, psym=10
    oplot, bins, gfit
    oplot, bins, replicate(mean(theoryDevs), N_ELEMENTS(bins))
endfor
; plot, ampFact, rmsDevMeans, /ynozero
stop
end
; sqrt(mean((sigm_a * filter)^2))
; sqrt(mean(((a[i] - aest) * filter)^2))
; sqrt(mean((Aest * (sigm_x0 * grad_x + sigm_y0 * grad_y))^2))
; sqrt(mean((Aest * binwidth * ((xparam - x0[i]) * grad_x + (yparam - y0[i]) * grad_y))^2))
; sqrt(mean(expectedMap[minx:maxx, miny:maxy]^2)) / sqrt(mean(noise^2))
; sqrt(mean(subtractedMap[minx:maxx, miny:maxy]^2)) / sqrt(mean(noise^2))
