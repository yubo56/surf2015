pro testsigm, numTests
; tests uncertainty on sigm with numTests guesses at x0, y0
; explanation of magic number 3*sigm: addGauss only adds up to 3 sigma, so
;   we can truncate our kernel to width 3 sigma
; at SNR 10

; specdens params
power = 8/3
constPow = 8  ; how many times const is the 1/k^alpha term contribute
const = 0.1D ; units of Jy
range = 256D
binwidth = 0.5D ; units of arcminutes; arcmin/bin
; gaussian parameters
sigm = 3D
Amp = 2 * const   ; height of Gaussian; now simeq 10 * noise height
fitRange = 3        ; fit an area of +- fitRange around max
; store deviations
xdevs = []
ydevs = []
Adevs = []

; set up some stuff to compute Gaussians
rand = RANDOMU(seed, 2 * numTests)
x0 = rand[0 : numTests - 1] * (range - 2 * fitRange - 1) + fitRange
y0 = rand[numTests : 2 * numTests - 1] * (range - 2 * fitRange - 1) + fitRange
sigm = replicate(sigm, numTests)
A = replicate(Amp, numTests)






; make specdens
specdens = spdensgen(range, const, constpow, power, binwidth)

; go through tests
for i=0, numtests - 1 do begin
    ; make signal
    noise = genNoise(specDens, binwidth) ; different realizations of noise each time
                               ; currently has units Jy
    signal = addgauss(a[i], sigm[i], x0[i], y0[i], noise) ; add gaussians
        ; still units of Jy

    ; convolve map
    kSignal = fft_shift(fft(signal))
        ; no need to manually insert units, since taken care of with L^2 correspondence
    paddedKernel = addgauss(1, sigm[i], 0, 0, dblarr(range, range)) ; kernel is normalized
        ; no units
    kkernel = fft_shift(fft(paddedKernel))
    ; convolve and figure out position
    convSig = fft(fft_shift(conj(kkernel),/REVERSE) * fft_shift(kSignal, /REVERSE) / specDens, /INVERSE)
         ; optimal filter = kernel/specDens

    ; fit map
    temp = max(convSig, loc)
    loc = to2d(range, loc)
    xmax = (loc[0] + range) MOD range ; forces positive index
    ymax = (loc[1] + range) MOD range

    ; fit peak on convolved map
    minx = max([xmax - fitRange, 0]) ; bounds checking
    maxx = min([xmax + fitRange, range - 1])
    miny = max([ymax - fitRange, 0])
    maxy = min([ymax + fitRange, range - 1])
    submap = convSig[minx:maxx, miny:maxy]
    params = fitquad(alog(submap))

    ; extract centers; backwards from expected because IDL x,y axis are flipped
    xparam = params[3] / (2 * params[1]) + xmax - fitRange
    xparam = (xparam + range) mod range
        ; last term compensates for non-centered kernel
    yparam = params[2] / (2 * params[1]) + ymax - fitRange
    yparam = (yparam + range) mod range
    filter = addgauss(1, sigm[i], xparam, yparam, dblarr(range, range)) ; start with gaussian in correct place
    kFilter = fft_shift(fft(filter))
        ; no units to insert; filter is unitless, and /arcmin^-2 comes with L substitution
    Aest = TOTAL(conj(kFilter) * kSignal / specDens) / TOTAL(conj(kFilter) * kFilter / specDens)

    ; print parameters
    ; print, 'Xparam: ' + string(xparam) + ' || X0' + string(x0[i])
    ; print, 'Yparam: ' + string(yparam) + ' || Y0' + string(y0[i])
    ; print, 'A     : ' + string(Aest) + ' || A0' + string(A[i])
    ; print, 'sigma : ' + string(sigmEst) + ' || A0' + string(sigm[i])

    ; sanity checks
    ; if abs(x0[i] - xparam) ge 1 then stop
    ; if abs(y0[i] - yparam) ge 1 then stop
    if imaginary(Aest) ge real_part(Aest) / 10 then stop

    xdevs = [xdevs, (x0[i] - xparam) * binwidth] ; convert to arcmins not bins
    ydevs = [ydevs, (y0[i] - yparam) * binwidth]
    Adevs = [Adevs, A[i] - real_part(Aest)]
endfor

; compute expected sigmas
sigm_x0 = SQRT( REAL_PART(1 / (TOTAL( (fft_shift(dist(range, 1)^2) # replicate(1.0 / (binwidth * range)^2, range)) * conj(kfilter) * kfilter / specDens )))) / (2 * !PI * Amp * (binwidth * range))
sigm_y0 = SQRT( REAL_PART(1 / (TOTAL( (replicate(1.0 / (binwidth * range)^2, range) # fft_shift(dist(range, 1)^2)) * conj(kfilter) * kfilter / specDens )))) / (2 * !PI * Amp * (binwidth * range))
    ; replicate contains a 1/(binwidth * range)^2 b/c nu_x needs units of 1/(binwidth * range), 1/arcmin
sigm_A = REAL_PART(SQRT(1  / ( (binwidth * range)^2 * TOTAL(kfilter * CONJ(kfilter) / specDens))))

; truncate stuff
temp = where(abs(xdevs) gt 5 * sigm_x0, complement=inds)
xdevs = xdevs[inds]
temp = where(abs(ydevs) gt 5 * sigm_x0, complement=inds)
ydevs = ydevs[inds]

; generate histograms
hist = histogram(xdevs, binsize=sigm_x0 / 10, locations=bins)
gfit = gaussfit(bins, hist, coeff, nterms=3)
print, coeff[2], sigm_x0
; plot, bins, hist
; oplot, bins, gfit
; write_png, 'plots/xdevs.png', tvrd()
; plot, bins, alog(hist)
; write_png, 'plots/xdevsLog.png', tvrd()
save, xdevs, filename='x0_hist.sav'

hist = histogram(ydevs, binsize=sigm_y0 / 10, locations=bins)
gfit = gaussfit(bins, hist, coeff, nterms=3)
print, coeff[2], sigm_y0
; plot, bins, hist
; oplot, bins, gfit
; write_png, 'plots/ydevs.png', tvrd()
; plot, bins, alog(hist)
; write_png, 'plots/ydevsLog.png', tvrd()
; save, ydevs, filename='y0_hist.sav'

hist = histogram(Adevs, binsize=sigm_A / 10, locations=bins)
gfit = gaussfit(bins, hist, coeff, nterms=3)
print, coeff[2], sigm_A
; plot, bins, hist
; oplot, bins, gfit
; write_png, 'plots/amps.png', tvrd()
; plot, bins, alog(hist)
; write_png, 'plots/ampsLog.png', tvrd()
save, Adevs, filename='amp_hist.sav'
stop

end
