pro itersigm, sep, numreals, numiters
; arguments 
;       sep - number of FWHM to set the beams apart
;       numreals - number of realisations of noise
;       numiters - number of iterations to do per noise realisation
; return



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PARAMETERS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

binwidth = 1D                                     ; bin width, arcmin/bin
range = 256D / binwidth                             ; size of grid, arcmin
power = 1                                           ; constants of the white noise
constPow = 3                                        ; how many times const is the 1/k^alpha term contribute
const = 0.1                                         ; Units of Jy
gaussPixWidth = 5 / binwidth                        ; pt source width in number of pixels
gaussHeight = 5 * const                             ; gaussian height in terms of noise height
    ; units of Jy



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;   CODE   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


k = fft_shift(dist(range))
k[(range - 1) / 2, (range - 1) / 2 ] = 1
specDens = make_array(range, range, VALUE=const^2) + (constPow * const^2 / k ^ power)
    ; use white noise for now
    ; units of Jy^2
specDens *= (binwidth * range)^2 ; units of Jy^2 / arcmin^-2
specDens /= range^2 ; missing 1/N^2 division that is never performed since we generate specdens directly rather than from fft
; store cumulative amplitudes

amp1Cum = [] ; n_elements = numreals, store amps over realisations
amp2Cum = []
pos1Cum = []
pos2Cum = []






beam1amp = 2 * gaussHeight ; gaussHeight = 5*SNR
beam2amp = gaussHeight

; first generate beams, separated by (trial * offset)
beams = addgauss( beam1amp, gaussPixWidth, range / 2, range / 2, dblarr(range, range))
beams = addgauss( beam2amp, gaussPixWidth, range / 2 + sep * gaussPixWidth, range / 2, beams)

for real=1, numreals do begin
    ; build noise, signal
    noise = gennoise(specdens, binwidth)
    signal = noise + beams

    ; itersubtract
    retval1 = subtractmax(signal, specdens, gaussPixWidth, binwidth)
    retval2 = subtractmax(retval1.signal, specdens, gaussPixWidth, binwidth)
    for iter=1, numiters do begin
        ; build beam1, add and resubtract
        beam1est = addgauss( retval1.aest, gaussPixWidth, retval1.xparam, retval1.yparam, dblarr(range, range))
        retval1 = subtractmax(retval2.signal + beam1est, specdens, gaussPixWidth, binwidth)

        ; same for beam2
        beam2est = addgauss( retval2.aest, gaussPixWidth, retval2.xparam, retval2.yparam, dblarr(range, range))
        retval2 = subtractmax(retval1.signal + beam2est, specdens, gaussPixWidth, binwidth)
    endfor

    amp1cum = [amp1cum, retval1.aest]
    amp2Cum = [amp2Cum, retval2.aest]
    pos1Cum = [[pos1Cum], [retval1.xparam, retval1.yparam]]
    pos2Cum = [[pos2Cum], [retval2.xparam, retval2.yparam]]
endfor

print, retval1.sigm_x0, retval2.sigm_y0, retval2.sigm_a
print, stddev(amp1cum), stddev(amp2cum)
print, stddev(pos1cum[0,*]), stddev(pos1cum[1,*]), stddev(pos2cum[0,*]), stddev(pos2cum[1,*])
stop

END
