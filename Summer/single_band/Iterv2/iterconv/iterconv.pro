pro iterconv
; tests convergence for two pulses separated by minsep * FWHM to maxsep * FWHM, as well as the deviations on the estimators

; iterparams
step = 0.1
minsep = 1.5
maxsep = 4
numreals = 200 ; number of realizations at each separation

; physical params
binwidth = 1D                                     ; bin width, arcmin/bin
range = 256D / binwidth                             ; size of grid, arcmin
power = 1                                           ; constants of the white noise
constPow = 3                                        ; how many times const is the 1/k^alpha term contribute
const = 0.1                                         ; Units of Jy
gaussPixWidth = 5 / binwidth                        ; pt source width in number of pixels
gaussHeight = 6 * const                             ; gaussian height in terms of noise height

amp1 = 2 * gaussHeight
amp2 = gaussHeight



; store characteristics of each step
steps = [] ; x-axis, separation in # of sigma
convs = [] ; average number of iterations required to reach convergence
sigm_x1 = [] ; sigm_x0 for each step
sigm_y1 = []
sigm_x2 = []
sigm_y2 = []

; start
specdens = spdensgen(range, const, constPow, power, binwidth)

; increment from minsep to maxstep with step stepsize
for sep=minsep / step, maxsep/step do begin
    beams = addgauss(amp1, gaussPixWidth, range/2, range/2, dblarr(range, range))
    beams = addgauss(amp2, gaussPixWidth, range/2, range/2 + sep * step * gaussPixWidth, beams)

    ; store as we go
    conv_real = []
    xpos1 = []
    ypos1 = []
    xpos2 = []
    ypos2 = []

    for real=1,numreals do begin
        noise = gennoise(specdens, binwidth)
        signal = noise + beams

        ; subtract now
        retVal = iterimp(signal, specdens, gaussPixWidth, binwidth, NUM_PULSES=2)

        ; store values
        conv_real = [conv_real, retVal.numIters]
        xpos1 = [xpos1, retval.xs[0]]
        ypos1 = [ypos1, retval.ys[0]]
        xpos2 = [xpos2, retval.xs[1]]
        ypos2 = [ypos2, retval.ys[1]]
    endfor

    histx1 = hist_wrapper(xpos1, stddev(xpos1) / 10, min(xpos1), max(xpos1), /gauss, /noverbose)
    histy1 = hist_wrapper(ypos1, stddev(ypos1) / 10, min(ypos1), max(ypos1), /gauss, /noverbose)
    histx2 = hist_wrapper(xpos2, stddev(xpos2) / 10, min(xpos2), max(xpos2), /gauss, /noverbose)
    histy2 = hist_wrapper(ypos2, stddev(ypos2) / 10, min(ypos2), max(ypos2), /gauss, /noverbose)

    ; store values
    steps = [steps, sep * step]
    convs = [convs, mean(conv_real)]
    sigm_x1 = [sigm_x1, histx1.fit_rms]
    sigm_y1 = [sigm_y1, histy1.fit_rms]
    sigm_x2 = [sigm_x2, histx2.fit_rms]
    sigm_y2 = [sigm_y2, histy2.fit_rms]
    print, sep * step, mean(conv_real), histx1.fit_rms, histy1.fit_rms, histx2.fit_rms, histy2.fit_rms
endfor

save, steps, convs, sigm_x1, sigm_y1, sigm_x2, sigm_y2, filename='iterconv.sav'
stop
end
