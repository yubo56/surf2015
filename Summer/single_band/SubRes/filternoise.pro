pro filterNoise, numSamples, numGaussians
; arguments 
;       numSamples - Number of times to try  subtracting some number of pulses
;       gaussHeight - height of Gaussians being inserted (width/number are fixed in program)
;       numGaussians - number of Gaussians to insert and seek

binwidth = 1D                                     ; bin width, arcmin/bin
range = 256D / binwidth                             ; size of grid, arcmin
power = 1                                           ; constants of the white noise
constPow = 3                                        ; how many times const is the 1/k^alpha term contribute
const = 0.1                                         ; Units of Jy
gaussPixWidth = 5 / binwidth                        ; pt source width in number of pixels
gaussHeight = 10 * const                             ; gaussian height in terms of noise height

; store cumulative amplitudes, moments
ampsCum = []
returns = []




; generate specdens
specdens = spdensgen(range, const, constpow, power, binwidth)

; test this for some number of iterations
for trial=1, numSamples do begin
    ; make noise in k space, random every time
    noise = genNoise(specDens, binwidth)
    ; units Jy

    ; add Gaussians
    addRetVal = addfunc(noise, gaussHeight, gaussPixWidth, numGaussians)
    signal = addRetVal.signal

    ; subtract
    retval = meansub(signal, specdens, gaussPixWidth, binwidth)
    if retval.numIters eq -1 then continue ; don't bother doing this
    ;stop ; useful commands for this stop:
        ; print, retval.as                      (see all amplitudes and see whether correct #)
        ; print, retval.xs, retval.ys           (prints locations, corresponding to amplitudes)
        ; plot, retval.xs, retval.ys, psym=2    (plots locations of beams)
    ; ampsCum = [[ampsCum], [retval.As]]
    ampsCum = [ampsCum, retval.As]

    ; print progress, save
    if (trial MOD 20) eq 0 then begin
        print, trial
        save, ampscum, filename='1000-20v_meansub.sav'
    endif
endfor

stop
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;           UNCOMMENT FOR AMPSHIST                   ;;;;;;;;;;;;;;
; hist = histogram(ampsCum, binsize = (mean(retVal.sigm_a)/10), locations = bins)
; gfit = gaussfit(bins, hist, coeff, nterms = 3)
; print, mean(retVal.sigm_a)
; print, coeff
; stop
; plot, bins, hist, psym = 10
; oplot, bins, gfit
; write_png, 'amps_dev.png', tvrd()
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
END
