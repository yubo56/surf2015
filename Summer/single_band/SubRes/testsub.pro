pro testsub, numTests
; Inputs
;   numTests    - number of tests to run at each ampFact
; Outputs - None
;
; checks subtraction residuals against expected expression

; specdens params
power = 1
binwidth = 0.5D ; units of arcminutes; arcmin/bin
constPow = 3  ; how many times const is the 1/k^alpha term contribute
const = 0.1D ; units of Jy
range = 128D / binwidth
; gaussian parameters
sigm = 3D / binwidth
fitRange = 3        ; fit an area of +- fitRange around max
; store deviations
rmsDevMeans = []
theoryDevMeans = []
tempa = [] ; store amplitudes
tempx = [] ; store devs

; set up some stuff to compute Gaussians
rand = RANDOMU(seed, 2 * numTests)
x0 = rand[0 : numTests - 1] * (range - 2 * fitRange - 1) + fitRange
y0 = rand[numTests : 2 * numTests - 1] * (range - 2 * fitRange - 1) + fitRange
sigm = replicate(sigm, numTests)


; ampFact = [3, 5, 10, 15, 20, 30, 40, 50, 70, 100] ; try with these SNR ratios
ampFact = [5]

for numAmp=0, (n_elements(ampFact) - 1) do begin
    ; store deviations per SNR ratio
    rmsDevs = [] ; ratio of RMS(map - noise) to RMS(noise)
    theoryDevs = [] ; ratio of RMS(theory map) to RMS(noise)
    means = [] ; mean of map

    ; set up amps
    Amp = ampFact[numAmp] * const   ; height of Gaussian; now simeq 5 * noise height
    A = replicate(Amp, numTests)


    ; make specdens
    specdens = spdensgen(range, const, constPow, power, binwidth)

    ; go through tests
    for i=0, numtests - 1 do begin
        ; make signal
        noise = genNoise(specDens, binwidth) ; different realizations of noise each time
                                   ; currently has units Jy
        beam = addgauss(a[i], sigm[i], x0[i], y0[i], dblarr(range, range)) ; add one beam
        signal = beam + noise

        ; get max
        retval = subtractmax(signal, specdens, sigm[i], binwidth)
        subtractedMap = retval.signal - noise
        rmsDevs = [rmsDevs, sqrt(mean(subtractedMap^2)) / sqrt(mean(noise^2))]
        means = [means, mean(subtractedmap)]

        ; compare to expected map
        grad_x = (SHIFT(beam, -1, 0) - SHIFT(beam, 1, 0))/(2 * binwidth) ; grad in x direction
        grad_y = (SHIFT(beam, 0, -1) - SHIFT(beam, 0, 1))/(2 * binwidth)
        map = retval.sigm_a^2 * beam^2 + a[i]^2 * retval.sigm_x0^2 * (grad_x^2 + grad_y^2)

        theoryDevs = [theoryDevs, sqrt(mean(map)) / sqrt(mean(noise^2))]
    endfor

    rmsDevMeans = [rmsDevMeans, mean(rmsDevs)]
    theoryDevMeans = [theoryDevMeans, mean(theoryDevs)]
    print, ampFact[numAmp], mean(means), mean(rmsDevs), mean(theoryDevs)

    ; plot the rmsdevs!
    hist = histogram(rmsdevs, binsize=stdev(rmsdevs) / 50, locations=bins)
    gfit = gaussfit(bins, hist, coeff, nterms = 3)
    plot, bins, hist, psym=10
    oplot, bins, gfit
    oplot, bins, replicate(mean(theoryDevs), N_ELEMENTS(bins))
endfor
; plot, ampFact, rmsDevMeans, /ynozero
stop
end
