pro avgsubmean
; for various numbers of coincident beams, checks mean of subtraction residuals as function of number of beams
;
; will add the same beams to various different realisations of noise and subtract, average the mean of residual map over realisations


; parameters
numTrials = 200
binwidth = 1D                                     ; bin width, arcmin/bin
range = 128D / binwidth                             ; size of grid, arcmin
power = 1                                           ; constants of the white noise
constPow = 3                                        ; how many times const is the 1/k^alpha term contribute
const = 0.1                                         ; Units of Jy
gaussPixWidth = 5 / binwidth                        ; pt source width in number of pixels
gaussHeight = 5 * const                             ; gaussian height in terms of noise height

; make specdens
specdens = spdensgen(range, const, constpow, power, binwidth)

; make beams
beam1 = addgauss(replicate(gaussHeight, 1), replicate(gaussPixWidth, 1), 63, 63, dblarr(range, range))
beam2 = addgauss(replicate(gaussHeight, 2), replicate(gaussPixWidth, 2), [63, 63], [63, 69], dblarr(range, range))
beam3 = addgauss(replicate(gaussHeight, 3), replicate(gaussPixWidth, 3), [63, 63, 68], [63, 69, 65], dblarr(range, range))

; store average of (mean of residual map) over many iterations
; try num_pulses = num_beams
vals1 = [0.0,0.0]
vals2 = [0.0,0.0,0.0]
vals3 = [0.0,0.0,0.0,0.0]

for i=0, numTrials do begin
    ; just reuse noise b/c easier
    noise = gennoise(specdens, binwidth)

    ; generate signals
    sig1 = beam1 + noise
    sig2 = beam2 + noise
    sig3 = beam3 + noise
    
    ; try various #s of subtractions for various num_pulses
    ; average over +- gaussPixWidth of beams
    retval1 = submatwrap(sig1, specdens, gaussPixWidth, binwidth, num_pulses=1)
    vals1[0] += mean(retval1.signal[58:68, 58:68])
    retval2 = submatwrap(sig1, specdens, gaussPixWidth, binwidth, num_pulses=2)
    vals1[1] += mean(retval2.signal[58:68, 58:68])

    retval1 = submatwrap(sig2, specdens, gaussPixWidth, binwidth, num_pulses=1)
    vals2[0] += mean(retval1.signal[58:68, 58:74])
    retval2 = submatwrap(sig2, specdens, gaussPixWidth, binwidth, num_pulses=2)
    vals2[1] += mean(retval2.signal[58:68, 58:74])
    retval3 = submatwrap(sig2, specdens, gaussPixWidth, binwidth, num_pulses=3)
    vals2[2] += mean(retval3.signal[58:68, 58:74])

    retval1 = submatwrap(sig3, specdens, gaussPixWidth, binwidth, num_pulses=1)
    vals3[0] += mean(retval1.signal[58:73, 58:74])
    retval2 = submatwrap(sig3, specdens, gaussPixWidth, binwidth, num_pulses=2)
    vals3[1] += mean(retval2.signal[58:73, 58:74])
    retval3 = submatwrap(sig3, specdens, gaussPixWidth, binwidth, num_pulses=3)
    vals3[2] += mean(retval3.signal[58:73, 58:74])
    retval4 = submatwrap(sig3, specdens, gaussPixWidth, binwidth, num_pulses=4)
    vals3[3] += mean(retval4.signal[58:73, 58:74])

    if i MOD numTrials/20 eq 0 then begin
        print, strcompress(string(i * 100 / numTrials) + '%')
    endif
endfor

print, vals1/numTrials, vals2/numTrials, vals3/numTrials
stop

end
