pro test

sizes = [20, 32, 64, 128, 256]
specdensorig = spdensgen(256, 0.1, 3, 1, 1)
for i=0, n_elements(sizes) - 1 do begin
    specdens0 = fft_shift(fft(specdensorig))
    specdens = real_part(fft(fft_shift(specdens0[127 - sizes[i] / 2 + 1: 127 + sizes[i] / 2, 127 - sizes[i] / 2 + 1: 127 + sizes[i] / 2], /reverse), /inverse))
    chi11 = []
    chi12 = []
    chi112 = []
    chi21 = []
    chi22 = []
    chi212 = []
    for j=0, 999 do begin
        noise = gennoise(specdens, 1)
        sig1 = addgauss(1, 5, sizes[i]/2, sizes[i]/2, noise)
        sig2 = addgauss([1, 1], [5, 5], [sizes[i]/2, sizes[i]/2],[ sizes[i]/2 - 3, sizes[i]/2 + 3], noise)

        retval11 = submatwrap(sig1, specdens, 5, 1, num_pulses=1)
        retval12 = submatwrap(sig1, specdens, 5, 1, num_pulses=2)
        retval21 = submatwrap(sig2, specdens, 5, 1, num_pulses=1)
        retval22 = submatwrap(sig2, specdens, 5, 1, num_pulses=2)
        chi11 = [chi11, retval11.chi2]
        chi12 = [chi12, retval12.chi2]
        chi112 = [chi112, retval11.chi2 - retval12.chi2]
        chi21 = [chi21, retval21.chi2]
        chi22 = [chi22, retval22.chi2]
        chi212 = [chi212, retval21.chi2 - retval22.chi2]
    end
    print, sizes[i], mean(chi11), stddev(chi11), mean(chi12), stddev(chi12), mean(chi112), stddev(chi112)
    print, mean(chi21), stddev(chi21), mean(chi22), stddev(chi22), mean(chi212), stddev(chi212)
    print
endfor

stop
end
