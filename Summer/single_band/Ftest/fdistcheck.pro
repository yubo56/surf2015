pro fdistcheck
; checks distributions with two pulses separated by 1.2sigma of chi1, chi2, chi2 - chi1, f-score
;
; denote chi1 = chi^2 with 1 beam, chi2 = chi^2 with 2 beams

numIncs = 12
specdens = spdensgen(256, 0.1, 3, 1, 1)

chi11 = [] ; store values
chi12 = []
chi121 = []
chi21 = [] ; store values
chi22 = []
chi221 = []

for i=0, 100 do begin
    ; make signal
    noise = gennoise(specdens, 1)
    sig1 = addgauss([0.5],[5],[127],[127], noise)
    sig2 = addgauss([0.5,0.5],[5,5],[127,127],[124, 130], noise)

    ; subtract
    retval11 = submatwrap(sig1, specdens, 5, 1, num_pulses = 1)
    retval12 = submatwrap(sig1, specdens, 5, 1, num_pulses = 2)
    retval21 = submatwrap(sig2, specdens, 5, 1, num_pulses = 1)
    retval22 = submatwrap(sig2, specdens, 5, 1, num_pulses = 2)

    ; store to val
    chi11 = [chi11, retval11.chi2]
    chi12 = [chi12, retval12.chi2]
    chi121 = [chi121, retval12.chi2 - retval11.chi2]
    chi21 = [chi21, retval21.chi2]
    chi22 = [chi22, retval22.chi2]
    chi221 = [chi221, retval22.chi2 - retval21.chi2]
    print, i
endfor

stop
end
