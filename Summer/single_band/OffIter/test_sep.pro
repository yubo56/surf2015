pro test_sep
; Tests the ability of two procedures to determine the accurate amplitudes of
; two beams as a function of their separation
;
; Two procedures are: subtractmat with exact positions
;                     subtractmat with positions fed by subtractmax

binwidth = 1D                                     ; bin width, arcmin/bin
range = 32D / binwidth                             ; size of grid, arcmin
power = 1                                           ; constants of the white noise
constPow = 3                                        ; how many times const is the 1/k^alpha term contribute
const = 0.1                                         ; Units of Jy
gaussPixWidth = 5 / binwidth                        ; pt source width in number of pixels
gaussHeight = 5 * const                             ; gaussian height in terms of noise height
    ; units of Jy

; iteration parameters
offset = 0.004                                      ; roughly sigma_x_0/3
numreals = 10                                       ; number of realisations at each displacement
    


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;   CODE   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

k = fft_shift(dist(range))
k[(range - 1) / 2, (range - 1) / 2 ] = 1
specDens = make_array(range, range, VALUE=const^2) + (constPow * const^2 / k ^ power)
    ; use white noise for now
    ; units of Jy^2
specDens *= (binwidth * range)^2 ; units of Jy^2 / arcmin^-2
specDens /= range^2 ; missing 1/N^2 division that is never performed since we generate specdens directly rather than from fft

amp1cumIter = [] ; 2 * numreals array of all amps
amp2cumIter = []
amp1cum = []
amp2cum = []

; add two pulses with various offsets
for trial=long(0.1 * gaussPixWidth / offset), long(3 * gaussPixWidth / offset) do begin
    amps1Iter = []
    amps2Iter = []
    amps1 = []
    amps2 = []
    
    ; first generate beams, separated by (trial * offset)
    beams = addgauss( gaussHeight, gaussPixWidth, range / 2, range / 2, dblarr(range, range))
    beams = addgauss( gaussHeight, gaussPixWidth, range / 2 + (trial * offset), range / 2, beams)

    for realisation=1, numreals do begin
        ; make noise in k space, random every time
        noise = genNoise(specDens, binwidth)
        ; units Jy

        ; generate signal
        signal = noise + beams

        ; itersubtract
        retval1 = subtractmax(signal, specdens, gaussPixWidth, binwidth)
        retval2 = subtractmax(retval1.signal, specdens, gaussPixWidth, binwidth)
        retValMatIter = subtractmat(signal, specdens, [retval1.xparam, retval2.xparam], [retval1.yparam, retval2.yparam], gaussPixWidth, binwidth)

        ; matrix subtract
        retValMat = subtractmat(signal, specdens, [range/2, range/2 + (trial * offset)], [range/2, range/2], gaussPixWidth, binwidth)

        ; store amps
        amps1Iter = [amps1Iter, retValMatIter.amps[0]]
        amps2Iter = [amps2Iter, retValMatIter.amps[1]]
        amps1 = [amps1, retValMat.amps[0]]
        amps2 = [amps2, retValMat.amps[1]]
    endfor

    print, trial * offset, mean(amps1Iter), mean(amps2Iter), mean(amps1), mean(amps2)
    
    ; store amps
    amp1cumIter = [[amp1cumIter], [amps1Iter]]
    amp2cumIter = [[amp2cumIter], [amps2Iter]]
    amp1cum = [[amp1cum], [amps1]]
    amp2cum = [[amp2cum], [amps2]]
endfor

stop
end
