pro testsigm, numTests
; tests uncertainty on sigm with numTests guesses at x0, y0
; explanation of magic number 3*sigm: addGauss only adds up to 3 sigma, so
;   we can truncate our kernel to width 3 sigma

; specdens params
power = 1
constPow = 3  ; how many times const is the 1/k^alpha term contribute
const = 0.1D ; units of Jy
range = 256D
binwidth = 0.5D ; units of arcminutes; arcmin/bin
; gaussian parameters
sigm = 3D
Amp = 5 * const   ; height of Gaussian; now simeq 5 * noise height
; store deviations
xdevs = []
ydevs = []
Adevs = []

; set up some stuff to compute Gaussians
edgeRange = 3        ; do not make Gaussians too close to edge
rand = RANDOMU(seed, 2 * numTests)
x0 = rand[0 : numTests - 1] * (range - 2 * edgeRange - 1) + edgeRange
y0 = rand[numTests : 2 * numTests - 1] * (range - 2 * edgeRange - 1) + edgeRange
sigm = replicate(sigm, numTests)
A = replicate(Amp, numTests)






; make specdens
k = fft_shift(dist(range))
k[(range - 1) / 2, (range - 1) / 2 ] = 1
specDens = make_array(range, range, VALUE=const^2) + (constPow * const^2 / k ^ power)
    ; use white noise for now
    ; units of Jy^2
specDens *= (binwidth * range)^2 ; units of Jy^2 / arcmin^-2
specDens /= range^2 ; needed b/c specDens is off by range^2 in correspondence from cont. case

; go through tests
for i=0, numtests - 1 do begin
    ; make signal
    noise = genNoise(specDens, binwidth) ; different realizations of noise each time
                               ; currently has units Jy
    signal = addgauss(a[i], sigm[i], x0[i], y0[i], noise) ; add gaussians
        ; still units of Jy

    ; subtract maximum
    retVal = subtractmax(signal, specdens, sigm[i], binwidth)

    xparam = retVal.xparam
    yparam = retVal.yparam
    Aest   = retVal.Aest
    signal = retVal.signal

    ; sanity checks
    if abs(x0[i] - xparam) ge 1 then stop
    if abs(y0[i] - yparam) ge 1 then stop
    if imaginary(Aest) ge real_part(Aest) / 10 then stop

    xdevs = [xdevs, (x0[i] - xparam) * binwidth] ; convert to arcmins not bins
    ydevs = [ydevs, (y0[i] - yparam) * binwidth]
    Adevs = [Adevs, A[i] - real_part(Aest)]
endfor

sigm_x0 = retVal.sigm_x0
sigm_y0 = retVal.sigm_y0
sigm_a  = retVal.sigm_a

; generate histograms
hist = histogram(xdevs, binsize=sigm_x0 / 10, locations=bins)
gfit = gaussfit(bins, hist, coeff, nterms=3)
print, coeff[2], sigm_x0
; plot, bins, hist
; oplot, bins, gfit
; write_png, 'plots/xdevs.png', tvrd()
; plot, bins, alog(hist)
; write_png, 'plots/xdevsLog.png', tvrd()

hist = histogram(ydevs, binsize=sigm_y0 / 10, locations=bins)
gfit = gaussfit(bins, hist, coeff, nterms=3)
print, coeff[2], sigm_y0
; plot, bins, hist
; oplot, bins, gfit
; write_png, 'plots/ydevs.png', tvrd()
; plot, bins, alog(hist)
; write_png, 'plots/ydevsLog.png', tvrd()

hist = histogram(Adevs, binsize=sigm_A / 10, locations=bins)
gfit = gaussfit(bins, hist, coeff, nterms=3)
print, coeff[2], sigm_A
; plot, bins, hist
; oplot, bins, gfit
; write_png, 'plots/amps.png', tvrd()
; plot, bins, alog(hist)
; write_png, 'plots/ampsLog.png', tvrd()
stop

end
