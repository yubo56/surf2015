pro filterNoise, numSamples, numGaussians
; arguments 
;       numSamples - Number of times to try  subtracting some number of pulses
;       numGaussians - number of Gaussians to insert and seek



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PARAMETERS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

binwidth = 1D                                     ; bin width, arcmin/bin
range = 256D / binwidth                             ; size of grid, arcmin
power = 1                                           ; constants of the white noise
constPow = 3                                        ; how many times const is the 1/k^alpha term contribute
const = 0.1                                         ; Units of Jy
gaussPixWidth = 5 / binwidth                        ; pt source width in number of pixels
gaussHeight = 5 * const                             ; gaussian height in terms of noise height
    ; units of Jy

    


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;   CODE   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; store cumulative amplitudes, moments
ampsCum = []
momentsCum = []
returns = []

k = fft_shift(dist(range))
k[(range - 1) / 2, (range - 1) / 2 ] = 1
specDens = make_array(range, range, VALUE=const^2) + (constPow * const^2 / k ^ power)
    ; use white noise for now
    ; units of Jy^2
specDens *= (binwidth * range)^2 ; units of Jy^2 / arcmin^-2
specDens /= range^2 ; missing 1/N^2 division that is never performed since we generate specdens directly rather than from fft



; test this for some number of iterations
for trial=1, numSamples do begin
    ; make noise in k space, random every time
    noise = genNoise(specDens, binwidth)
    ; units Jy

    ; add Gaussian of height
    addRetVal = addfunc(noise, gaussHeight, gaussPixWidth, numGaussians)
    signal = addRetVal.signal

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;OPTIMAL FILTERING;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Code works by first finding pulses, then constructing matrix and inverting

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; FIND PULSES;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    orig = signal ; keep a copy for matrix computation later
    foundX = [] ; store found locations
    foundY = []

    ; let's just subtract numSamples for now until we know our matrix implementation works
    for i=1, numGaussians do begin
        retVal = subtractmax(signal, specDens, gaussPixWidth, binwidth)
        loc = retVal.loc
        signal = retVal.signal

        foundX = [foundX, loc[0]]
        foundY = [foundY, loc[1]]
    endfor

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; matrix solve the problem ;;;;;;;;;;;;;;;;;;;;;;;

    retValMat = subtractmat(orig, specDens, foundX, foundY, gaussPixWidth, binwidth)
    ; if max(abs(retValMat.amps - gaussHeight)) gt (gaussHeight / 5) then stop
    ampsCum = [[ampsCum], [retValMat.amps]]
    momentsCum = [[momentsCum], [moment(retValMat.amps)]]




    ;;;;;;;;;;;;;; save returns ;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; addRetVal = {signal:addGauss(A, sigm2, x0, y0, noise), x0:x0, y0:y0}
    ; retVal = {loc: [xparam, yparam], Aest: Aest, signal: (signal - Aest * filter), sigm_x0:sigm_x0, sigm_y0:sigm_y0, sigm_a:sigm_a}
    ; retValMat = {amps:real_part(amps), x:foundX, y:foundY}
    returns = [returns, {x0:addRetVal.x0, y0:addRetVal.y0, x:foundX, y:foundY, Aest:retValMat.amps, sigm_x0:retVal.sigm_x0, sigm_y0:retVal.sigm_y0, sigm_a:retVal.sigm_a, gauss_sigm:gaussPixWidth}]
endfor


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;           UNCOMMENT FOR TESTSIG                    ;;;;;;;;;;;;;;
; plot, retVal.amps, /ynozero
; plot, ampsCum, /ynozero
; oplot, replicate(gaussHeight + retVal.sigm_a, numGaussians)
; oplot, replicate(gaussHeight - retVal.sigm_a, numGaussians)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;           UNCOMMENT FOR TESTMULT                   ;;;;;;;;;;;;;;
; if numSamples ne 1 then begin
;     print, TOTAL(momentsCum, 2)/numSamples ; prints sigma
; endif else begin
;     print, momentsCum
; endelse
; print, [ retVal.sigm_a, retVal.sigm_a^2 ]
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;           UNCOMMENT FOR AMPSHIST                   ;;;;;;;;;;;;;;
hist = histogram(ampsCum, binsize = (retVal.sigm_a/10), locations = bins)
gfit = gaussfit(bins, hist, coeff, nterms = 3)
print, retVal.sigm_a
print, coeff
print, coeff[1] - retVal.sigm_a / sqrt(numSamples), coeff[1] + retVal.sigm_a / sqrt(numSamples)
print, mean(ampscum) - retVal.sigm_a / sqrt(numSamples), mean(ampscum) + retVal.sigm_a / sqrt(numSamples)
stop
plot, bins, hist, psym = 10
oplot, bins, gfit
write_png, 'amps_dev.png', tvrd()
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
END
