pro gen_hist, FN
; generates histogram from ampscum, restoring input parameter

restore, FN
amp_filename=FN + '.ps'
sigm_a = 0.013868306 ; theoretical value

; get fit
amps = ampscum[where(ampscum lt mean(ampscum) + 5 * sigm_a)]
amps = ampscum[where(ampscum gt mean(amps) - 5 * sigm_a)] ; only get mean +- 5sigm_a
hist_a = histogram(amps, binsize=sigm_a/20, locations = bins_a)
gfit_a = gaussfit(bins_a, hist_a, coeff_a, nterms=3)

; get new dimensions for plot (no need to be so tall anymore)
set_plot, 'X'
device, decomposed=0
tek_color
window, 1, xsize=800, ysize=400
pageInfo = pswindow()
cleanplot, /silent
wdelete

; plot a
!P.FONT = 1
set_plot, 'PS'
device, _Extra = pageInfo, /color, filename=amp_filename, language=2, SET_FONT='Helvetica Bold', /TT_font
!P.MULTI = [0, 1, 1] ; grids window into top, bottom [win_number, cols, rows]
plot, bins_a, hist_a, charsize=1.0, title='Amplitude estimators', psym=10, xtitle='Amplitude of beam (units)', xrange=[coeff_a[1] - 5 * coeff_a[2], coeff_a[1] + 5 * coeff_a[2]], thick=2, xthick=4, ythick=4, /ylog, yrange=[0.1, max(gfit_a)]
oplot, bins_a, gfit_a, color=2, thick=6
; put fit params on plot
plot_xpos = !X.CRANGE[0] + 0.05 * (!X.CRANGE[1] - !X.CRANGE[0]); coordinates for top left corner
plot_ypos = 10^(!Y.CRANGE[0] + 0.9 * (!Y.CRANGE[1] - !Y.CRANGE[0]))
str = 'mean = ' + string(format = '(G8.5,"!C")', coeff_a[1])
str = str + 'rms = ' + string(format = '(G8.3, "!C")', coeff_a[2])
str = str + 'theory = ' + string(format = '(G8.3)', sigm_a)
xyouts, /data, plot_xpos, plot_ypos, str, charsize=1

; close file
device, /close_file

; restore plotting method
set_plot, 'X'

end
