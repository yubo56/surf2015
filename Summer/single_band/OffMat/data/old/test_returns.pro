pro test_returns
; takes no arguments
;
; simply tests what we get when we apply subtract max to returns containing
; failures even when exact positions are used

binwidth = 1D                                     ; bin width, arcmin/bin
range = 256D / binwidth                             ; size of grid, arcmin
power = 1                                           ; constants of the white noise
constPow = 3                                        ; how many times const is the 1/k^alpha term contribute
const = 0.1                                         ; Units of Jy
gaussPixWidth = 5 / binwidth                        ; pt source width in number of pixels
gaussHeight = 5 * const                             ; gaussian height in terms of noise height

k = fft_shift(dist(range))
k[(range - 1) / 2, (range - 1) / 2 ] = 1 
specDens = make_array(range, range, VALUE=const^2) + (constPow * const^2 / k ^ power)
    ; use white noise for now
        ; units of Jy^2
        specDens *= (binwidth * range)^2 ; units of Jy^2 / arcmin^-2
        specDens /= range^2 ; missing 1/N^2 division that is never performed since we generate specdens directly rather than from fft


restore, 'returns.sav'
aests = []

for i=1, n_elements(returns) do begin
    ret = subtractmax(returns[i-1].signal, specDens, returns[i-1].gauss_sigm, binwidth)
    aests = [aests, ret.aest]
endfor
print, mean(aests)
plot, aests[sort(aests)]
stop

end
