pro filterNoise, numSamples, numGaussians
; arguments 
;       numSamples - Number of times to try  subtracting some number of pulses
;       gaussHeight - height of Gaussians being inserted (width/number are fixed in program)
;       numGaussians - number of Gaussians to insert and seek



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PARAMETERS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

binwidth = 1D                                     ; bin width, arcmin/bin
range = 256D / binwidth                             ; size of grid, arcmin
power = 1                                           ; constants of the white noise
constPow = 3                                        ; how many times const is the 1/k^alpha term contribute
const = 0.1                                         ; Units of Jy
gaussPixWidth = 5 / binwidth                        ; pt source width in number of pixels
gaussHeight = 10 * const                             ; gaussian height in terms of noise height
    ; units of Jy



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;   CODE   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; store cumulative amplitudes, moments
ampsCum = []
returns = []

; generate specdens
specdens = spdensgen(range, const, constpow, power, binwidth)

; test this for some number of iterations
for trial=1, numSamples do begin
    ; make noise in k space, random every time
    noise = genNoise(specDens, binwidth)
    ; units Jy

    ; add Gaussian of height
    addRetVal = addfunc(noise, gaussHeight, gaussPixWidth, numGaussians)
    signal = addRetVal.signal


    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; FIND PULSES;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    retval = iterimp(signal, specdens, gaussPixWidth, binwidth, /SIGM, num_pulses=20)
    foundX = retval.xs
    foundY = retval.ys

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; matrix solve the problem ;;;;;;;;;;;;;;;;;;;;;;;

    retValMat = subtractmat(signal, specDens, foundX, foundY, gaussPixWidth, binwidth)
    ; if max(abs(retValMat.amps - gaussHeight)) gt (gaussHeight / 5) then stop
    ampsCum = [[ampsCum], [retValMat.amps]]



    ;;;;;;;;;;;;;; save returns ;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; addRetVal = {signal:addGauss(A, sigm2, x0, y0, noise), x0:x0, y0:y0}
    ; retVal = {loc: [xparam, yparam], Aest: Aest, signal: (signal - Aest * filter), sigm_x0:sigm_x0, sigm_y0:sigm_y0, sigm_a:sigm_a}
    ; retValMat = {amps:real_part(amps), x:foundX, y:foundY}
    ; returns = [returns, {x0:addRetVal.x0, y0:addRetVal.y0, x:foundX, y:foundY, Aest:retValMat.amps, sigm_x0:retVal.sigm_x0, sigm_y0:retVal.sigm_y0, sigm_a:retVal.sigm_a, gauss_sigm:gaussPixWidth}]
    if (trial MOD 20) eq 0 then print, trial
    save, ampscum, filename='1000-20v_iter.sav'
endfor


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;           UNCOMMENT FOR AMPSHIST                   ;;;;;;;;;;;;;;
hist = histogram(ampsCum, binsize = (mean(retVal.sigm_a)/10), locations = bins)
gfit = gaussfit(bins, hist, coeff, nterms = 3)
print, mean(retVal.sigm_a)
print, coeff
stop
plot, bins, hist, psym = 10
oplot, bins, gfit
write_png, 'amps_dev.png', tvrd()
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
END
